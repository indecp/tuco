# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import csv
import sys
import os
import json
import requests
import time
from pprint import pprint
from flask_restful import  fields, marshal

if os.path.exists('radi/.env'):
    print('Importing environment from radi/.env...')
    for line in open('radi/.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


RADI_JSON = os.environ.get('RADI_JSON')
TORRENT_DIR = os.environ.get('TORRENT_DIR')


_str = "RADI3"
FORMAT = "RADI{}_".format(_str[4:].zfill(2))

#with open(RADI_JSON, 'rb') as radijson:

def create_post(payload):
    url = os.environ.get('URL_API')
    #url = u'http://tuco.tdcpb.org/pelle/v1.0/Short'
    headers = {'Content-Type': "application/json; charset=UTF-8"}
    auth=(os.environ.get('USER'), os.environ.get('PASSWORD'))
    r =requests.post(url, headers = headers, auth=auth,
        data = json.dumps(payload, ensure_ascii=False).encode('utf-8'))
    print r.status_code
    if r.status_code not in [201,409]:
        print r.status_code
    time.sleep(0.1)

with open(RADI_JSON, 'r') as data_file:
    jsondata = json.load(data_file)




torrents = os.listdir(TORRENT_DIR)
#for _t in torrents:
#    _f, _e = os.path.splitext(_t)
#    if _e == ".torrent":
#        res = seachInJson(jsondata, _f)
not_found = 0
nbfound = 0
nbshort = 0
for _short in jsondata:
    _torrent_name =  []
    _found = False
    nbshort +=1
    for _dcp in  _short[u'torrents']:
        _str = _short[u'radi']
        _radi = "RADI{}_".format(_str[4:].zfill(2))
        _radi = ""
        _t = u'{}{}.torrent'.format(_radi,_dcp)
        if _t in torrents:
            _found = True
            _torrent_name.append(u'{}{}'.format(_radi,_dcp))
        else:
            print "NOT found" , _t
    if _found:
        _short[u'torrents'] = _torrent_name
        create_post(_short)
        nbfound +=1
    else:
        not_found +=1

print "shorts not found:", not_found
print "shorts  found:", nbfound
print "Total json data:", nbshort








