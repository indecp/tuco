#!/usr/bin/env python
import os
from datetime import datetime, timedelta
DAYS_AGO = 8
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from app import db, create_app
from app.models import User, Distributor, Movie, Exhibitor, Dcp, Distribution, DistributionStatus, TrackerMessage, TdcpbServer
from parse_db_inputs import Parseinputs
from app.torrent.xbt import XbtDB
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()
app.logger.info('start tracker update')
_diff = datetime.now() - timedelta(days=DAYS_AGO)

TrackerMessage.update(since=_diff)
app.logger.info('finish tracker update')

db.session.remove()
app_context.pop()

