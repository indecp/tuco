"""issue314: update tables

Revision ID: 11a4ad8ab33d
Revises: 3d0d78bc2789
Create Date: 2018-06-29 13:58:11.210735

"""

# revision identifiers, used by Alembic.
revision = '11a4ad8ab33d'
down_revision = '3d0d78bc2789'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('message',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('from_id', sa.Integer(), nullable=True),
    sa.Column('to_id', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.Column('body', sa.Text(), nullable=True),
    sa.Column('subject', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['from_id'], ['user.userid'], ),
    sa.ForeignKeyConstraint(['to_id'], ['user.userid'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('movie_request_message',
    sa.Column('movie_request_id', sa.Integer(), nullable=True),
    sa.Column('message_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['message_id'], ['message.id'], ),
    sa.ForeignKeyConstraint(['movie_request_id'], ['movie_request.id'], )
    )
    op.add_column(u'movie_request', sa.Column('allow_date', sa.DateTime(), nullable=True))
    op.add_column(u'movie_request', sa.Column('cancel_date', sa.DateTime(), nullable=True))
    op.add_column(u'movie_request', sa.Column('deny_causeId', sa.Integer(), nullable=True))
    op.add_column(u'movie_request', sa.Column('deny_date', sa.DateTime(), nullable=True))
    op.add_column(u'movie_request', sa.Column('deny_other_cause', sa.Text(), nullable=True))
    op.add_column(u'movie_request', sa.Column('first_demand_date', sa.DateTime(), nullable=True))
    op.add_column(u'movie_request', sa.Column('statusId', sa.Integer(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column(u'movie_request', 'statusId')
    op.drop_column(u'movie_request', 'first_demand_date')
    op.drop_column(u'movie_request', 'deny_other_cause')
    op.drop_column(u'movie_request', 'deny_date')
    op.drop_column(u'movie_request', 'deny_causeId')
    op.drop_column(u'movie_request', 'cancel_date')
    op.drop_column(u'movie_request', 'allow_date')
    op.drop_table('movie_request_message')
    op.drop_table('message')
    ### end Alembic commands ###
