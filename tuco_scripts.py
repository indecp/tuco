#!/usr/bin/env python
import os

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.models import User,Exhibitor, Distribution, Dcp, DistributionStatus
from app.TorrentsModel import Client, Torrent
from flask_script import Manager, Shell

from sqlalchemy.orm.exc import NoResultFound

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
def make_shell_context():
    return dict(app=app, db=db, User=User)


@manager.command
@manager.option('-l', '--login', help='user login')
@manager.option('-p', '--password', help='user pass to set')
def setpass(login="login", password="dummy"):
    "Set user pass and activate it"
    _u = User.query.filter_by(login=login).first()
    if _u is None:
        app.logger.error("No user with login {}".format(login))
        return
    _u.set_password(password)
    _u.confirmed = True
    db.session.add(_u)
    db.session.commit()
    app.logger.info('password set for {}'.format(login))


@manager.command
@manager.option('-l', '--login', help='torrent client login')
@manager.option('-e', '--email', help='user email')
@manager.option('-p', '--password', help='user password')
@manager.option('-c', '--cinema', help='cinema name')
@manager.option('-i', '--iptinc', help='tinc ip')
def new_exploitant(
        login    = u"login",
        email    = u"email",
        password = u"dummy",
        cinema   = u"Cinema",
        iptinc  =  u"10.10.10.0",
        ):
    "Create new exhibitor"
    _u = User(login = login,
              email = email,
              user_role = "Exhibitor")
    _u.set_password(password)
    _u.confirmed = True
    db.session.add(_u)
    _e = Exhibitor(name = cinema,
                   iptinc = iptinc,
                   user = _u,
                   tc_login = login,
                   tc_password = password)
    _e.nbscreens = 0
    db.session.add(_e)

    _tc = Client(
            ipt = iptinc,
            login = login,
            password = password,
            client_type = u'cinema')
    db.session.add(_tc)
    db.session.commit()

    Distribution.create_new_user_authorisations(_e)

    app.logger.info('Cinema {} added in tucodb'.format(cinema))

@manager.command
@manager.option('-l', '--login', help='torrent client login')
@manager.option('-e', '--email', help='user email')
@manager.option('-p', '--password', help='user password')
def new_apiuser(
        login    = u"login",
        email    = u"email",
        password = u"dummy",
        ):
    "Create new exhibitor"
    _u = User.query.filter_by(login=login).first()
    if _u:
        app.logger.error(u"User {} already in DB".format(_u))
        return -1
    _u = User(login = login,
              email = email,
              user_role = "Apiuser")
    _u.set_password(password)
    _u.confirmed = True
    db.session.add(_u)
    db.session.commit()

    app.logger.info('API user {} added in tucodb'.format(_u))




@manager.command
@manager.option('-c', '--cinema', help='cinema name')
@manager.option('-l', '--login', help='cinema user login')
def rm_exploitant(
        cinema   = u"Cinema",
        login = None
        ):
    "Remove exploitant"
    _e = Exhibitor.query.filter_by(name=cinema).first()
    if _e is None:
        app.logger.info('Cinema {} unknown'.format(cinema))
        app.logger.info('Nothing to do leaving')
        return
    if  not _e.user:
        if login:
            _login = login
        else:
            app.logger.info('Cinema {} without user'.format(cinema))
            app.logger.info('Nothing to do leaving')
            return
    else:
        _login = _e.user.login
    _u = User.query.filter_by(login=_login).first()
    _tc = Client.query.filter_by(login=_login).first()
    _dist = Distribution.query.filter(Distribution.exhibitorid == _e.exhibitorid).all()
    var = raw_input("Confirm remove of {} (y/n):".format(_e.name))
    if var != "y":
        app.logger.info('Nothing to do leaving')
        return

    if _dist is not None:
        app.logger.info('Removing all distribution authorisations for {}'.format(_e.name))
        for _auth in _dist:
            db.session.delete(_auth)
        db.session.commit()

    if _u is not None:
        db.session.delete(_u)
        app.logger.info('User {} removed'.format(_login))
        db.session.commit()
    else:
        app.logger.info('User {} unknown'.format(_login))

    if _e is not None:
        db.session.delete(_e)
        app.logger.info('Cinema {} removed'.format(cinema))
        db.session.commit()
    else:
        app.logger.info('Cinema {} unknown'.format(cinema))

    if _tc is not None:
        for _torrent in Torrent.query.filter_by(client =_tc).all():
            db.session.delete(_torrent)

        db.session.delete(_tc)
        db.session.commit()
        app.logger.info('Torrent client for {} removed'.format(_login))
    else:
        app.logger.info('Torrent client for {} unknown'.format(_login))

@manager.command
@manager.option('-c', '--cinema', help='cinema cnc ID ')
@manager.option('-d', '--dcp', help='dcp ID ')
def rm_authorisation(
        cinema   = u"Cinema",
        dcp = None
        ):
    "Remove Movie download authorisation"
    try:
        e = Exhibitor.query.filter_by(cncid = cinema).one()
    except NoResultFound:
        app.logger.error("No exhibitor {} found".format(cinema))
        return


    try:
        d = Dcp.query.get(dcp)
    except NoResultFound:
        app.logger.error("No Dcp {} found".format(dcp))
        return

    print e
    print d
    try:
        a = Distribution.query.filter_by(exhibitor = e , dcp =d).one()
    except NoResultFound:
        app.logger.error("No result found")
        return

    if a.dcp.contentkind == Dcp.FEATURE:
        a.isauthorised = False
        a.autorisation_date = None
        a.status = DistributionStatus.DL_NOT_AUTHORIZED
    elif a.dcp.contentkind == Dcp.TRAILER:
        a.autorisation_date = None
        a.status = DistributionStatus.DL_AUTHORIZED
    else:
        app.logger.error("No result found")
        return

    var = raw_input("Confirm delete authorisation of {} (y/n):".format(a))
    if var == "y":
        db.session.commit()
    else :
        app.logger.info('Nothing to do leaving')
        return

    print a
    print a.isauthorised
    print a.autorisation_date
    print a.dcp.contentkind
    _msg = u'Distribution authorsation of {} to {}  deleted'.format(
            a.dcp.name,
            e.name,)
    app.logger.info(_msg)


if __name__ == '__main__':
    manager.run()

