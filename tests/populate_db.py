#!/usr/bin/env python
import os

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from datetime import datetime
DATE_FORMAT="%Y-%m-%d"
from app import db, create_app

from app.models import User, Distributor, Movie, Exhibitor, Dcp, Distribution, DistributionStatus, TrackerMessage, TdcpbServer
from parse_db_inputs import Parseinputs
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()
print app.config['TORRENT_FOLDER']
db.drop_all()
db.create_all()
# user
_hautetcourt = User(login = 'hautetcourt',
                    email= 'hautetcourt@example.com',
                    role=User.DISTRIBUTOR)

#TDCPB serveurs
_parser = Parseinputs('./serveurs.txt')
for _s in _parser.list_serveurs():
    _e = TdcpbServer( name   = _s[u'name'],
                    iptinc = _s[u'iptinc'])

    db.session.add(_e)
db.session.commit()

#Exhibitors
_parser = Parseinputs('./salles.csv')
for _s in _parser.list_salles():
    _u = User ( login    = _s[u'login'],
                email    = _s[u'email'],
                role     = User.EXHIBITOR)

    db.session.add(_u)
    _e = Exhibitor( name   = _s[u'name'],
                    iptinc = _s[u'iptinc'],
                    city   = _s[u'city'],
                    dept   = _s[u'dept'],
                    user   = _u)

    db.session.add(_e)
db.session.commit()

# Distributors
_parser = Parseinputs('./agarder.csv')
for _d in _parser.list_distributors():
    _distributor = Distributor(name = _d)
    if _distributor.name == u'HAUT ET COURT':
        _distributor.user = _hautetcourt
    db.session.add(_distributor)
db.session.commit()

# Movies
for _m in _parser.list_movies():
    _distributor = Distributor.query.filter_by(name = _m[u'distributor']).first()
    if _distributor:
        _movie = Movie(title = _m[u'title'],
                      original_title = _m[u'original_title'],
                      releasedate = _m[u'releasedate'],
                      distributor = _distributor)
        db.session.add(_movie)
    else :
        print "Movie %s without distributor, weird"%_m[u'title']
db.session.commit()

#DCP/Torrents
for _t in _parser.list_torrents():
    #print _t
    _movie = Movie.query.filter_by(title=_t[u'movie_title']).first()
    if _movie:
        _dcp = Dcp( name = _t[u'name'],
                    contentkind = Dcp.FEATURE,
                    size = _t[u'size'],
                    torrent_hash = _t[u'hash'],
                    torrent_creation = _t[u'torrent_creation'],
                    movie = _movie)
        db.session.add(_dcp)
    else :
        print "Dcp/torrent %s without movie, weird"%_m[u'name']
db.session.commit()
# TRAILERS

_parser = Parseinputs('./trailers.csv')
for _t in _parser.list_trailers():
    _movie = Movie.query.filter_by(title=_t[u'movie_title']).first()
    if _movie:
        _dcp = Dcp( name = _t[u'name'],
                    contentkind = Dcp.TRAILER,
                    size = _t[u'size'],
                    torrent_hash = _t[u'hash'],
                    torrent_creation = _t[u'torrent_creation'],
                    movie = _movie)
        db.session.add(_dcp)
    else :
        print "Dcp/torrent %s without movie, weird"%_m[u'name']
db.session.commit()
_m =  Movie.query.get_or_404(3)
print _m.title
print _m.dcps.all()
TrackerMessage.update()
db.session.commit()

db.session.remove()
app_context.pop()

