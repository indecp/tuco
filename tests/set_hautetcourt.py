# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
#!/usr/bin/env python
import os
from datetime import datetime, timedelta
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from app import db, create_app
from app.models import User, Distributor, Movie, Exhibitor, Dcp, Distribution, DistributionStatus, TrackerMessage, TdcpbServer
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

app.logger.warning('Removing fake users')

_u = User.query.filter_by(login='hautetcourt').first()

_m = Movie.query.filter_by(title = 'CITIZENFOUR').first()
print _m.title
_e = Exhibitor.query.filter_by(name = 'FAKE_EXHIBITHOR').first()

for _dcp in _m.dcps:
    print 'In distribution {}: {}'.format(
        _dcp.name, _dcp.is_in_distribution(_e))

_m.add_dcps_for_distribution(_e, dcp_type = Dcp.FEATURE)

for _dcp in _m.dcps:
    print 'In distribution {}: {}'.format(
        _dcp.name, _dcp.is_in_distribution(_e))

_m.add_dcps_for_distribution(_e, dcp_type = Dcp.TRAILER)

for _dcp in _m.dcps:
    print 'In distribution {}: {}'.format(
        _dcp.name, _dcp.is_in_distribution(_e))

_m.add_dcps_for_distribution(_e, dcp_type = Dcp.FEATURE)

for _dcp in _m.dcps:
    print 'In distribution {}: {}'.format(
        _dcp.name, _dcp.is_in_distribution(_e))







#db.session.commit()
db.session.remove()
app_context.pop()

