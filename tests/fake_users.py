#!/usr/bin/env python
import os
from datetime import datetime, timedelta
DAYS_AGO = 30
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from app import db, create_app
from app.models import User, Distributor, Movie, Exhibitor, Dcp, Distribution, DistributionStatus, TrackerMessage, TdcpbServer
from app.distributor.views import do_distribution

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()
db.drop_all()
db.create_all()

app.logger.warning('Adding fake users')

# add fake distributor
_d = Distributor(name=u'FAKE DISTRIBUTOR')
db.session.add(_d)
_u = User(login="fake_distrib",
        email ="nicolas.bertrand@tdcpb.org",
        user_role = "Distributor")
_u.set_password('123456')
_u.confirmed = True
_u.distributor = _d
db.session.add(_u)
db.session.commit()

# add fake exhibitor
_e = Exhibitor(name =u'FAKE EXHIBITOR')
db.session.add(_e)
_u = User(login="fake_exhib",
        email ="nicoinattendu@gmail.com",
        user_role = "Exhibitor")
_u.set_password('123456')
_u.confirmed = True
_u.exhibitor = _e
db.session.add(_u)
db.session.commit()

# add dummy admin
_u = User(login="fake_admin",
        email ="lutins@tdcpb.org",
        user_role = "Administrator")
_u.set_password('123456')
_u.confirmed = True
db.session.add(_u)
db.session.commit()

# add fake movie
_m = Movie(title = u'FAKE_MOVIE')
_m.distributor = _d
db.session.add(_m)
# add fake FTR Dcp
_d1 = Dcp(name=u'FAKE_FTR', contentkind=Dcp.FEATURE, movie = _m)
db.session.add(_d1)
_d2 = Dcp(name=u'FAKE_FTR_OCAP', contentkind=Dcp.FEATURE, movie = _m)
db.session.add(_d2)
_d3 = Dcp(name=u'FAKE_TLR', contentkind=Dcp.TRAILER, movie =_m)
db.session.add(_d3)

db.session.commit()

#do_distribution(_m, _e)
#db.session.commit()
db.session.remove()
app_context.pop()

