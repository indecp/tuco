#!/usr/bin/env python
import sys
sys.path.append('/home/nicolas/dev/tuco')
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]



from datetime import datetime
DATE_FORMAT="%Y-%m-%d"
from flask import current_app
from app import db, create_app

from app.models import User, Distributor, Movie, Exhibitor, Distribution, DistributionStatus
from app.models import Dcp
from app.distributor.distribution import do_distribution

#app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app = create_app('testing')
app_context = app.app_context()
app_context.push()



movie = Movie.query.filter_by(title = u'MON FILM').first()
print movie
exhibitor = Exhibitor.query.filter_by(name=u'TEST EXPLOITANT').first()
print u'{}'.format(exhibitor)

for dcp in movie.get_all_dcps():
    authors=Distribution.query.filter_by(dcp = dcp, exhibitor=exhibitor).all()
    for author in authors:
        print author.distributionid
        author.started_transfer_date = None
        author.finished_transfer_date = None
        author.autorisation_date = None
        if dcp.contentkind == Dcp.FEATURE:
            author.isauthorised = False
            author.status = DistributionStatus.DL_NOT_AUTHORIZED
        else:
            author.status = DistributionStatus.DL_AUTHORIZED
            author.isauthorised = True

db.session.commit()
#    _dcp = Dcp.query.filter_by(name = dcp_name).first()
#    if _dcp :
#        _authors=Distribution.query.filter_by(dcpid = _dcp.dcpid).all()
#        for item in _authors:
#            if (_dcp.contentkind == Dcp.FEATURE) and (item.isauthorised):
#                item.isauthorised = False
#        db.session.commit()
#    else:
#        print "No dcp found for {}".format(dcp_name)

#dcp= Dcp.query.filter_by(name = TEST_DCPS[0][0]).first()
#print dcp
#dcps = [ Dcp.query.filter_by(name = TEST_DCPS[0][0]).first(),
#        Dcp.query.filter_by(name = TEST_DCPS[1][0]).first() ]

do_distribution(movie, movie.get_all_dcps(), exhibitor)

db.session.remove()
app_context.pop()
