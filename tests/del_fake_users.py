# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
#!/usr/bin/env python
import os
from datetime import datetime, timedelta
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from app import db, create_app
from app.models import User, Distributor, Movie, Exhibitor, Dcp, Distribution, DistributionStatus, TrackerMessage, TdcpbServer
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

app.logger.warning('Removing fake users')

_u = User.query.filter_by(login='fake_distrib').first()
_d = _u.distributor
for _m in _d.movies:
    for _dcp in _m.dcps:
        for _item in _dcp.exhibitors:
            db.session.delete(_item)
            app.logger.warning("Deleting {}".format(_item))
        db.session.delete(_dcp)
        app.logger.warning("Deleting {}".format(_dcp.name))
    db.session.delete(_m)
    app.logger.warning("Deleting {}".format(_m.title))
db.session.delete(_d)
db.session.delete(_u)
db.session.commit()

_u = User.query.filter_by(login='fake_exhib').first()
_e = _u.exhibitor
db.session.delete(_e)
db.session.delete(_u)
db.session.commit()



_u = User.query.filter_by(login='nicolas').first()
print _u.login


#db.session.commit()
db.session.remove()
app_context.pop()

