# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
import sys
import requests
import json
BASE       = u"http://localhost:5000/"
#BASE       = u"https://tuco.tdcpb.org/"
BASE_PELLE = BASE + u"pelle/v1.0/"

token_url = BASE + "api/token"

exhibitors_url       = BASE_PELLE + u"exhibitors"
exhibitors_current_downloads = exhibitors_url \
                                + u"/{cncid}/current_downloads"

distributors_url        = BASE_PELLE       + u"distributors"
distributors_movies_url = distributors_url + u"/{cncid}/movies"

distributions_url      = BASE_PELLE + u"distributions/{cncid}/{dcpid}"
distributions_post_url = BASE_PELLE + u"distributions"

headers = {'Content-Type': "application/json; charset=UTF-8"}

TEST_RUN_TPL = u"Runing Test {} -- {}"
TEST_RES_TPL = u"Test {} -- {} : {} {}"


def connect(login, password):
    auth = (login,password)
    r = requests.get(token_url,auth= auth, headers=headers)
    if r.status_code != 200:
        print "get token not valid"
        sys.exit()
    return (r.json()['token'], 'dummy')


def test1(auth, expected_nb_exhibitors):
    testid = 1
    testdesc = u'GET all exhibitors'
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = exhibitors_url
    r = requests.get(url,auth= auth, headers=headers)
    if r.status_code == 200 :
        json_data =  json.loads(r.text)
        if not json_data['exhibitors']:
            test_status = u"FAIL"
        if len(json_data['exhibitors']) == expected_nb_exhibitors :
            test_status = u"OK"
        else:
            fail_text = u"Incorrect number of exhibitors"

    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def test2(auth,  cncid, expected_exhibitor_name):
    testid = 2
    testdesc = u'GET one exhibitor'
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = exhibitors_url+"/{cncid}".format(cncid=cncid)
    r = requests.get(url,auth= auth, headers=headers)
    if r.status_code == 200 :
        json_data =  json.loads(r.text)
        exhibitor = json_data['exhibitor']
        if exhibitor :
            cond = True
            cond = cond & (exhibitor[u'name'] == expected_exhibitor_name)
            cond = cond & (exhibitor[u'cncid'] == cncid)
            if cond:
                test_status = u"OK"
            else:
                fail_text = u"invalid exhibitor data"
    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def test3(auth,  cncid, expected=None):
    testid = 3
    testdesc = u'GET exhibitor current downloads'
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = exhibitors_current_downloads.format(cncid=cncid)
    r = requests.get(url,auth= auth, headers=headers)
    if r.status_code == 200 :
        json_data =  json.loads(r.text)
        print "incomplete test"
        #TODO: find a way to improve this tests
        test_status = u"OK"

    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def test4(auth, expected_nb_distributors):
    testid = 4
    testdesc = u'GET all distributors'
    test_status = u"FAIL"
    fail_text = u'unknown'
    print TEST_RUN_TPL.format(testid, testdesc)

    url = distributors_url
    r = requests.get(url,auth= auth, headers=headers)
    if r.status_code == 200 :
        json_data =  json.loads(r.text)
        if not json_data['distributors']:
            test_status = u"FAIL"
        if len(json_data['distributors']) == expected_nb_distributors :
            test_status = u"OK"
        else:
            fail_text = u"Incorrect number of distributors"
    else:
        fail_text = u"Invalid return code {}".format(r.status_code)

    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status


def test5(auth, distributor_cncid, expected_distributor_name):
    testid = 5
    testdesc = u'GET one distributor'
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = distributors_url+"/{cncid}".format(cncid=distributor_cncid)
    r = requests.get(url,auth= auth, headers=headers)
    if r.status_code == 200 :
        json_data =  json.loads(r.text)
        distributor = json_data['distributor']
        if distributor :
            cond = True
            cond = cond & (distributor[u'name'] == expected_distributor_name)
            cond = cond & (distributor[u'cncid'] == distributor_cncid)
            if cond:
                test_status = u"OK"
            else:
                fail_text = u"invalid distributor data"

    else:
        fail_text = u"Invalid return code {}".format(r.status_code)
    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def get_distributor_movies(auth,  distributor_cncid, expected=None):
    testid = 6
    testdesc = u'GET distributor catalogue'
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = distributors_movies_url.format(cncid=distributor_cncid)
    r = requests.get(url,auth= auth, headers=headers)
    if r.status_code == 200 :
        json_data =  json.loads(r.text)
        print "incomplete test"
        #TODO: find a way to improve this tests
        test_status = u"OK"
    else:
        fail_text = u"Invalid return code {}".format(r.status_code)

    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def get_authorisation(auth,  exhibitor_cncid, dcpid):
    url = distributions_url.format(cncid= exhibitor_cncid, dcpid=dcpid)
    r = requests.get(url,auth= auth, headers=headers)
    print r
    print r.status_code
    json_data = json.loads(r.text)
    print json.dumps(json_data, indent=2, sort_keys=True)

def test7(auth,  exhibitor_cncid, dcpid, expected_dcp=None, expected_exhibitor=None):
    testid = 7
    testdesc = u'GET distribution authorizations'
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = distributions_url.format(cncid= exhibitor_cncid, dcpid=dcpid)
    r = requests.get(url,auth= auth, headers=headers)
    if r.status_code == 200 :
        json_data =  json.loads(r.text)
        print  json.dumps(json_data, indent=2, sort_keys=True)
        try:
            distribution = json_data['distributions']
        except KeyError as exp:
            print exp
            fail_text= u'Invalid attribute'
        else:
            cond = True
            cond = cond & (distribution['dcp'] == expected_dcp)
            print cond
            cond = cond & (distribution['exhibitor'] == expected_exhibitor)
            print cond
            if cond:
                test_status = u"OK"
            else:
                fail_text = u"invalid distribution data"

    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def test8(auth,  exhibitor_cncid, dcpid, expected_dcp=None, expected_exhibitor=None):
    testid = 8
    testdesc = u'Remove distribution authorizations'
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = distributions_url.format(cncid= exhibitor_cncid, dcpid=dcpid)
    r = requests.delete(url,auth= auth, headers=headers)
    if r.status_code == 204:
        test_status = u"OK"
        r = requests.get(url,auth= auth, headers=headers)
        if r.status_code == 200:
            json_data = json.loads(r.text)
            print  json.dumps(json_data, indent=2, sort_keys=True)

    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def distributions_get(auth):
    url = distributions_post_url
    r = requests.get(url,auth= auth, headers=headers)
    print r
    print r.status_code
    print json.loads(r.text)

def distributions_post(auth, exhibitor_cncid, dcpid):
    url = distributions_post_url
    data = {
        'dcpid' :dcpid,
        'cncid': exhibitor_cncid
        }
    r = requests.post(url,auth= auth, json = data)
    print r
    print r.status_code
    print json.loads(r.text)


def test9(auth,  exhibitor_cncid, dcpid, expected_dcp=None, expected_exhibitor=None):
    testid = 9
    testdesc = u'DO distribution '
    test_status = u"FAIL"
    fail_text = u''
    print TEST_RUN_TPL.format(testid, testdesc)

    url = distributions_post_url
    data = {
        'dcpid' :dcpid,
        'cncid': exhibitor_cncid
        }
    r = requests.post(url,auth= auth, json = data)
    if r.status_code == 201:
        json_data = json.loads(r.text)
        print  json.dumps(json_data, indent=2, sort_keys=True)
        test_status = u"OK"



    print TEST_RES_TPL.format(testid, testdesc, test_status, fail_text)
    return test_status

def main(argv):
    if argv[3] :
        BASE = argv[3]
    auth = connect(argv[1], argv[2])
    CINEMA ={
            'name': u'Amerian',
            'cncid': 430243 ,
            }
    DCP = {
            'name':'LeQuatuorACornes_SHR_F_FR-XX_51_2K_20180529_DSN_SMPTE_OV',
            'dcpid': 3191,
            }

    # res = get_distributor_movies(auth, DISTRIBUTOR_CNCID, u'LES FILMS DU LOSANGE')
    distributions_get(auth)
    get_authorisation(auth, CINEMA['cncid'], DCP['dcpid'])
    distributions_post(auth, CINEMA['cncid'], DCP['dcpid'])



if __name__ == "__main__":
    sys.exit(main(sys.argv))
