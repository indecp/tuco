from datetime import datetime
DATE_FORMAT="%Y-%m-%d"
from app import db, create_app

from app.models import User, Distributor, Movie, Exhibitor, Dcp, Distribution, DistributionStatus

app = create_app('testingdev')
app_context = app.app_context()
app_context.push()

db.drop_all()
db.create_all()

# users 
_u1 = User(login = 'hautercourt', email='hautetcourt@example.com', password='123456', role=User.DISTRIBUTOR)
_u2 = User(login = 'pyramide', email='pyramide@example.com',    password='123456', role=User.DISTRIBUTOR)
_u3 = User(login = 'utopiatls', email='utopiatls@example.com',   password='123456', role=User.EXHIBITOR)
_u4 = User(login='utopiatnf', email='utopiatnf@example.com',   password='123456', role=User.EXHIBITOR)
_u5 = User(login = 'utopiamontpellier',  email='utopiamtp@example.com',   password='123456', role=User.EXHIBITOR)
_u6 = User(login = 'utopiabdx ', email='utopiadbx@example.com',   password='123456', role=User.EXHIBITOR)
db.session.add(_u1)
db.session.add(_u2)
db.session.add(_u3)
db.session.add(_u4)
db.session.add(_u5)
db.session.add(_u6)

# Distributors
_d1 = Distributor(name='HAUT ET COURT', user = _u1)
_d2 = Distributor(name='PYRAMIDE',      user = _u2)
db.session.add(_d1)
db.session.add(_d2)

# Movies
_m1 = Movie(title="CITIZEN FOUR",
            releasedate=datetime.strptime("2015-03-04", DATE_FORMAT),
            distributor = _d1)
_m2 = Movie(title="LE DERNIER COUP DE MARTEAU",
            releasedate=datetime.strptime("2015-03-11", DATE_FORMAT),
            distributor = _d2)

_m3 = Movie(title="LA MAISON AU TOIT ROUGE",
            releasedate=datetime.strptime("2015-04-01", DATE_FORMAT),
            distributor = _d2)

db.session.add(_m1)
db.session.add(_m2)
db.session.add(_m3)

#DCP

_dcp1 = Dcp(name = "CitizenFour_FTR-5_F-178_EN-FR_INT_51_2K_HAUT_20150209_DGM_IOP_OV",
    contentkind=Dcp.FEATURE,
    size = 141800000,
    torrent_hash = '6aac41a7491f7813c360e3ebcd80048961edeab2',
    torrent_creation = datetime(2015,02,18,12,34,26) ,
    movie = _m1)

_dcp2 = Dcp(name = "DernierCoupMar_FTR-1_F_FR-FR_FR_51_4K_LION_20150118_DGM_IOP_OV",
    contentkind=Dcp.FEATURE,
    size = 147200000,
    torrent_hash = '5c4b1d4bbac61ff2cf4d54ff0de3b16cbfddbade',
    torrent_creation = datetime(2015,02,26,10,20,33) ,
    movie = _m2)

_dcp3 = Dcp(name = "DernierCoupMar_FTR-1_F_FR-FR-OCAP_FR_51_4K_LION_20150119_DGM_IOP_VF",
    contentkind=Dcp.FEATURE,
    size = 32000,
    torrent_hash = '7f8db0dadc148f9fbfdd79748d38e7281ddfc63b',
    torrent_creation = datetime(2015,02,26,9,36,46) ,
    movie = _m2)

_dcp4 = Dcp(name = "PYRAMIDE-DISTRIBUTION_LA-MAISON-AU-TOIT-ROUGE_2K-F_51-VO_ST-FR_DCP-24_CPT_1409TIT3138",
   contentkind=Dcp.FEATURE,
   movie = _m3)

db.session.add(_dcp1)
db.session.add(_dcp2)
db.session.add(_dcp3)
db.session.add(_dcp4)

# Exhibitors
_e1 = Exhibitor(name='Utopia Toulouse',      iptinc = '10.10.10.131', user = _u3)
_e2 = Exhibitor(name='Utopia Tournefeuille', iptinc = '10.10.10.32',  user = _u4)
_e3 = Exhibitor(name='Utopia Montpellier',   iptinc = '10.10.10.34',  user = _u5)
_e4 = Exhibitor(name='Utopia Bordeaux',      iptinc = '10.10.10.33',  user = _u6)

db.session.add(_e1)
db.session.add(_e2)
db.session.add(_e3)
db.session.add(_e4)

# Distribution
# Pour Citizen four
#    Utopia toulouse --> recus
_d = Distribution(exhibitor = _e1, dcp = _dcp1,
                 status = DistributionStatus.DL_FINISHED,
                 started_transfer_date = datetime.strptime("2015-02-10", DATE_FORMAT),
                 finished_transfer_date = datetime.strptime("2015-02-12", DATE_FORMAT)
                 )
db.session.add(_d)
#    Utopia tournefeuille --> non distribue
#    Utopia Montpellier   --> non distribue
#    Utopia Bordeaux      --> recus
_d = Distribution(exhibitor = _e4, dcp = _dcp1,
                 status = DistributionStatus.DL_FINISHED)
db.session.add(_d)

# Pour le dernier coup de marteau
#    Utopia toulouse --> recus
_d = Distribution(exhibitor = _e1, dcp = _dcp2,
                 status = DistributionStatus.DL_FINISHED)
db.session.add(_d)
_d = Distribution(exhibitor = _e1, dcp = _dcp3,
                 status = DistributionStatus.DL_FINISHED)
db.session.add(_d)
#    Utopia tournefeuille --> non distribue
#    Utopia Montpellier   --> recus
_d = Distribution(exhibitor = _e3, dcp = _dcp2,
                 status = DistributionStatus.DL_FINISHED)
db.session.add(_d)
_d = Distribution(exhibitor = _e3, dcp = _dcp3,
                 status = DistributionStatus.DL_FINISHED)
db.session.add(_d)

#    Utopia Bordeaux   --> recus
_d = Distribution(exhibitor = _e4, dcp = _dcp2,
                 status = DistributionStatus.DL_FINISHED)
db.session.add(_d)
_d = Distribution(exhibitor = _e4, dcp = _dcp3,
                 status = DistributionStatus.DL_FINISHED)
db.session.add(_d)


db.session.commit()

# test
rvs = _m1.get_distribution_authorisations()
for rv in rvs:
    print rv.exhibitor.name
    for _ddi in rv.dcp_distribution_items:
        print "    %s = %s"%(_ddi.dcp.name, _ddi.distribution_status.status)

rvs = _m2.get_distribution_authorisations()
for rv in rvs:
    print rv.exhibitor.name
    for _ddi in rv.dcp_distribution_items:
        print "    %s = %s"%(_ddi.dcp.name, _ddi.distribution_status.status)

print "Release date %r"%_m1.releasedate
db.session.remove()
app_context.pop()

