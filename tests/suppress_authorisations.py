#!/usr/bin/env python
import sys
sys.path.append('/home/nicolas/dev/tuco')
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]



from datetime import datetime
DATE_FORMAT="%Y-%m-%d"
from app import db, create_app

from app.models import User, Distributor, Movie, Exhibitor, Distribution, DistributionStatus
from app.models import Dcp
from app.admin.actions import do_newdcp, do_deletedcp
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()


TEST_DCPS=[
('DCP_CORNICHE-KENNE_FR-XX_IOP_DSN','FTR'),
('CORNICHE-KENNE_FTR_F_FR-FR-OCAP_FR_51-VI_2K_20160912_RCH_SMPTE','FTR'),
]


movie = Movie.query.filter_by(title = u'MON FILM').first()

for dcp_name, contentkind in TEST_DCPS:

    _dcp = Dcp.query.filter_by(name = dcp_name).first()
    if _dcp :
        _authors=Distribution.query.filter_by(dcpid = _dcp.dcpid).all()
        for item in _authors:
            if (_dcp.contentkind == Dcp.FEATURE) and (item.isauthorised):
                item.isauthorised = False
        db.session.commit()
    else:
        print "No dcp found for {}".format(dcp_name)


db.session.remove()
app_context.pop()
