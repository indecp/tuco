# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
import os
basedir = os.path.abspath(os.path.dirname(__file__))

import logging
from logging.handlers import SMTPHandler
from logging.handlers import SysLogHandler
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
class MySMTPHandler(SMTPHandler):

    def emit(self, record):
        """
        Emit a record.

        Format the record and send it to the specified addressees.
        """
        try:
            import smtplib
            from email.utils import formatdate
            port = self.mailport
            if not port:
                port = smtplib.SMTP_PORT
            smtp = smtplib.SMTP(self.mailhost, port, timeout=self._timeout)
            msg = MIMEMultipart()
            msg['Subject'] = self.getSubject(record)
            msg['From'] = u'{}'.format(self.fromaddr.decode('utf-8'))
            msg['To'] = u",".join(self.toaddrs)
            body = MIMEText(u"{}".format(self.format(record)), 'plain','utf-8')
            msg.attach(body)
            if self.username:
                if self.secure is not None:
                    smtp.ehlo()
                    smtp.starttls(*self.secure)
                    smtp.ehlo()
                smtp.login(self.username, self.password)
            smtp.sendmail(self.fromaddr, self.toaddrs, msg.as_string())
            smtp.quit()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


class Config:
    #SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    WTF_CSRF_ENABLED = True
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    MAIL_SERVER = os.environ.get('MAIL_SERVER') or 'localhost'
    MAIL_PORT = 25
    MAIL_USERNAME = None
    MAIL_PASSWORD = None

    TUCO_MAIL_SUBJECT_PREFIX = '[Inde-CP]'
    TUCO_MAIL_SENDER = u'Inde-CP <notification@indecp.org>'
    TUCO_ADMIN = os.environ.get('TUCO_ADMIN') or "lutins@tdcpb.org"
    TUCO_SUIVI = os.environ.get('TUCO_SUIVI') or u'Inde-CP <suivi@indecp.org>'
    TORRENT_FOLDER = os.path.join(basedir, 'torrents')
    DATE_FORMAT="%Y-%m-%d"

    XBT_HOST    = os.getenv('XBT_HOST')
    XBT_USER    = os.getenv('XBT_USER')
    XBT_PASS    = os.getenv('XBT_PASS')
    XBT_DB_NAME = os.getenv('XBT_DB_NAME')
    BABEL_DEFAULT_LOCALE ='fr'


    ERROR_404_HELP = False

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True

    WTF_CSRF_ENABLED = True
    BOOTSTRAP_SERVE_LOCAL = True

    # when set to True, it disable mail sending
    # at mail send level
    MAIL_SUPPRESS_SEND = False
    # mail server settings
    # for test
    # launch fake email server
    # python -m smtpd -n -c DebuggingServer localhost:2525
    MAIL_PORT = 2525

    TTD_DATABASE_URL=os.environ.get('TTD_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'ttd-test.sqlite')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')

    SQLALCHEMY_BINDS = {
                    'appdb': SQLALCHEMY_DATABASE_URI,
                    'torrentsdb': TTD_DATABASE_URL
                    }


    @classmethod
    def init_app(cls, app):
        Config.init_app(app)
        # log to syslog
        formatter = logging.Formatter('[TUCO] %(levelname)s %(message)s (%(filename)s %(funcName)s l %(lineno)d)')
        syslog_handler = SysLogHandler(address="/dev/log")
        syslog_handler.setLevel(logging.INFO)
        syslog_handler.setFormatter(formatter)
        app.logger.addHandler(syslog_handler)
        from logging.handlers import TimedRotatingFileHandler
        file_handler = TimedRotatingFileHandler('/var/log/tuco/tuco.log', when='D')
        file_handler.setLevel(logging.INFO)
        formatter = logging.Formatter('[TUCO] %(asctime)s %(levelname)s %(message)s (%(filename)s %(funcName)s l %(lineno)d)')
        syslog_handler = SysLogHandler(address="/dev/log")
        file_handler.setFormatter(formatter)
        app.logger.addHandler(file_handler)
        app.logger.warning('Starting tuco')


class TestingDevConfing(Config):
    DEBUG=True

    # mail server settings
    MAIL_USERNAME = None
    MAIL_PASSWORD = None

    TTD_DATABASE_URL=os.environ.get('TTD_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'ttd-test.sqlite')
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'tuco_sample.db')
    SQLALCHEMY_BINDS = {
                    'appdb': SQLALCHEMY_DATABASE_URI,
                    'torrentsdb': TTD_DATABASE_URL
                    }


class TestingConfig(Config):
    TESTING = True
    DEBUG=True

    # when set to True, it disable mail sending
    # at mail send level
    MAIL_SUPPRESS_SEND = False

    TTD_DATABASE_URL=os.environ.get('TTD_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'ttd-test.sqlite')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')
    SQLALCHEMY_BINDS = {
                    'appdb': SQLALCHEMY_DATABASE_URI,
                    'torrentsdb': TTD_DATABASE_URL
                    }


    WTF_CSRF_ENABLED = False
    SERVER_NAME = 'localhost:5000'

class ProductionConfig(Config):

    TTD_DATABASE_URL=os.environ.get('TTD_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'ttd-test.sqlite')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    SQLALCHEMY_BINDS = {
                    'appdb': SQLALCHEMY_DATABASE_URI,
                    'torrentsdb': TTD_DATABASE_URL
                    }


    @classmethod
    def init_app(cls, app):
        Config.init_app(app)
        # email errors to the administrators
        import logging
        from logging.handlers import SMTPHandler
        credentials = None
        secure = None
        if getattr(cls, 'MAIL_USERNAME', None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)
            if getattr(cls, 'MAIL_USE_TLS', None):
                secure = ()
        mail_handler = MySMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr=cls.TUCO_MAIL_SENDER,
            toaddrs="lutins@tdcpb.org",
            subject='[TUCO] application error',
            )
        _format = '[TUCO]  %(asctime)s %(levelname)s %(message)s (%(filename)s %(funcName)s l %(lineno)d)'
        formatter = logging.Formatter(fmt = _format)
        mail_handler.setFormatter(formatter)
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

class UnixConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # log to syslog
        import logging
        from logging.handlers import TimedRotatingFileHandler
        app.logger.setLevel(logging.INFO)
        file_handler = TimedRotatingFileHandler('/var/log/tuco/tuco.log', when='D')
        file_handler.setLevel(logging.INFO)
        _format =u'[TUCO] %(asctime)s %(levelname)s %(message)s (%(filename)s %(funcName)s l %(lineno)d)'.encode('utf-8')
        formatter = logging.Formatter(_format)
        file_handler.setFormatter(formatter)
        app.logger.addHandler(file_handler)

        app.logger.info(u'Starting tuco')


config = {
    'development': DevelopmentConfig,
    'testingdev':TestingDevConfing,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'unix': UnixConfig,
    'default': DevelopmentConfig
}

