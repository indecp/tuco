#!/usr/bin/env python
import sys
sys.path.append('.')
import os
from datetime import datetime, timedelta, date
import calendar
import operator

DAYS_AGO = 30
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

def sizeof_fmt(num, suffix='o'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def calc_range (year, month):
    start_date = date(year, month, 01)
    wd, days_in_month = calendar.monthrange(start_date.year, start_date.month)
    end_date = start_date + timedelta(days=days_in_month)
    return start_date, end_date

def StatsByMonth(year, month):
    _s, _e = calc_range(year, month)
    _distribution = Distribution.query.\
        filter(Distribution.finished_transfer_date >= _s,
                Distribution.finished_transfer_date <= _e).all()

    NB_TLR = 0
    NB_FTR = 0;
    size=0
    for _i in _distribution:
        if _i.dcp.contentkind == Dcp.TRAILER:
            NB_TLR+=1
        if _i.dcp.contentkind == Dcp.FEATURE and _i.dcp.size>1*2**30:
            NB_FTR+=1
        size = size + _i.dcp.size
    print "{:%b %Y} | {:>5} | {:>7} |  {}".\
            format(_s, NB_FTR, NB_TLR, sizeof_fmt(size))
    return NB_FTR, NB_TLR

def StatsByMonthWithAuthorisation(year, month):
    _s, _e = calc_range(year, month)
    _distribution = Distribution.query.\
        filter(Distribution.finished_transfer_date >= _s,
                Distribution.finished_transfer_date <= _e,
                ).all()

    NB_TLR = 0
    NB_FTR = 0;
    dict_dist={}
    for _i in _distribution:
        if _i.dcp.contentkind == Dcp.TRAILER:
            NB_TLR+=1
        if _i.dcp.contentkind == Dcp.FEATURE and _i.dcp.size>1*2**30:
            if _i.autorisation_date.year == 1970:
                print "{: <40} DL without autorisation by {}".format(_i.dcp.name[:40], _i.exhibitor.name)
            if _i.dcp.movie.distributor.name not in dict_dist:
                dict_dist[_i.dcp.movie.distributor.name] = 1
            else:
                dict_dist[_i.dcp.movie.distributor.name] += 1
            NB_FTR+=1

    sorted_x = sorted(dict_dist.items(), key=operator.itemgetter(1), reverse=True)
    for k in sorted_x:
        print u"{:<30}: {: <3}".format(k[0],k[1])

    return NB_FTR, NB_TLR


from app import db, create_app
from app.models import User, Distributor, Movie, Exhibitor, Dcp, Distribution, DistributionStatus, TrackerMessage, TdcpbServer
from app.torrent.xbt import XbtDB
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()
app.logger.warning('start stats')



print "         | films | trailer | Total "
res= StatsByMonth(2018,1)
res= StatsByMonth(2018,2)
res= StatsByMonth(2018,3)
res= StatsByMonth(2018,4)
res= StatsByMonth(2018,5)
res= StatsByMonth(2018,6)
res= StatsByMonth(2018,7)
res= StatsByMonth(2018,8)
res= StatsByMonth(2018,9)
res= StatsByMonth(2018,10)
res= StatsByMonth(2018,11)
res= StatsByMonth(2018,12)
res= StatsByMonth(2019,1)
res= StatsByMonth(2019,2)
res= StatsByMonth(2019,3)
res= StatsByMonth(2019,4)
res= StatsByMonth(2019,5)
res= StatsByMonth(2019,6)
res= StatsByMonth(2019,7)
res= StatsByMonth(2019,8)
res= StatsByMonth(2019,9)
res= StatsByMonth(2019,10)
res= StatsByMonth(2019,11)
res= StatsByMonth(2019,12)

app.logger.warning('finish stats')

db.session.remove()
app_context.pop()

