#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
import sys
sys.path.append('.')
import os
from datetime import datetime, timedelta
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from app import db, create_app
from app.models import Tag, Movie

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

app.logger.warning('Creation du Tag telerama')
tag = Tag.query.filter_by(name='telerama2018').first()
if not tag:
    tag = Tag(
        name=u"telerama2018",
        valid = True,
        longname = u"Festival Télérama 2018"
        )
    db.session.add(tag)
    db.session.commit()

#1001    120 BATTEMENTS PAR MINUTE
#1055    FAUTE D'AMOUR
#1069    LOGAN LUCKY
#1161    UN HOMME INTÈGRE
#889 VISAGES VILLAGES
#1091    L'ATELIER
#905 LE CAIRE CONFIDENTIEL
#1224    LA VILLA
#983 UNE VIE VIOLENTE
#985 UNE FEMME DOUCE
#586 CERTAINES FEMMES
#1350    LES BONNES MANIÈRES
#1000    JUSQU'À LA GARDE

films = [
1001,
1055,
1069,
1161,
889,
1091,
905,
1224,
983,
985,
586,
1350,
1000
]

for f in films:
    print f
    m = Movie.query.get(f)
    print m
    print tag
    m.tags.append(tag)


db.session.commit()



db.session.remove()
app_context.pop()

