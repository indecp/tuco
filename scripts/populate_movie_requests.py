#!/usr/bin/env python
import sys
sys.path.append('.')
from datetime import datetime, timedelta
import os

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.TorrentsModel import Client
from app.models import Movie, MovieRequest, MovieRequestStatus, MovieRequestDenyCause, Distribution
from flask_script import Manager, Shell

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

#for request in MovieRequest.query.all():
for request in MovieRequest.query.order_by(MovieRequest.id.desc()).limit(800):
    if request.is_autorised:
        #print u"ALLOWED {}".format(request)
        request.statusId = MovieRequestStatus.ALLOWED.value
        autor = Distribution.query.filter_by(exhibitor = request.exhibitor)
        for dcp in request.movie.get_features():
            res = autor.filter_by(dcp=dcp).first()
            if res.isauthorised:
                request.allow_date = res.autorisation_date
    else:
        if request.demand_date < datetime.now() -  timedelta(weeks =4):
            request.statusId = MovieRequestStatus.DENIED.value
            request.deny_date = datetime.now()
        else:
            request.statusId = MovieRequestStatus.PENDING.value
            request.deny_causeId = MovieRequestDenyCause.NO_REASON.value
    request.first_demand_date = request.demand_date

db.session.commit()
app_context.pop()
