# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
#!/usr/bin/env python
import sys
sys.path.append('.')
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]



from datetime import datetime
DATE_FORMAT="%Y-%m-%d"
from app import db, create_app

from app.models import User, Distributor, Movie, Exhibitor, Distribution, DistributionStatus
from app.models import Dcp, TrackerMessage, Short
from app.admin.actions import do_newdcp, do_deletedcp
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

DATETIME_0 = datetime.fromtimestamp(0)

query=db.session.query(Distribution)
query = query.filter(
            Distribution.started_transfer_date == DATETIME_0,
            )
res = query.all()
print u"Found {} results".format(len(res))

for r in res:
    r.started_transfer_date = None
    if r.finished_transfer_date == DATETIME_0:
        r.finished_transfer_date = None


query=db.session.query(Distribution)
query = query.filter(
            Distribution.finished_transfer_date == DATETIME_0,
            )
res = query.all()
print u"Found {} results".format(len(res))

for r in res:
    r.finished_transfer_date = None

db.session.commit()


db.session.remove()
app_context.pop()
