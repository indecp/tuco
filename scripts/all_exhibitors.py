# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
#!/usr/bin/env python
import sys
sys.path.append('/home/nicolas/dev/tuco')
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.TorrentsModel import Client
from app.models import Exhibitor
from app.filters import iptinc_to_web_filter
from flask_script import Manager, Shell


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

with open('/tmp/salles.csv', 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=';',
                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
    exhibs = Exhibitor.query.all()
    for _e in exhibs:
        url = iptinc_to_web_filter(_e.iptinc) 
        url = "http://".+url
        writer.writerow([
            _e.name.encode('utf-8'),
            url,
            _e.tc_login,
            _e.tc_password
            ])
    app_context.pop()
