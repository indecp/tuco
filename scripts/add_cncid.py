#!/usr/bin/env python
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.TorrentsModel import Client
from app.models import Exhibitor
from flask_script import Manager, Shell

SALLES_TUCO='/home/nicolas/data/tdcpb/webapi/salles_tuco.csv'

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, 'utf-8') for cell in row]


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()


with open(SALLES_TUCO, 'rb') as csvfile:
    csvreader = unicode_csv_reader(csvfile, delimiter=';', quotechar='|')
    for _l in csvreader:
        print _l[0], _l[3]
        _e = Exhibitor.query.filter_by(exhibitorid = _l[0]).one()
        _e.cncid = _l[3]
    db.session.commit()

app_context.pop()
