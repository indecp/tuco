#!/usr/bin/env python
import sys
sys.path.append('.')
from datetime import datetime, timedelta
import os

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.models import Movie, Dcp, Distribution
from flask_script import Manager, Shell


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
def make_shell_context():
    return dict(app=app, db=db, User=User)


@manager.command
@manager.option('-d', '--dcpid', help=u'DCP id')
def issue502(dcpid=None):
    _dcp = Dcp.query.get(dcpid)
    if _dcp is None:
        app.logger.error("No DCPr with id {}".format(dcpid))
        return
    if _dcp.movie :
        movie = _dcp.movie
        movie.nb_valid_ftr = movie.nb_ftrs()
        movie.nb_valid_tlr = movie.nb_tlrs()
        print u"Movie {}: NB FTR = {} NB TLR = {}".format(movie, movie.nb_valid_ftr , movie.nb_valid_tlr)
        db.session.commit()

    # create authorisations
    app.logger.info(u'creating authorisations for {}'.format(_dcp))
    Distribution.create_authorisations(_dcp)

if __name__ == '__main__':
    import sys
    import codecs
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf8')(sys.stderr)
    manager.run()

