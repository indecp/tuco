#!/usr/bin/env python
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.models import Distributor,Exhibitor,Dcp

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()


_items = Distributor.query.all()
for _i in _items:
    _i.valid = True

_items = Exhibitor.query.all()
for _i in _items:
    _i.valid = True

_items = Dcp.query.all()
for _i in _items:
    _i.valid = True


db.session.commit()


app_context.pop()
