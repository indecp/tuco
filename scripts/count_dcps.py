#!/usr/bin/env python
import sys
sys.path.append('.')
import os

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.TorrentsModel import Client
from app.models import Movie
from flask_script import Manager, Shell

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

for movie in Movie.query.all():
    count = movie.nb_ftrs()
    if count != movie.nb_valid_ftr:
        print u'{} Count FTR updated OLD = {} new = {}'.format(
                movie.title, movie.nb_valid_ftr, count)
        movie.nb_valid_ftr = count

    count = movie.nb_tlrs()
    if count != movie.nb_valid_tlr:
        print u'{} Count TLR updated OLD = {} new = {}'.format(
                movie.title, movie.nb_valid_ftr, count)
        movie.nb_valid_tlr = count

db.session.commit()

app_context.pop()
