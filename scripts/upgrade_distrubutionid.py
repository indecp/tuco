#!/usr/bin/env python
import sys
sys.path.append('.')
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.models import Exhibitor,Movie,Distribution, DistributionStatus, Dcp

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

idx = 1;
for _dist in Distribution.query.all():
    _dist.distributionid= idx
    idx+=1
    _dist.isauthorised = True

_items = Movie.query.all()
for _i in _items:
    for _d in _i.dcps:
        for _exh in Exhibitor.query.all():
            _distexh = Distribution.query.filter_by(exhibitorid = _exh.exhibitorid, dcpid = _d.dcpid).first()
            if not _distexh :
                if _d.contentkind != Dcp.FEATURE:
                    _new_dist= Distribution(
                            status = DistributionStatus.DL_AUTHORIZED,
                            isauthorised = True,
                            autorisation_date = _d.torrent_creation,
                            exhibitor = _exh,
                            distributionid = idx,
                            dcp = _d)
                else:
                    _new_dist= Distribution(
                            status = DistributionStatus.DL_NOT_AUTHORIZED,
                            isauthorised = False,
                            exhibitor = _exh,
                            distributionid = idx,
                            dcp = _d)
                idx+=1
                db.session.add(_new_dist)
                #print _new_dist.distributionid


db.session.commit()


app_context.pop()
