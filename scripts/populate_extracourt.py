#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
import sys
sys.path.append('.')
import os
from datetime import datetime, timedelta
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from app import db, create_app
from app.models import Catalogue
from app.models import Short

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

app.logger.warning('Creation du cataogue extracourt et mettre tout les shorts à valid')


cat = Catalogue(name=u"L'EXTRA COURT")
cat.valid = True
db.session.add(cat)

for s in Short.query.all():
    s.valid = True
    s.catalogues = [ cat]


db.session.commit()



db.session.remove()
app_context.pop()

