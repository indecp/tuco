# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
#!/usr/bin/env python
import sys
sys.path.append('/home/nicolas/dev/tuco')
import os
import csv

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]



from datetime import datetime
DATE_FORMAT="%Y-%m-%d"
from app import db, create_app

from app.models import User, Distributor, Movie, Exhibitor, Distribution, DistributionStatus
from app.models import Dcp, TrackerMessage, Short
from app.admin.actions import do_newdcp, do_deletedcp
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app_context = app.app_context()
app_context.push()

#dcps = Dcp.query.filter_by(contentkind = Dcp.SHORT).all()

#for dcp in dcps:
#    print dcp.exhibitors.all()
#    print ""

#    ds = Distribution.query.filter_by(dcp=dcp).all()
#    for d in ds:
#        print d
#        if not d.isauthorised:
#            print u"Not auhtorized: {} ".format(d)

#tm = TrackerMessage.query.filter_by(announce_id =34110158).first()
#print u'{}'.format(tm.exhibitor)
#print u'{}'.format(tm.dcp)

#ds = Distribution.query.filter_by(dcp=tm.dcp).all()
#print ds

for short in Short.query.all():
    for dcp in short.dcps:
        Distribution.create_authorisations(dcp)


db.session.remove()
app_context.pop()
