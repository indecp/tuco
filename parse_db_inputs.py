#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4
#

import sys
import csv
import os
import os.path
from pprint import pprint
import codecs
from datetime import datetime
from app.torrent.torrent_info import TorrentInfo

TORRENT_DIR="./torrents/"
DATE_FORMAT="%Y-%m-%d"

def search_torrent(p_torrent):
    for root, dirs, files in os.walk(TORRENT_DIR):
        for file in files:
            if file == p_torrent:
                return os.path.join(root, file)
    return None

class Parseinputs:

    def __init__(self,p_csv_path):
        print "Importing from %s"%p_csv_path
        self.data = []
        with open(p_csv_path, 'rb') as csvfile:
            self.reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
            for _row in self.reader:
                _urow = {unicode(k,'utf-8'):unicode(v,'utf-8') for k,v in _row.items()}
                self.data.append(_urow)

    def list_distributors(self):
        _distributors = []
        for _row in self.data:
            if _row['DISTRIBUTOR']:
                if _row['DISTRIBUTOR'].upper() not in _distributors:
                    _distributors.append(_row['DISTRIBUTOR'].upper())
        return _distributors

    def list_torrents(self):
        _torrents = []
        for _row in self.data:
            if _row[u'TORRENT']:
                _torrent = {}
                _torrent[u'name'] = _row[u'TORRENT']
                if _row[u'ALLOCINE_TITRE']:
                    _torrent[u'movie_title'] = _row[u'ALLOCINE_TITRE'].upper()
                else:
                    _torrent[u'movie_title'] = _row[u'ORIGINAL_TITLE'].upper()
                _path = search_torrent( _torrent[u'name']+'.torrent')
                if _path is None:
                    print "No torrent for %s"%_torrent[u'name']
                    continue
                _info = TorrentInfo.info(_path)
                _torrent[u'size'] = _info['size']
                _torrent[u'hash'] = _info['hash']
                _torrent[u'torrent_creation'] = _info['creation_date']
                _torrents.append(_torrent)
        return _torrents


    def list_movies(self):
        _movies = []
        for _row in self.data:
            if _row[u'ORIGINAL_TITLE']:
                _movie = {}
                _movie[u'original_title'] = _row[u'ORIGINAL_TITLE'].upper()
                if _row[u'ALLOCINE_TITRE']:
                    _movie[u'title'] = _row[u'ALLOCINE_TITRE'].upper()
                else:
                    _movie[u'title'] = _row[u'ORIGINAL_TITLE'].upper()
                _movie[u'distributor'] = _row[u'DISTRIBUTOR'].upper()
                _movie[u'releasedate'] = datetime.strptime( _row[u'RELEASE_DATE'], DATE_FORMAT)
                if _movie not in _movies:
                    _movies.append(_movie)
                else :
                    print "%s already registered"%_row[u'ORIGINAL_TITLE'].encode('utf-8')
        return _movies

    def list_salles(self):
        _salles = []
        for _row in self.data:
            if _row[u'SALLE']:
                _salle = {
                    u'login':       _row[u'login'].strip(),
                    u'email':       _row[u'email'].strip(),
                    u'password':    _row[u'password'].strip(),
                    u'name':        _row[u'SALLE'].strip(),
                    u'iptinc':      _row[u'IPTINC'].strip(),
                    u'city':        _row[u'city'].strip(),
                    u'dept':        _row[u'dept'].strip(),
                    }
            _salles.append(_salle)
        return _salles

    def list_serveurs(self):
        _serveurs = []
        for _row in self.data:
            if _row[u'SERVEUR']:
                _serveur = {
                    u'name': _row[u'SERVEUR'].strip(),
                    u'iptinc': _row[u'IPTINC'].strip()
                    }

                _serveurs.append(_serveur)
        return _serveurs

    def list_trailers(self):
        _trailers = []
        for _row in self.data:
            if _row[u'TORRENT']:
                _path = search_torrent( _row[u'TORRENT']+'.torrent')
                if _path is None:
                    print "No torrent for %s"._torrent[u'name']
                _info = TorrentInfo.info(_path)
                _trailer = {
                    u'name'             : _row[u'TORRENT'].strip(),
                    u'movie_title'      : _row[u'MOVIE'].strip().upper(),
                    u'size'             : _info['size'],
                    u'hash'             : _info['hash'],
                    u'torrent_creation' : _info['creation_date']}
                _trailers.append(_trailer)
        return _trailers

def main(argv):

    UTF8Writer = codecs.getwriter('utf8')
    sys.stdout = UTF8Writer(sys.stdout)

    obj = Parseinputs(argv[1])
    _torrents = obj.list_torrents()
    pprint (_torrents)
if __name__ == "__main__":
    sys.exit(main(sys.argv))
