import os.path

from flask import render_template, redirect, url_for
from flask_login import current_user
from . import main
from ..auth.forms import LoginForm
from ..auth.views import do_login

@main.route('/', methods=["GET", "POST"])
@main.route('/index', methods=["GET", "POST"])
def index():
    if current_user.is_authenticated():
        if current_user.is_exhibitor:
            return redirect(url_for('exhibitor.home'))
        elif current_user.is_distributor:
            return redirect(url_for('distributor.home'))
        elif current_user.is_admin:
            return redirect(url_for('admin.home'))
    else:
        form = LoginForm()
        if form.validate_on_submit():
            return do_login(form)
        return render_template('index.html',
                               form=form)
