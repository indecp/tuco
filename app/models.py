# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import os.path
import base64
from enum import Enum

from . import db, ma, login_manager
from app.exceptions import TucoException
from flask import flash, current_app, render_template
from flask_login import UserMixin, AnonymousUserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from torrent.xbt import XbtDB
from datetime import datetime
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm.exc import NoResultFound
from email import send_email, send_email_wo_template, build_to, build_cc

class ModelException(TucoException):
    def __init__(self, message, status_code=None):
        super(ModelException, self).__init__(message, status_code )

class TrackerMessage(db.Model):
    __bind_key__ ='appdb'
    tracker_message_id       = db.Column(db.Integer, primary_key=True)
    event       = db.Column(db.Integer)
    hash        = db.Column(db.String(40))
    stamp       = db.Column(db.DateTime)
    ip          = db.Column(db.String(15))
    left0       = db.Column(db.BigInteger)
    announce_id = db.Column(db.BigInteger)
    downloaded  = db.Column(db.Integer)
    dcpid = db.Column(db.Integer, db.ForeignKey('dcp.dcpid'))
    exhibitorid = db.Column(db.Integer, db.ForeignKey('exhibitor.exhibitorid'))



    def __repr__(self):
        return u'<TrackerMessage {} {} {}>'.format(self.announce_id, self.dcp, self.exhibitor)

    @classmethod
    def update_xbt(self, since):
        _xbt = XbtDB()
        for _res in _xbt.get_started_and_complete_downloads(since):
            _indb = TrackerMessage.query.filter_by(announce_id = _res['id']).first()
            if _indb:
                dcp = Dcp.query.filter_by(torrent_hash = _indb.hash).first()
                if dcp and (_indb.dcp is None) :
                    _indb.dcp = dcp
                    db.session.commit()

                exhibitor = Exhibitor.query.filter_by(iptinc = _indb.ip).first()
                if exhibitor and (_indb.exhibitor is None):
                    _indb.exhibitor = exhibitor
                    db.session.commit()
            else:
                _tm = TrackerMessage(
                    event       = _res['event'],
                    hash        = _res['hash'],
                    stamp       = _res['ts'],
                    ip          = "%s"%_res['ipa'],
                    left0       = _res['left0'],
                    announce_id = _res['id'],
                    downloaded  = _res['downloaded'])

                exhibitor = Exhibitor.query.filter_by(iptinc = "%s"%_res['ipa']).first()
                if exhibitor is not None :
                    _tm.exhibitor = exhibitor
                else:
                    msg =  u"No exhibitor found with ip  {}".format(_res['ipa'])
                    current_app.logger.info(msg)
                # TODO: for statistical purpose add server downloads: use class TdcpbServer

                dcp = Dcp.query.filter_by(torrent_hash = _res['hash']).first()
                if dcp is not None :
                    _tm.dcp = dcp
                else:
                    msg =  u"Not found DCP for {}".format(_res['hash'])
                    current_app.logger.info(msg)
                db.session.add(_tm)
                db.session.commit()

    @classmethod
    def update(cls, since = None):
        cls.update_xbt(since)

        tracker_messages = TrackerMessage.query.filter(cls.stamp > since).all()
        _msg = "Analysing {} tracker messages".format(len(tracker_messages))
        current_app.logger.info(_msg)
        for _tm in tracker_messages:
            # Skip unknown Dcps
            if _tm.dcp is None:
                continue
            # skip unknown exhibitor
            if _tm.exhibitor is None:
                continue
            _author = Distribution.query.filter_by(exhibitor = _tm.exhibitor, dcp= _tm.dcp).first()
            # alert DL without Distribution db entry
            if _author is None:
                _msg = u"No distribution authorisation found for {}".format(_tm)
                current_app.logger.info(_msg)
                continue
            # alert DL without authorisation
            if not _author.isauthorised:
                if _tm.dcp.valid == True:
                    _msg = u"Distribution not authorized for {}".format(_tm)
                    current_app.logger.info(_msg)
                continue
            if (_tm.event == XbtDB.STARTED):
                if (_author.status == DistributionStatus.DL_FINISHED):
                    _msg = u"TRACKER in start but DL already finished (distributionid {} )".\
                        format(_author.distributionid)
                    current_app.logger.debug(_msg)
                elif (_author.status == DistributionStatus.DL_STARTED) :
                    pass
                elif (_author.status == DistributionStatus.DL_NOT_AUTHORIZED):
                    _msg = u"TRACKER  DL started for an NON AUTHORIZED CONTENT".\
                        format(_author.distributionid)
                    current_app.logger.info(_msg)
                else:
                    _author.status = DistributionStatus.DL_STARTED
                    _author.started_transfer_date = _tm.stamp
                    db.session.commit()
            if (_tm.event == XbtDB.COMPLETED):
                if _author.status == DistributionStatus.DL_STARTED:
                    _author.status = DistributionStatus.DL_FINISHED
                    _author.finished_transfer_date = _tm.stamp
                    db.session.commit()
                if _author.status == DistributionStatus.DL_FINISHED:
                    #DL already finished
                    _author.finished_transfer_date = _tm.stamp
                    db.session.commit()
                else:
                    _msg= u"TRACKER completed({}) event. Invalid Distribution state {} for {} ({}) ".\
                            format(_tm.event, _author.status, _author, _tm)
                    current_app.logger.info(_msg)



class DistributionStatus(object):
    DL_NOT_AUTHORIZED = 0x00
    DL_AUTHORIZED     = 0x01
    DL_STARTED        = 0x02
    DL_FINISHED       = 0x04
    DL_ERROR          = 0x80

class Distribution(db.Model):
    __bind_key__ ='appdb'
    __tablename__ = 'Distribution'

    dcpid = db.Column(db.Integer, db.ForeignKey('dcp.dcpid'), primary_key=True)
    exhibitorid = db.Column(db.Integer, db.ForeignKey('exhibitor.exhibitorid'), primary_key=True)
    distributionid = db.Column(db.BigInteger, index=True, unique = True, autoincrement = True, nullable = False)
    isauthorised  = db.Column(db.Boolean, default=False, nullable = False)
    status =      db.Column(db.Integer, default= DistributionStatus.DL_AUTHORIZED)
    autorisation_date = db.Column(db.DateTime, default = None)
    started_transfer_date = db.Column(db.DateTime, default = None)
    finished_transfer_date = db.Column(db.DateTime, default = None)
    last_reminder = db.Column(db.DateTime, default = None)

    def __repr__(self):
        return u'<Distribution> {} {} {} '.format(self.distributionid, self.dcp.name, self.exhibitor.name )


    @property
    def last_update(self):
        return max(self.autorisation_date, self.started_transfer_date, self.finished_transfer_date)

    @classmethod
    def create_authorisations(cls, p_dcp):
        for _e in Exhibitor.query.all():
            resq =cls.query.filter_by(dcp=p_dcp, exhibitor=_e).first()
            if resq:
                msg =  "Distribution item already exists: {}".format(resq)
                current_app.logger.info(msg)
                continue
            _new = cls(dcp = p_dcp,
                       exhibitor =  _e)
            if p_dcp.contentkind in (Dcp.TRAILER, Dcp.SHORT) :
                _new.isauthorised = True
                _new.status = DistributionStatus.DL_AUTHORIZED
            elif p_dcp.contentkind == Dcp.FEATURE:
                _new.isauthorised = False
                _new.status = DistributionStatus.DL_NOT_AUTHORIZED

            db.session.add(_new)
        db.session.commit()

    @classmethod
    def create_new_user_authorisations(cls, e):
        for dcp in Dcp.query.all():
            _new = cls(dcp = dcp,
                       exhibitor = e)
            if dcp.valid :
                if dcp.contentkind in (Dcp.TRAILER, Dcp.SHORT) :
                    _new.isauthorised = True
                    _new.status = DistributionStatus.DL_AUTHORIZED
                elif dcp.contentkind == Dcp.FEATURE:
                    _new.isauthorised = False
                    _new.status = DistributionStatus.DL_NOT_AUTHORIZED
            else:
                    _new.isauthorised = False
                    _new.status = DistributionStatus.DL_NOT_AUTHORIZED

            db.session.add(_new)
        db.session.commit()




class Permission:
    EXHIBIT    = 0x01
    DISTRIBUTE = 0x02
    USEAPI     = 0x04
    ADMINISTER = 0x80


class User(db.Model, UserMixin):
    __bind_key__ ='appdb'
    DISTRIBUTOR = 'Distributor'
    EXHIBITOR   = 'Exhibitor'
    APIUSER     = 'Apiuser'
    ADMINISTRATOR= 'Administrator'
    roles = {
            EXHIBITOR    : Permission.EXHIBIT,
            DISTRIBUTOR  : Permission.DISTRIBUTE,
            APIUSER      : Permission.USEAPI,
            ADMINISTRATOR: 0xff,
            }


    userid        = db.Column(db.Integer, primary_key=True)
    email         = db.Column(db.String(120), index=True, unique=True)
    email2         = db.Column(db.String(120))
    email3         = db.Column(db.String(120))
    login         = db.Column(db.String(64), index=True, unique=True)

    password_hash = db.Column(db.String(128))
    confirmation_requested     = db.Column(db.Boolean, default=False)
    confirmed     = db.Column(db.Boolean, default=False)
    role          = db.Column(db.Integer)
    last_seen     = db.Column(db.DateTime(), default=datetime.utcnow)
    distributor   = db.relationship('Distributor',
                                  uselist=False, backref="user")
    exhibitor     = db.relationship('Exhibitor',
                                  uselist=False, backref="user")
    properties    = db.relationship('UserProperty', backref='user',
                                   lazy='dynamic')

    def __init__(self, login, email, confirmation_requested = False, user_role ='Exhibitor'):
        self.login = login
        self.email = email
        self.user_role = user_role

    def __repr__(self):
        return '<User %r>' % self.login

    def get_id(self):
            return unicode(self.userid)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    def get_role(self):
        if self.role == Permission.EXHIBIT:
            return self.EXHIBITOR
        elif self.role == Permission.DISTRIBUTE:
            return self.DISTRIBUTOR
        elif self.role == Permission.USEAPI:
            return self.APIUSER
        elif self.role == 0xff:
            return self.ADMINISTRATOR
        else:
            raise AttributeError('role is not defined')

    def set_role(self, role):
        if role in self.roles:
            self.role = self.roles[role]
        else:
            raise AttributeError('role {} is not defined'.format(role))

    user_role = property(get_role, set_role)

    def can(self, permissions):
        return self.role is not None and \
            (self.role & permissions) == permissions

    @property
    def is_distributor(self):
        return self.role == self.roles['Distributor']

    @property
    def is_exhibitor(self):
        return self.role == self.roles['Exhibitor']

    @property
    def is_apiuser(self):
        return self.role == self.roles[self.APIUSER]

    @property
    def is_admin(self):
        return self.role == self.roles['Administrator']

    @property
    def emails(self):
        _emails= []
        if self.email:
            _emails = _emails + [ x.strip() for x in self.email.split(',')]
        if self.email2:
            _emails = _emails + [ x.strip() for x in self.email2.split(',')]
        if self.email3:
            _emails = _emails + [ x.strip() for x in self.email3.split(',')]
        print _emails
        return _emails

    def ping(self):
        self.last_seen = datetime.utcnow()
        db.session.add(self)

    def generate_confirmation_token(self, expiration=3600*48):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.userid})

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.userid:
            return False
        return True

    # method with auth with token:
    # cf . http://blog.miguelgrinberg.com/post/restful-authentication-with-flask
    # Both methods used for api authentication
    def generate_auth_token(self, expiration = 600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in = expiration)
        header = s.make_header( {})
        return { 'token': s.dumps({ 'id': self.userid }),
                 'expiration': header['exp'] }


    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user


@login_manager.user_loader
def load_user(userid):
    return User.query.get(int(userid))

@login_manager.request_loader
def load_user_from_request(request):
    # next, try to login using Basic Auth
    api_key = request.headers.get('Authorization')
    if api_key:
        api_key = api_key.replace('Basic ', '', 1)
        try:
            api_key = base64.b64decode(api_key)
        except TypeError:
            pass
        _l, _p = api_key.split(':')
        user = User.query.filter_by(login=_l).first()
        if user and  user.check_password(_p):
            return user
    # finally, return None if  methods did not login the user
    return None



class AnonymousUser(AnonymousUserMixin):

    def can(self, permissions):
        return False

    @property
    def is_distributor(self):
        return False
    @property
    def is_administrator(self):
        return False

    @property
    def is_exhibitor(self):
        return False

login_manager.anonymous_user = AnonymousUser


class Distributor(db.Model):
    __bind_key__ ='appdb'

    CODE_AGENCECM = 9797

    distributorid = db.Column(db.Integer, primary_key=True)
    valid    = db.Column(db.Boolean, default=True)
    name = db.Column(db.String(120), index=True, unique=True)
    isdcpbay = db.Column(db.Boolean, default = False)
    contact = db.Column(db.String(120))
    email = db.Column(db.String(120))
    phone = db.Column(db.String(20))
    cnc_code = db.Column(db.Integer)
    userid = db.Column(db.Integer, db.ForeignKey('user.userid'))
    movies = db.relationship('Movie', backref=db.backref('distributor', lazy="joined"), lazy='dynamic')
    shorts = db.relationship('Short', backref=db.backref('distributor', lazy="joined"), lazy='dynamic')
    demands = db.relationship('MovieRequest', backref='distributor', lazy='dynamic')

    def __repr__(self):
        return u'<Distributor %r>' % self.name

    def __str__(self):
        return u'{}'.format(self.name)
    def get_emails(self):
        _emails = []
        if self.is_in_dcpbay :
            if self.user is None:
                _msg = u" Official Distributor {}  shall have a user account".format(self.name)
                current_app.logger.warning(_msg)
            else:
                _emails = self.user.emails
        else :
            if self.user is not None:
                _msg = u" Non Official Distributor {} shall not have a user account ({})".format(self.name, self.user.login)
                current_app.logger.warning(_msg)
                _emails = self.user.emails
            if self.email:
                _emails.append(self.email)
        if _emails == []:
            _msg = u'Distributor {} have no email. Official: {}'.format(self.name, self.is_in_dcpbay)
            raise ModelException(_msg)
        return _emails


    @property
    def is_in_dcpbay(self):
        return self.isdcpbay

    @property
    def is_agencecm(self):
        if (self.cnc_code is not None) and (self.cnc_code == self.CODE_AGENCECM):
            return True
        return False

    @classmethod
    def all_valid(cls):
        return cls.query.filter_by(valid = True).all()

class Exhibitor(db.Model):
    __bind_key__ ='appdb'
    exhibitorid = db.Column(db.Integer, primary_key=True)
    valid    = db.Column(db.Boolean, default=True)
    name = db.Column(db.String(120), index=True, unique=True)
    address = db.Column(db.String(200))
    contact = db.Column(db.String(120))
    city = db.Column(db.String(200))
    dept = db.Column(db.Integer)
    phone = db.Column(db.String(40))
    zipcode = db.Column(db.String(20))
    nbscreens = db.Column(db.Integer)
    iptinc = db.Column(db.String(15), unique=True)
    tc_login = db.Column(db.String(64))
    tc_password = db.Column(db.String(128))
    cncid = db.Column(db.Integer)
    userid = db.Column(db.Integer, db.ForeignKey('user.userid'))
    dcps = db.relationship('Distribution',
                                foreign_keys = [Distribution.exhibitorid],
                                backref=db.backref('exhibitor', lazy='joined'),
                                lazy = 'dynamic',
                                cascade='all, delete-orphan')
    messages = db.relationship('TrackerMessage',
                                foreign_keys = [TrackerMessage.exhibitorid],
                                backref=db.backref('exhibitor', lazy='joined'),
                                lazy = 'dynamic',
                                cascade='all, delete-orphan')

    demands = db.relationship('MovieRequest', backref='exhibitor', lazy='dynamic')

    def __repr__(self):
        return u'<Exhibitor %r>' % self.name

    def __str__(self):
        return u'{} - {}'.format(self.city, self.name)

    @property
    def standard_name(self):
        return u'{} - {}'.format(self.city, self.name)


    def get_movies_list_with_status(self):
        _movie_distribution_items = []
        _movies =  Movie.query.all()
        for _movie in _movies:
            _dcps_id = [ _dcp.dcpid for _dcp in _movie.dcps ]
            _rvs = Distribution.query.join(Dcp,Movie).filter(
                        Distribution.dcpid.in_(_dcps_id),
                        Distribution.exhibitorid == self.exhibitorid).\
                        order_by(Movie.releasedate.desc()).all()

            _movie_distribution_items.append(
                MovieDistributionItem(exhibitor = self,
                                      movie = _movie,
                                      dcp_distribution_items = _rvs)
                                    )
        return _movie_distribution_items

    def get_movies_list_with_statusv2(self):
        _movie_distribution_items = []
        dcps = Dcp.query.join(Movie).\
                filter(Dcp.contentkind == Dcp.FEATURE,
                Dcp.valid == True).\
                order_by(Movie.releasedate.desc()).all()
        _rvs = []
        for _d in dcps:
            _distribution=Distribution.query.\
                filter_by(exhibitorid = self.exhibitorid, dcpid=_d.dcpid).\
                first()
            _rvs.append({'dcp':_d,'distribution':_distribution})
        return _rvs

    @classmethod
    def count_exhibitors(cls):
        _nb_exhibitors = cls.query.count()
        _nb_screens = 0
        for _e in cls.query.all():
            _nb_screens += _e.nbscreens
        return (_nb_exhibitors, _nb_screens)

    @classmethod
    def all_valid_exhibitors(cls):
        return cls.query.filter_by(valid = True).all()

    @property
    def startauto(self):
       prop = self.user.properties.filter_by(name = UserProperty.STARTAUTO).first()
       if not prop:
           return False
       if prop.value == "True":
           return True
       return False

    @startauto.setter
    def startauto(self, value):
        prop = self.user.properties.filter_by(name = UserProperty.STARTAUTO).first()
        if not prop:
            prop = UserProperty(
                    name = UserProperty.STARTAUTO,
                    value = u'dummy',
                    user = self.user
                    )
            db.session.add(prop)
        prop.value = UserProperty.set_bool(value)
        db.session.commit()


class DcpDistributionItem():
    exhibitor = None;
    dcp = None
    distribution_status = None

    def __init__(self, exhibitor, dcp, distribution_status):
        self.exhibitor = exhibitor
        self.dcp = dcp
        self.distribution_status = distribution_status
    def __repr__(self):
        return '<DcpDistrubutionItem %r %r %r >'%( self.dcp,
                                                self.exhibitor,
                                                self.distribution_status)

movie_catalogues = db.Table('movie_catalogue',
                db.Column('catalogue_id', db.Integer, db.ForeignKey('catalogue.id')),
                db.Column('movie_id', db.Integer, db.ForeignKey('movie.movieid')),
                )

short_catalogues = db.Table('short_catalogue',
                db.Column('catalogue_id', db.Integer, db.ForeignKey('catalogue.id')),
                db.Column('short_id', db.Integer, db.ForeignKey('short.shortid')),
                )


class Catalogue(db.Model):
    __bind_key__ ='appdb'
    __tablename__ = 'catalogue'
    id = db.Column(db.Integer, primary_key=True)
    valid    = db.Column(db.Boolean, default=True)
    name = db.Column(db.String(511))

class MovieDistributionItem():
    exhibitor = None
    dcp_distribution_items = []
    movie = None

    def __init__(self, exhibitor = None , movie = None, dcp_distribution_items =[]):
        self.exhibitor = exhibitor
        self.movie = movie
        self.dcp_distribution_items = dcp_distribution_items


short_tags = db.Table('short_tag',
                db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
                db.Column('short_id', db.Integer, db.ForeignKey('short.shortid')),
                )

class Short(db.Model):
    __bind_key__ ='appdb'
    shortid = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(512))
    director = db.Column(db.String(512))
    valid    = db.Column(db.Boolean, default=True)
    distributorid = db.Column(db.Integer, db.ForeignKey('distributor.distributorid'))
    dcps = db.relationship('Dcp', backref='short', lazy='dynamic')
    catalogues = db.relationship('Catalogue', secondary=short_catalogues,
                           backref=db.backref('shorts', lazy='dynamic'))
    tags = db.relationship('Tag', secondary=short_tags,
                           backref=db.backref('shorts', lazy='dynamic'))

    extra = db.relationship('Extra', uselist= False, back_populates = "short")
    def __repr__(self):
        return '<Short %r>' % self.title
    def get_all_dcps(self):
        return self.dcps.filter(Dcp.valid == True).all()

    @hybrid_property
    def nb_dcps(self):
        return self.dcps.filter( Dcp.valid == True).count()

    def get_dcps(self, only_valid = True):
        if only_valid == True:
            return self.dcps.filter(Dcp.valid == True).all()
        else:
            return self.dcps.all()




movie_tags = db.Table('movie_tag',
                db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
                db.Column('movie_id', db.Integer, db.ForeignKey('movie.movieid')),
                )


class Movie(db.Model):
    __bind_key__ ='appdb'

    # constants
    UNOFFICIAL = u'UNOFFICIAL'
    OFFICIAL_WITHOUT_FTR = u'OFFICIAL_WITHOUT_FTR'
    OFFICIAL_WITH_FTR = u'OFFICIAL_WITH_FTR'


    movieid = db.Column(db.Integer, primary_key=True)
    valid    = db.Column(db.Boolean, default=True)
    title = db.Column(db.String(512))
    original_title = db.Column(db.String(512))
    releasedate = db.Column(db.Date)
    nb_valid_ftr = db.Column(db.Integer, default=0)
    nb_valid_tlr = db.Column(db.Integer, default=0)
    distributorid = db.Column(db.Integer, db.ForeignKey('distributor.distributorid'))
    dcps = db.relationship('Dcp', backref='movie', lazy='dynamic')
    demands = db.relationship('MovieRequest', backref='movie', lazy='dynamic')
    catalogues = db.relationship('Catalogue', secondary=movie_catalogues,
                           backref=db.backref('movies', lazy='dynamic'))
    tags = db.relationship('Tag', secondary=movie_tags,
                           backref=db.backref('movies', lazy='dynamic'))
    extra = db.relationship('Extra', uselist= False, back_populates = "movie")


    def __repr__(self):
        return u'<Movie> {}'.format(self.title)

    def __str__(self):
        return u'{}'.format(self.title)


    def is_in_distribution(self, exhibitor):
        for _dcp in self.dcps:
            if _dcp.is_in_distribution(exhibitor):
                return True
        return False

    @hybrid_property
    def nb_dcps(self):
        return len(self.dcps.all())

    def nb_tlrs(self, only_valid = True):
        if only_valid:
            return self.dcps.filter(Dcp.contentkind == Dcp.TRAILER, Dcp.valid == True).count()
        else :
            return self.dcps.filter(Dcp.contentkind == Dcp.TRAILER).count()

    def nb_ftrs(self, only_valid = True):
        if only_valid:
            return self.dcps.filter(Dcp.contentkind == Dcp.FEATURE, Dcp.valid == True).count()
        else :
            return self.dcps.filter(Dcp.contentkind == Dcp.FEATURE).count()

    def get_all_dcps(self, only_valid = True):
        if only_valid == True:
            return self.dcps.filter(Dcp.valid == True).all()
        else:
            return self.dcps.all()

    def get_trailers(self):
        return self.dcps.filter(Dcp.valid == True, Dcp.contentkind == Dcp.TRAILER).all()

    def get_features(self):
        return self.dcps.filter(Dcp.valid == True, Dcp.contentkind == Dcp.FEATURE).all()



    def add_dcps_for_distribution(self, exhibitor, selected_dcps = None):
        """
        if dcp_type is None all the Dcps for the current movie will be added
        for distribution
        """
        current_app.logger.info(u'selected_dcps {}'.format(selected_dcps))
        _added_authorisations = []
        if selected_dcps is None:
            selected_dcps = self.dcps
        else:
            for _d in selected_dcps:
                if _d not in self.dcps:
                    current_app.logger.error(u'DCP {} not a DCP of {} '.\
                            format(_d, self.title))
                    return []

        _selected_dcps = [ d.dcpid for d in selected_dcps]
        current_app.logger.info(u'selected_dcps {}'.format(_selected_dcps))
        _authorisations = Distribution.query.\
                filter( Distribution.dcpid.in_(_selected_dcps),
                        Distribution.exhibitor == exhibitor).\
                all()

        for _a in _authorisations:
            if _a.isauthorised and _a.dcp.contentkind == Dcp.FEATURE:
                current_app.logger.\
                    info(u'Already in distribution {} ({}) to {}'. format(\
                        _a.dcp.name,
                        _a.dcp.contentkind,
                        _a.exhibitor.name))
                continue
            _a.isauthorised = True
            _a.status = DistributionStatus.DL_AUTHORIZED
            _a.autorisation_date = datetime.utcnow()
            _added_authorisations.append(_a)
            db.session.commit()
            current_app.logger.info(u'Distribute {} {} to {}'.format(\
                _a.dcp.contentkind,
                _a.dcp.name,
                _a.exhibitor.name))
        return _added_authorisations


    def movies_on_distribution(self):
        _list = []
        for _dcp in self.dcps:
            data ={}
            data['name'] = _dcp.name
            res = Distribution.query.filter_by(dcpid=_dcp.dcpid).all()
            data['exhibitors'] = [ r.exhibitor.name for r in res]
            _list.append(data)
        return _list

    def get_distribution_authorisations(self):
        """
            returns a list authorisation status for each exhibitor.
        """
        _movie_distribution_items = []
        for _exhibitor in Exhibitor.query.all():
            _movie_distribution_items.append(
                MovieDistributionItem(exhibitor = _exhibitor,
                                      dcp_distribution_items = [])
                                            )

        # Essayer avec:
        # q = (db.session.query(Dcp, Distribution, Exhibitor).join(Distribution).join(Exhibitor)).all()
        # cf stackoverflow.com/questions/21996288/join-multiple-tables-in-sqlalchemy-flask
        # Encore mieux :
        # q = (db.session.query(Movie, Dcp, Distribution, Exhibitor).join(Dcp).join(Distribution).join(Exhibitor)).filter(Movie.movieid == 2).all()
        # et aussi voir; http://stackoverflow.com/questions/2524600/how-do-i-join-three-tables-with-sqlalchemy-and-keeping-all-of-the-columns-in-one
        _rows = (db.session.query(Movie, Dcp, Distribution, Exhibitor).join(Dcp).join(Distribution).join(Exhibitor)).filter(Movie.movieid == self.movieid).all()
        for _movie, _dcp, _distribution, _exhibitor  in _rows:
            _ddi = DcpDistributionItem(_exhibitor, _dcp, _distribution)
            for _mdi in _movie_distribution_items:
                if _exhibitor == _mdi.exhibitor:
                    _mdi.dcp_distribution_items.append(_ddi)
                    break
        return _movie_distribution_items

    def get_distribution_authorisationsV2(self, selected_dcps):
        """
            returns a list authorisation status for each exhibitor.
        """
        _movie_distribution_items = []
        for _exhibitor in Exhibitor.query.all():
            _movie_distribution_items.append(
                MovieDistributionItem(exhibitor = _exhibitor,
                                      dcp_distribution_items = [])
                                            )

        # Essayer avec:
        # q = (db.session.query(Dcp, Distribution, Exhibitor).join(Distribution).join(Exhibitor)).all()
        # cf stackoverflow.com/questions/21996288/join-multiple-tables-in-sqlalchemy-flask
        # Encore mieux :
        # q = (db.session.query(Movie, Dcp, Distribution, Exhibitor).join(Dcp).join(Distribution).join(Exhibitor)).filter(Movie.movieid == 2).all()
        # et aussi voir; http://stackoverflow.com/questions/2524600/how-do-i-join-three-tables-with-sqlalchemy-and-keeping-all-of-the-columns-in-one
        _rows = (db.session.query(Movie, Dcp, Distribution, Exhibitor).join(Dcp).join(Distribution).join(Exhibitor)).filter(Movie.movieid == self.movieid).all()
        for _movie, _dcp, _distribution, _exhibitor  in _rows:
            if _dcp not in selected_dcps:
                continue
            _ddi = DcpDistributionItem(_exhibitor, _dcp, _distribution)
            for _mdi in _movie_distribution_items:
                if _exhibitor == _mdi.exhibitor:
                    _mdi.dcp_distribution_items.append(_ddi)
                    break
        return _movie_distribution_items


    def get_torrents_to_distribute(self, contentkind = "FTR"):
        """
            returns torrents paths
            by default return only torrents for features
        """
        _torrents_path = []
        app = current_app._get_current_object()
        for _dcp in self.dcps:
            if _dcp.contentkind == contentkind:
                _path = os.path.join(app.config['TORRENT_FOLDER'],
                        _dcp.name+".torrent")
                _torrents_path.append(_path)
        return _torrents_path

    def is_distributable(self):
        """"
        Determines if a Dcp is distributable. Two conditions:
        1. Distrributor is official
        2. Dcp have FTR content lind
        """
        res = True
        if self.distributor.isdcpbay == False:
            return False
        for  _dcp in self.dcps:
            if _dcp.contentkind == Dcp.FEATURE:
                return True
        return False

    @hybrid_property
    def officiality(self):
        if self.distributor.isdcpbay == False:
            return self.UNOFFICIAL
        else :
            if self.nb_ftrs() > 0 :
                return self.OFFICIAL_WITH_FTR
            else:
                return self.OFFICIAL_WITHOUT_FTR

    @classmethod
    def all_valid(cls):
        return cls.query.filter_by(valid = True).all()

    def remove_authorisations(self, exhibitor):
        for dcp in self.get_all_dcps():
            try:
                distribution = Distribution.query.filter_by(
                    exhibitor = exhibitor,
                    dcp =dcp).one()
            except NoResultFound:
                raise TucoException(u'No Result found for {} / {}'.\
                        format(exhibitor, dcp))
            # TODO: verify if movie not alreeady distributer --> 
            # deny this action if movie already started or finished by 
            # exhibitor
            if dcp.contentkind == Dcp.FEATURE:
                if distribution.isauthorised:
                    distribution.isauthorised = False
                    distribution.autorisation_date = None
                    distribution.status = DistributionStatus.DL_NOT_AUTHORIZED
                    db.session.commit()
                else:
                    msg = u'authorisation for {} not yet allowed'.\
                        format(distribution)
                    current_app.logger.info(msg)
            elif dcp.contentkind == Dcp.TRAILER:
                distribution.autorisation_date = None
                distribution.status = DistributionStatus.DL_AUTHORIZED
                db.session.commit()
            else:
                raise TucoException(u'Not implemented contentkind({})'.\
                        format(dcp))


from sqlalchemy.orm import class_mapper, ColumnProperty
class Extra(db.Model):
    def columns(self):
        """Return the actual columns of a SQLAlchemy-mapped object"""
        return [prop.key for prop in class_mapper(self.__class__).iterate_properties
            if isinstance(prop, ColumnProperty)]

    id = db.Column(db.Integer, primary_key=True)
    website_url = db.Column(db.String(512))
    website_dsc = db.Column(db.String(512))
    movie_id  = db.Column(db.Integer, db.ForeignKey('movie.movieid'))
    short_id  = db.Column(db.Integer, db.ForeignKey('short.shortid'))
    movie = db.relationship("Movie", back_populates="extra")
    short = db.relationship("Short", back_populates="extra")



class Tag(db.Model):
    __bind_key__ ='appdb'
    __tablename__ = 'tag'
    id = db.Column(db.Integer, primary_key=True)
    valid    = db.Column(db.Boolean, default=True)
    name = db.Column(db.String(64), unique=True)
    longname = db.Column(db.String(512))
    description = db.Column(db.Text())

    def __repr__(self):
        return '<Tag %r>' % self.name
    def __str__(self):
        return u'{}'.format(self.name)

    @property
    def all_content(self):
        return self.movies.all() + self.shorts.all()

class Dcp(db.Model):
    __bind_key__ ='appdb'
    # types
    TRAILER = "TLR"
    FEATURE = "FTR"
    SHORT   = "SHR"

    dcpid = db.Column(db.Integer, primary_key=True)
    valid    = db.Column(db.Boolean, default=True)
    name = db.Column(db.String(150), unique=True)
    contentkind = db.Column(db.String(5))
    size = db.Column(db.BigInteger)
    torrent_hash = db.Column(db.String(40))
    torrent_creation = db.Column(db.DateTime)
    movieid =     db.Column(db.Integer, db.ForeignKey('movie.movieid'))
    shortid =     db.Column(db.Integer, db.ForeignKey('short.shortid'))
    exhibitors = db.relationship('Distribution',
                                foreign_keys = [Distribution.dcpid],
                                backref=db.backref('dcp', lazy='joined'),
                                lazy = 'dynamic',
                                cascade='all, delete-orphan')
    messages = db.relationship('TrackerMessage',
                                foreign_keys = [TrackerMessage.dcpid],
                                backref=db.backref('dcp', lazy='joined'),
                                lazy = 'dynamic',
                                cascade='all, delete-orphan')


    def __repr__(self):
        return u'<DCP ({},{})>'.format(self.dcpid, self.name)

    def __str__(self):
        return u'<DCP ({},{})>'.format(self.dcpid, self.name)

    def is_in_distribution(self, exhibitor):
        rv = Distribution.query.filter_by(exhibitor = exhibitor,
                                dcp = self).first()
        if rv is not None:
            return True
        return False

    @classmethod
    def get_torrent_path(cls, dcps):
        _torrents_path = []
        app = current_app._get_current_object()
        for _dcp in dcps:
            _path = os.path.join(app.config['TORRENT_FOLDER'],
                        _dcp.name+".torrent")
            _torrents_path.append(_path)
        return _torrents_path

class TdcpbServer(db.Model):
    __bind_key__ ='appdb'
    serverid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    iptinc = db.Column(db.String(15), unique=True)

    def __repr__(self):
        return u'<Server {} {}>'.format(self.name, self.iptinc)

class Message(db.Model):
    __bind_key__ ='appdb'
    id = db.Column(db.Integer, primary_key=True)
    from_id = db.Column(db.Integer, db.ForeignKey('user.userid'))
    to_id = db.Column(db.Integer, db.ForeignKey('user.userid'))
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    body = db.Column(db.Text())
    subject = db.Column(db.Text())
    # cf: http://docs.sqlalchemy.org/en/latest/orm/join_conditions.html#handling-multiple-join-paths
    msg_from     = db.relationship('User',foreign_keys = from_id)
    msg_to     = db.relationship('User',foreign_keys = to_id)



    @classmethod
    def prepare_sending(cls, msg_from, msg_to, subject, body = None, template = None,
            **kwargs):
        _body = None
        app = current_app._get_current_object()

        if template:
            _body = render_template(template + '.txt', **kwargs)

        if body:
            _body = body

        if 'payload_msg' in kwargs:
            _body = kwargs['payload_msg']

        if 'date_progammation' in kwargs:
            _str = u"\nDate de programmation du film dans le cinéma : {} \n".\
                    format(kwargs['date_progammation'])
            _body = _body + _str

        if not msg_from.user:
            from_id = None
        else:
            from_id = msg_from.user.userid

        if not msg_to.user:
            to_id = None
        else:
            to_id = msg_to.user.userid,
        message = cls(
                from_id = from_id,
                to_id = to_id,
                subject = subject,
                body =  _body)
        db.session.add(message)
        if 'movie_request' in kwargs:
            kwargs['movie_request'].messages.append(message)

        db.session.commit()

        try:
            emails_to = build_to(msg_to)
        except TucoException as err:
            emails_to = ['contacter@indecp.org']

        try:
            kwargs['cc'] = build_cc(msg_from)
        except TucoException as err:
             kwargs['cc'] = [app.config['TUCO_ADMIN']]



        if template:
            send_email(emails_to,
                subject,
                template,
                **kwargs
                )
        else:
            send_email_wo_template(
                sender = build_to(msg_from)[0],
                to = build_to(msg_to),
                subject = subject,
                body = _body,
                cc = [app.config['TUCO_ADMIN']]
                )
        return message



movie_request_messages = db.Table('movie_request_message',
                db.Column('movie_request_id', db.Integer, db.ForeignKey('movie_request.id')),
                db.Column('message_id', db.Integer, db.ForeignKey('message.id')),
                )


class MovieRequestStatus(Enum):
    NOT_SET   = 0
    PENDING   = 1
    ALLOWED   = 2
    DENIED    = 3
    CANCELED  = 4

class MovieRequestDenyCause(Enum):
    NO_REASON       = 0
    POSTAL_DELIVERY = 1
    OTHER_RECIPIENT = 2
    NEW_VERSION     = 3
    NOT_PROGRAMED   = 4
    OTHER           = 5




class MovieRequest(db.Model):
    __bind_key__ ='appdb'
    id = db.Column(db.Integer, primary_key=True)
    statusId       =      db.Column(db.Integer, default=MovieRequestStatus.PENDING.value)
    first_demand_date     = db.Column(db.DateTime())
    demand_date     = db.Column(db.DateTime())
    allow_date     = db.Column(db.DateTime())
    cancel_date     = db.Column(db.DateTime())
    deny_date     = db.Column(db.DateTime())
    deny_causeId   = db.Column(db.Integer())
    deny_other_cause   = db.Column(db.Text())
    programed_date = db.Column(db.Date)
    #TODO : verify use of is_autorised
    is_autorised    = db.Column(db.Boolean, default=False)
    movieid =     db.Column(db.Integer, db.ForeignKey('movie.movieid'))
    exhibitorid = db.Column(db.Integer, db.ForeignKey('exhibitor.exhibitorid'))
    distributorid = db.Column(db.Integer, db.ForeignKey('distributor.distributorid'))
    messages = db.relationship('Message',
                                secondary = movie_request_messages,
                                backref = db.backref('movie_requests', lazy='dynamic'),
                                order_by="desc(Message.id)")

    def __repr__(self):
        return '<Movie Request %r for %r >' % (self.movie.title, self.exhibitor.name)

    def __repr__(self):
        return u'Movie Request {} {}  for {}'.format(self.id, self.movie.title, self.exhibitor.name)

    @classmethod
    def new_request(cls, exhibitor, movie, programed_date = None):
        request = cls.query.filter_by(exhibitor = exhibitor,
                movie = movie).first()
        if request is not None:
            msg = u'Demand for {} to {} already exist. Update demand date only'.\
                    format(movie.title, exhibitor.name)
            current_app.logger.warning(msg)
            request.demand_date  = datetime.now()
            # TODO: update programed_date ?
            request.programed_date = programed_date
        else:
            request = cls(
                    first_demand_date = datetime.now(),
                    exhibitor = exhibitor,
                    movie = movie,
                    distributor = movie.distributor,
                    programed_date = programed_date)
            request.demand_date = request.first_demand_date
            db.session.add(request)
        db.session.commit()
        return request

    #TODO : verify use of distribution_is_autorised
    @classmethod
    def distribution_is_autorised(cls, exhibitor, movie):
        _res= cls.query.filter_by(exhibitor = exhibitor,
                                movie = movie).first()
        if _res is not None:
            _res.is_autorised = True;
            _res.statusId = MovieRequestStatus.ALLOWED.value
            _res.allow_date = datetime.now()

            db.session.commit()
            msg = u'Distribution of {} to {} autorised'.\
                    format(movie.title, exhibitor.name)
            current_app.logger.warning(msg)

    @property
    def status(self):
        try:
            return MovieRequestStatus(self.statusId).name
        except:
            return MovieRequestStatus.NOT_SET.name
    @property
    def deny_cause(self):
        try:
            return MovieRequestDenyCause(self.deny_causeId).name
        except:
            return MovieRequestDenyCause.NO_REASON.name


    def update(self):
        if self.statusId == MovieRequestStatus.PENDING.value:
            self.demand_date = datetime.now()
            db.session.commit()
        if self.statusId == MovieRequestStatus.CANCELED.value:
            self.statusId = MovieRequestStatus.PENDING.value
            self.cancel_date = None
            self.demand_date = datetime.now()
            db.session.commit()


    def allow(self):
        if self.statusId == MovieRequestStatus.PENDING.value:
            self.allow_date = datetime.now()
            self.statusId = MovieRequestStatus.ALLOWED.value
            db.session.commit()
        elif self.statusId == MovieRequestStatus.DENIED.value:
            self.allow_date = datetime.now()
            self.statusId = MovieRequestStatus.ALLOWED.value
            db.session.commit()

    def deny(self, p_deny_causeId=MovieRequestDenyCause.NO_REASON.value, deny_other_cause=None):

        self.statusId = MovieRequestStatus.DENIED.value
        try:
            deny_causeId = MovieRequestDenyCause(int(p_deny_causeId)).value
        except ValueError:
            deny_causeId= MovieRequestDenyCause.NO_REASON.value
        self.deny_causeId = deny_causeId
        self.deny_date = datetime.now()
        if deny_other_cause:
            self.deny_other_cause = deny_other_cause



    def cancel(self, reason=None):
        if self.statusId in (MovieRequestStatus.PENDING.value, MovieRequestStatus.ALLOWED.value):
            self.statusId =  MovieRequestStatus.CANCELED.value
            self.cancel_date = datetime.now()
            db.session.commit()


class UserProperty(db.Model):
    __bind_key__ ='appdb'
    STARTAUTO =( u"startauto")
    PROPERTIES = (STARTAUTO)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    value = db.Column(db.String(50))
    user_id = db.Column(db.Integer, db.ForeignKey('user.userid'))

    def __init__(self, name, value, user):
        res = self.query.filter_by(name = name, user =user).first()
        if res:
            raise ModelException(u'property {} already exists for {}'.format(name, user))
        else:
            self.name = name
            self.value = value
            self.user = user

    def __setattr__(self, name, value):
        super(UserProperty, self).__setattr__(name, value)

    @classmethod
    def set_bool(cls, value):
        if not isinstance(value, bool):
            raise ValueError(u'A boolean value is expected')
        if value:
            return "True"
        else:
            return "False"

class ExhibitorSchema(ma.ModelSchema):
    class Meta:
        model = Exhibitor
        exclude = ('dcps', 'messages', 'demands')

class MessageSchema(ma.ModelSchema):
    class Meta:
        model = Message

from marshmallow import fields
class MovieRequestSchema(ma.ModelSchema):
    class Meta:
        model = MovieRequest
    exhibitor = fields.Nested(ExhibitorSchema())
    messages =  fields.List(fields.Nested(MessageSchema, exclude=('body',)))

