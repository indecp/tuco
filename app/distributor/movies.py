# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from datetime import date, timedelta

from flask import render_template, redirect, url_for, flash, request, jsonify, abort
from flask import safe_join
from flask_login import login_required, current_user
from flask_restful import fields, marshal

from . import distributor_movies
from app import db, current_app
from app.exceptions import TucoException
from app.decorators import distributor_required
from app.models import Movie, Distributor


blueprint = distributor_movies

TEMPLATES={
    'home': 'all.html',
    'details': 'details.html',
    }

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )

@blueprint.route('/home', methods=['GET','POST'])
@blueprint.route('/')
@login_required
@distributor_required
def home():
    app = current_app._get_current_object()
    distributor = current_user.distributor
    movies = distributor.movies.filter_by(valid = True).all()
    return render ('home',
            movies = movies)


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@distributor_required
def details(id):
    distributor = current_user.distributor
    movie = Movie.query.filter_by(movieid = id, distributor=distributor, valid=True).first()
    if not movie:
        abort(404)
    return render ('details',
            distributor = distributor,
            movie = movie)


