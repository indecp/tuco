from flask import Blueprint

distributor = Blueprint('distributor', __name__)

distributor_movies = Blueprint('distributor_movies', __name__, template_folder="distributor/movies/")
distributor_movie_requests = Blueprint('distributor_movie_requests', __name__, template_folder="distributor/movie_requests/")

from . import views
from . import forms
from . import distribution
from . import movies
from . import movie_requests

