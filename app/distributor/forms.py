# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

from flask_wtf import Form
from wtforms import SubmitField, SelectField, SelectMultipleField
from wtforms import widgets
from ..models import Dcp

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.Input()
    option_widget = widgets.CheckboxInput()

class ConfirmationForm(Form):
    submit = SubmitField(u'Valider la distribution')
    cancel = SubmitField(u'Annuler')


choices = [
    (Dcp.TRAILER, u'Bande Annonce (TLR)'),
    (Dcp.FEATURE, u'Long métrage (FTR)'),
]

class SelectDistributionTypeForm(Form):
    dist_type = MultiCheckboxField('A distribuer ', choices=choices)
    submit = SubmitField('Continuer')
    cancel = SubmitField('Annuler')
