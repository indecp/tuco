# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import os.path
import sys

from flask import render_template, session, redirect, url_for, request
from flask import current_app, flash, jsonify
from flask_login import login_required, current_user
from flask_wtf import Form
from sqlalchemy import func

from . import distributor
from .. import db
from ..models import User, Movie, Exhibitor, DistributionStatus, Dcp, Short
from ..models import Distribution, MovieRequest, Distributor
from ..email import send_email, build_to
from ..decorators import distributor_required
from .forms import ConfirmationForm, SelectDistributionTypeForm
from .distribution import do_distribution

from app.datatables import ColumnDT, DataTables
from app.exceptions import TucoException
from app.common   import redirect_url

from string import upper

@distributor.route('/distributor/')
@distributor.route('/distributor/home')
@login_required
@distributor_required
def home():
    _d = current_user.distributor
    if _d.is_agencecm:
        shorts = Short.query.filter_by(distributor = _d).all()
        return render_template('distributor/agencecm_view.html',
                           title='Tuco',
                           distributor = _d,
                           shorts = shorts
                    )
    else:
        movies =_d.movies.filter_by(valid=True).order_by(Movie.releasedate.desc()).limit(4)
        return render_template('distributor/distributor_view.html',
                           title='Tuco',
                           distributor = _d,
                           movies = movies
                           )

@distributor.route('/distributor/distribute_movie/<int:movieid>', methods=['GET','POST'])
@login_required
@distributor_required
def distribute_movie(movieid):
    app = current_app._get_current_object()
    movie = Movie.query.get_or_404(movieid)
    exhibitors = Exhibitor.all_valid_exhibitors()

    form = Form()
    if form.validate_on_submit():
        if request.form.get('cancel'):
            flash(u'Distribution annulée')
            return redirect(url_for('distributor_movies.details', id = movie.movieid ))

        elif request.form.get('submit'):
            session['exhibitors'] = request.form.getlist('exhibitors')
            session['selected_dcps'] =  request.form.getlist('dcps')
            return redirect(url_for('distributor.distribute_movie_confirmation',
                                movieid = movieid))

        else:
            _msg=u'unkown POST directive'
            app.logger.error(_msg)
            flash(_msg)


    return render_template('distributor/distribute_movie.html',
        movie = movie,
        form = form)



@distributor.route('/distributor/distribute_movie_confimation/<int:movieid>', methods=['GET','POST'])
@distributor.route('/distributor/distribute_movie_confimation/<int:movieid>/<int:exhibitorid>', methods=['GET','POST'])
@login_required
@distributor_required
def distribute_movie_confirmation(movieid, exhibitorid=None):
    movie = Movie.query.get_or_404(movieid)
    app = current_app._get_current_object()
    if exhibitorid is None:
        exhibitors   = [Exhibitor.query.get(_id) for _id in session['exhibitors']]
        selected_dcps = [Dcp.query.get(_id) for _id in session['selected_dcps']]
    else:
        exhibitors = [Exhibitor.query.get_or_404(exhibitorid)]
        selected_dcps = movie.get_all_dcps()
    if not exhibitors:
        _msg = u'Pas d\'exploitant sélectionné: Distribution impossible'
        flash(_msg)
        current_app.logger.warning(_msg)
        return redirect(url_for('.distribute_movie', movieid = movieid))

    form = ConfirmationForm()
    if form.validate_on_submit():
        if form.cancel.data:
            flash(u'Distribution annulée')
            return redirect(url_for('distributor_movies.details', id = movieid))
        elif form.submit.data:
            exhibitors_ok = []
            for exhibitor in exhibitors:
                try:
                     _added_dcps = do_distribution(movie, selected_dcps, exhibitor)
                except TucoException as err:
                    flash(err.message)
                    continue
                if _added_dcps is not None:
                    exhibitors_ok.append(exhibitor)
                else:
                    _msg = u"L'exploitant {} est déjà autorisé à recevoir :{}".\
                            format(exhibitor, ' '.join([ _dcp.name for _dcp in selected_dcps] ))
                    flash(_msg)
            # send recap email to ditributor
            if exhibitors_ok:
                try:
                    send_email(
                        build_to(current_user.distributor),
                        u'Récapitulatif distribution de {}'.format(movie.title),
                        'distributor/email/recap_distribution',
                        cc = [app.config['TUCO_ADMIN']],
                        movie = movie,
                        selected_dcps = selected_dcps,
                        exhibitors = exhibitors_ok)
                except:
                    _msg = u'Something goes wrong in expedition'
                    flash(_msg)
                    _msg= u'Unexpected error:', sys.exc_info()[0]
                    current_app.logger.error(_msg)
            return redirect(url_for('distributor_movies.details', id = movieid))
        else:
            current_app.logger.error(u'Something goes wrong')
            return redirect(url_for('distributor_movies.details', id = movieid))
    return render_template('distributor/distribute_movie_confirmation.html',
                            movie = movie,
                            dcps = selected_dcps,
                            exhibitors = exhibitors,
                            form = form)

@distributor.route('/distributor/exhibitors')
@login_required
@distributor_required
def exhibitors():
    exhibitors = Exhibitor.query.order_by(Exhibitor.name)
    return render_template('distributor/exhibitors.html',
                            exhibitors = exhibitors)

@distributor.route('/distributor/exhibitor/<int:exhibitorid>', methods=['GET','POST'])
@login_required
@distributor_required
def show_exhibitor(exhibitorid):
    _d = current_user.distributor
    exhibitor = Exhibitor.query.get_or_404(exhibitorid)

    return render_template('distributor/exhibitor.html',
                           exhibitor = exhibitor,
                           distributor= _d)

@distributor.route('/distributor/list_demands', methods=('GET', 'POST'))
@login_required
@distributor_required
def list_demands():
    distributor = current_user.distributor
    demands = MovieRequest.query.filter_by(distributor = distributor).all()
    return render_template('distributor/list_demands.html',
                            demands = demands)

@distributor.route('/distributor/distributiondata')
@login_required
@distributor_required
def distributiondata():
    """Return server side data."""
    # defining columns
    columns = []
    columns.append(ColumnDT('dcp.movie.title','movie'))
    columns.append(ColumnDT('dcp.name','dcp' ))
    columns.append(ColumnDT('dcp.movie.distributor.name','distributor'))
    columns.append(ColumnDT('exhibitor.name', 'exhibitor'))
    columns.append(ColumnDT('autorisation_date', 'autorisation_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('started_transfer_date','started_transfer_date',  search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('finished_transfer_date', 'finished_transfer_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('dcp.contentkind', 'contentkind', searchable  =False))
    columns.append(ColumnDT('dcp.movie.movieid', 'movieid',  searchable  =False))
    columns.append(ColumnDT('dcp.movie.distributor.distributorid','distributorid', searchable  =False))
    columns.append(ColumnDT('exhibitor.exhibitorid', 'exhibitorid', searchable  =False))
    columns.append(ColumnDT('dcp.dcpid', 'dcpid', searchable  =False))
    columns.append(ColumnDT('dcp.valid', 'valid', searchable  =False))
    columns.append(ColumnDT('exhibitor.city', 'city', searchable  =False, filter= upper) )

    try:
        _e = int( request.values['exhibitor'])
    except ValueError:
        _e = None

    try:
        _d = int( request.values['distributor'])
    except ValueError:
        _d = None

    try:
        _m = int( request.values['movie'])
    except ValueError:
        _m = None

    query = db.session.query(Distribution).join(Dcp).join(Exhibitor).join(Movie).join(Distributor)
    if _e is not None:
        query = query.filter(Exhibitor.exhibitorid == _e)
    if _d is not None:
        query = query.filter(Distributor.distributorid == _d)
    if _m is not None:
        query = query.filter(Movie.movieid == _m)
    from sqlalchemy import or_, and_
    query = query.filter(or_(
        and_(Dcp.contentkind == Dcp.FEATURE, Distribution.isauthorised ==True),
        and_( Dcp.contentkind == Dcp.TRAILER,
            or_( Distribution.autorisation_date != None,  Distribution.started_transfer_date !=None)
            )
        ))

    # instantiating a DataTable for the query and table needed
    rowTable = DataTables(request.args, Distribution, query, columns)
    # returns what is needed by DataTable
    return jsonify(rowTable.output_result())


from flask_restful import fields, marshal
from flask.json import dumps

class StatusField(fields.Raw):
    DISTRIBUTION_STATUS = {
            DistributionStatus.DL_NOT_AUTHORIZED : u'Distribution not allowed',
            DistributionStatus.DL_AUTHORIZED : u'Distribution allowed',
            DistributionStatus.DL_STARTED : u'Distribution started',
            DistributionStatus.DL_FINISHED : u'Distribution finished',
            DistributionStatus.DL_ERROR : u'Distribution error',
            }


    def format(self, value):
        return self.DISTRIBUTION_STATUS[value]

class DateTimeField(fields.DateTime):
    def format(self, value):
        return super(DateTimeField,self).format(value)

distribution_fields = {
    'distributionid':fields.Integer,
    'dcpid': fields.Integer,
    'dcp': fields.String(attribute='dcp.name'),
    'contentkind': fields.String(attribute='dcp.contentkind'),
    'exhibitor_cncid': fields.Integer,
    'exhibitor': fields.String,
    'isauthorised':fields.Boolean,
    'autorisation_date': DateTimeField(dt_format='iso8601'),
    'started_transfer_date': DateTimeField(dt_format='iso8601'),
    'finished_transfer_date': fields.DateTime(dt_format='iso8601'),
    'status': StatusField(attribute = 'status'),
    'statusId': fields.Integer(attribute='status'),
    'eta': fields.DateTime(dt_format='iso8601' ),
    'progress': fields.String
}

exhibitor_fields = {
    'exhibitorid': fields.Integer,
    'cncid': fields.Integer,
    'name': fields.String,
    'city': fields.String,
    'authorisations': fields.List(fields.Nested(distribution_fields))
}


@login_required
@distributor_required
@distributor.route('/distributor/exhibitorsdata')
def exhibitorsdata():
    #TODO: find a way to get movieid
    try:
        movieid = int( request.values['movieid'])
    except ValueError:
        movieid = None


    movie = Movie.query.get_or_404(movieid)

    exhibitors_data = []
    exhibitors = Exhibitor.all_valid_exhibitors()
    dcpsid = [d.dcpid for d in movie.get_all_dcps()]
    for _e in exhibitors:
        _e.authorisations = _e.dcps.\
        filter(Distribution.dcpid.in_(dcpsid))\
        .all()

        marshaled = marshal(_e, exhibitor_fields)
        exhibitors_data.append(marshaled)

    return jsonify(data = exhibitors_data)
