# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from app import db
from app.models import MovieRequest,Dcp,ModelException
from app.email import send_email, build_to
from flask import current_app
from app.exceptions import TucoException
from app.exhibitor.actions import do_start_torrent_downloads



def notify_exhibitor(exhibitor, movie, selected_dcps):
    if exhibitor.startauto:
        dcpids = [ d.dcpid for d in selected_dcps]
        try :
            do_start_torrent_downloads(dcpids, exhibitor)
        except TucoException as err:
            raise TucoException(err)
        else:
            send_email(build_to(exhibitor.user),
            u'Téléchargement - {} de {}'.format(
                movie.title,
                movie.distributor),
            'distributor/email/distribution_started',
            exhibitor = exhibitor, movie = movie,
            selected_dcps = selected_dcps
            )
    else:
        send_email(build_to(exhibitor.user),
            u'Autorisation de téléchargement - {} de {}'.format(
                movie.title,
                movie.distributor),
            'distributor/email/autorisation_distribution',
            exhibitor = exhibitor, movie = movie,
            selected_dcps = selected_dcps
            )

def do_distribution(movie, selected_dcps, exhibitor):
    """
    distribute a movie ( 1+ DCP) to an exhibitor
    manage errors and update flash and logging
    returns nothing
    """
    # Is the exhibitor linked to a user? (robustness)
    if not exhibitor.user:
        _msg = u'Exploitant {} non actif. Distribution impossible'.\
            format(exhibitor.name)
        raise TucoException(_msg)
    if selected_dcps == []:
        _msg = u'Pas de DCP selectionné'
        raise TucoException(_msg)
    # Is movie (FTR only) already on distribution (robustness)
    MovieRequest.distribution_is_autorised(exhibitor,movie)
    _added_dcps =  movie.add_dcps_for_distribution(exhibitor, selected_dcps)
    if _added_dcps:
        #QUESTION: if an exception is raised a db.session.rollback is needed ?
        # ie to undo the db.session.add in add_dcps_for_distribution.
        # looks like no need of commit but more test or doc read needed
        try:
            current_app.logger.warning('Selected DCPS : {}'.format(selected_dcps))
            notify_exhibitor(exhibitor, movie, selected_dcps)
        except TucoException as err:
            current_app.logger.error(err)
            raise TucoException(err)
        except:
            _msg = u'Something goes wrong in expedition'
            current_app.logger.exception(_msg)
            raise TucoException(_msg)
        else :
            db.session.commit()
            _msg = u'Distribution de {} ajouté pour  {}'.format(movie.title, exhibitor.name)
            current_app.logger.info(_msg)
            return _added_dcps
    else:
        _msg = u'Pas de DCPs a distribué pour {}. Le ciméma {} les a déjà ?'.\
            format(movie.title, exhibitor.name)
        current_app.logger.warning(_msg)
        return None
    return None
