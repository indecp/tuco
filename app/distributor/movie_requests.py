# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
import sys

from datetime import date, datetime, timedelta

from flask import render_template, redirect, url_for, flash, request, jsonify, abort
from flask import safe_join
from flask_login import login_required, current_user
from flask_restful import fields, marshal
from sqlalchemy import or_, and_

from . import distributor_movie_requests
from app import db, current_app
from app.exceptions import TucoException
from app.torrent.transmission import TorrentClient
from app.decorators import distributor_required
from app.common import path2url, redirect_url, post_json_data_to_dict
from app.models import MovieRequest, MovieRequestStatus, Movie, Exhibitor, Message
from sqlalchemy.orm import aliased
from ..email import send_email, build_to
from .distribution import do_distribution

from app.common import redirect_url
from app.filters import deny_cause_fr_filter


blueprint = distributor_movie_requests

TEMPLATES={
    'home': 'home.html',
    'all': 'all.html',
    'new': 'new.html',
    'details': 'details.html',
    }

message_fields = {
    'id'                : fields.Integer,
    'from_id'                : fields.Integer,
    'msg_from'                : fields.String,
    'to_id'                : fields.Integer,
    'msg_to'                : fields.String,
    'timestamp'       : fields.DateTime(dt_format='iso8601'),
    'body'            : fields.String,
    'subject'            : fields.String
    }

movie_request_fields = {
    'id'                : fields.Integer,
    'statusId'          : fields.Integer,
    'status'            : fields.String,
    'first_demand_date' : fields.DateTime(dt_format='iso8601'),
    'demand_date'       : fields.DateTime(dt_format='iso8601'),
    'allow_date'        : fields.DateTime(dt_format='iso8601'),
    'cancel_date'       : fields.DateTime(dt_format='iso8601'),
    'deny_date'         : fields.DateTime(dt_format='iso8601'),
    'deny_causeId'      : fields.Integer,
    'deny_cause'        : fields.String,
    'deny_other_cause'  : fields.String,
    'programed_date'    : fields.DateTime(dt_format='iso8601'),
    'is_autorised'      : fields.Boolean,
    'exhibitorid'       : fields.Integer,
    'exhibitor'         : fields.String(attribute="exhibitor.standard_name"),
    'movieid'           : fields.Integer,
    'movie'             : fields.String(attribute='movie.title'),
    'distributorid'     : fields.Integer,
    'distributor'       : fields.String(attribute='distributor.name'),
    'isdcpbay'          : fields.Boolean(attribute='distributor.isdcpbay'),
    'messages'         : fields.List(fields.Nested(message_fields)),
}

dcp_fields = {
    'dcpid': fields.Integer,
    'name': fields.String,
    'valid': fields.Boolean,
    'size': fields.String,
    'contentkind': fields.String,
    }

movie_fields = {
    'movieid': fields.Integer,
    'title': fields.String,
    'original_title': fields.String,
    'valid': fields.Boolean,
    'releasedate': fields.String,
    'dcps':  fields.List( fields.Nested(dcp_fields), attribute=lambda x: x.get_all_dcps()),
}

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )


@blueprint.route('/home', methods=['GET','POST'])
@blueprint.route('/pending', methods=['GET','POST'])
@blueprint.route('/')
@login_required
@distributor_required
def home():
    app = current_app._get_current_object()
    distributor = current_user.distributor

    return render ('home',
            distributor = distributor)

@blueprint.route('/all')
@login_required
@distributor_required
def all():
    app = current_app._get_current_object()
    distributor = current_user.distributor

    return render ('all',
            distributor = distributor)

@blueprint.route('/new')
@login_required
@distributor_required
def new():
    app = current_app._get_current_object()
    distributor = current_user.distributor

    return render ('new',
            distributor = distributor)



@blueprint.route('/movie_request_data/<string:query_type>')
@login_required
@distributor_required
def movie_request_data(query_type):
    app = current_app._get_current_object()
    distributor = current_user.distributor

    fromNow = datetime.now() - timedelta(weeks=5)
    if query_type == 'new':
        movie_requests = MovieRequest.query.\
        filter_by(distributor = distributor).\
        filter ( or_(
           (MovieRequest.statusId == MovieRequestStatus.PENDING.value),
           and_(MovieRequest.statusId == MovieRequestStatus.ALLOWED.value,
               MovieRequest.allow_date > fromNow),
           and_(MovieRequest.statusId == MovieRequestStatus.DENIED.value,
               MovieRequest.deny_date > fromNow)
           ))\
        .all()
    elif query_type == 'pending':
        movie_requests = MovieRequest.query.\
        filter_by(distributor = distributor).\
        filter (
           (MovieRequest.statusId == MovieRequestStatus.PENDING.value),
          )\
        .all()
    else:
        movie_requests = MovieRequest.query.filter_by(distributor = distributor).all()

    marshaled = [marshal(_d, movie_request_fields) for _d in movie_requests]
    return jsonify(data=marshaled)



@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@distributor_required
def details(id):
    app = current_app._get_current_object()
    distributor = current_user.distributor

    movie_request = MovieRequest.query.get_or_404(id)
    if movie_request.movie.distributor != distributor:
        abort(403)
    marshaled = marshal(movie_request, movie_request_fields)
    return render ('details',
            distributor = distributor,
            data = marshaled,
            requestid = id
            )

@blueprint.route('/detailsdata/<int:id>', methods=['GET'])
@login_required
@distributor_required
def detailsdata(id):
    app = current_app._get_current_object()
    distributor = current_user.distributor

    movie_request = MovieRequest.query.get_or_404(id)
    if movie_request.movie.distributor != distributor:
        abort(403)
    marshaled = marshal(movie_request, movie_request_fields)
    return jsonify(data=marshaled)


@blueprint.route('/do_deny', methods=['POST'])
@login_required
@distributor_required
def do_deny():
    app = current_app._get_current_object()
    distributor = current_user.distributor

    content = post_json_data_to_dict(request.json)
    movie_request = MovieRequest.query.get_or_404(content['movierequestid'])
    if movie_request.movie.distributor != distributor:
        abort(403)
    movie_request.deny(content["deny_causeId"] , content['message'])
    movie_request.movie.remove_authorisations(movie_request.exhibitor)
    payload_msg = \
        u"Raison du refus: {}\n".format(deny_cause_fr_filter(movie_request)) \
        + u"\n" \
        + u"Commentaire:\n" \
        + u"{}".format(content['message'])
    try:
        Message.prepare_sending(
                msg_from = distributor,
                msg_to = movie_request.exhibitor,
                subject = \
        u'Réponse négative - Demande de téléchargement de {} du cinéma {}'.\
                    format(movie_request.movie.title, movie_request.exhibitor),
                template = 'distributor/email/deny',
                payload_msg = payload_msg,
                movie_request = movie_request,
                )
    except TucoException as err:
        message = u"Imposible d'envoyer un message à {}.\
Vous pouvez contacter Indé-CP pour résoudre ce problème".\
            format(movie_request.exhibitor, err)
        app.logger.warning(message)
        raise InvalidUsage(message, status_code=410)
    return jsonify( marshal(movie_request,movie_request_fields))

@blueprint.route('/do_allow', methods=['POST'])
@login_required
@distributor_required
def do_allow():
    app = current_app._get_current_object()
    distributor = current_user.distributor
    content = post_json_data_to_dict(request.json)
    movie_request = MovieRequest.query.get_or_404(content['movierequestid'])

    if movie_request.movie.distributor != distributor:
        abort(403)

    movie_request.allow()
    movie= movie_request.movie
    exhibitor = movie_request.exhibitor
    selected_dcps = movie.get_all_dcps()

    try:
        _added_dcps = do_distribution(movie, selected_dcps, exhibitor)
    except TucoException as err:
        current_app.logger.error(err.message)
        flash(err.message)
        return jsonify({'status': 'ko', 'message':err.message})

    if _added_dcps is not None:
        try:
            send_email(
                build_to(distributor),
                u'Récapitulatif distribution de {}'.format(movie.title),
                'distributor/email/recap_distribution',
                cc = [app.config['TUCO_ADMIN']],
                movie = movie,
                selected_dcps = selected_dcps,
                exhibitors = [exhibitor])
        except:
            _msg = u'Something goes wrong in expedition'
            flash(_msg)
            _msg= u'Unexpected error:', sys.exc_info()[0]
            current_app.logger.error(_msg)
    else:
        _msg = u"L'exploitant {} est déjà autorisé à recevoir :{}".\
                        format(exhibitor, ' '.join([ _dcp.name for _dcp in selected_dcps] ))
        flash(_msg)

    return jsonify( marshal(movie_request,movie_request_fields))


@blueprint.route('/do_get_movie_data', methods=['POST'])
@login_required
@distributor_required
def do_get_movie_data():
    app = current_app._get_current_object()
    distributor = current_user.distributor
    content = request.json
    movie_request = MovieRequest.query.get_or_404(content['id'])
    if movie_request.movie.distributor != distributor:
        abort(403)

    return jsonify( marshal(movie_request.movie, movie_fields))


@blueprint.route('/do_contact', methods=['POST'])
@login_required
@distributor_required
def do_contact():
    distributor = current_user.distributor
    app = current_app._get_current_object()
    content = post_json_data_to_dict(request.json)
    movie_request = MovieRequest.query.get_or_404(content['movierequestid'])
    try:
        Message.prepare_sending(
                msg_from = distributor,
                msg_to = movie_request.exhibitor,
                subject = \
        u"Information - Demande de téléchargement de {} du cinéma {}".\
                    format(movie_request.movie.title, movie_request.exhibitor),
                template = 'distributor/email/contact',
                payload_msg = content[u'message'],
                movie_request = movie_request,
                )
    except TucoException as err:
        message = u"Imposible d'envoyer un message à {}.\
Vous pouvez contacter Indé-CP pour résoudre ce problème".\
            format(movie_request.exhibitor, err)
        app.logger.warning(message)
        raise InvalidUsage(message, status_code=410)
    return jsonify({'status': 'ok'})

