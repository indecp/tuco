# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
from datetime import datetime
from flask_babel import format_datetime, format_date
from models import Dcp, Movie, Distribution
IS08601_DATE_FORMAT='%Y-%m-%d'
IS08601_DATETIME_FORMAT='%Y-%m-%dT%H:%M:%S'

def mydatetime_filter(s):
    if s is None:
        return ""
    else:
        return format_datetime(s)

def datefr_filter(s):
    if s is None:
        return ""
    else:
        return format_date(s)

def siso8601todatetime_filter(s):

    _date = datetime.strptime(s, IS08601_DATETIME_FORMAT)
    return _date

def siso8601todate_filter(s):

    _date = datetime.strptime(s, IS08601_DATE_FORMAT)
    return _date


def radi_type_filter(s):
    return "RADI{}".format(s[4:].zfill(2))

def dcps_filter(movie):
    for _i in movies.dcp:
        print _i
    return "coucou"

def get_distributor_filter(dcpid):
    dcp = Dcp.query.get_or_404(dcpid)
    return dcp.movie.distributor

def get_movie_filter(dcpid):
    dcp = Dcp.query.get_or_404(dcpid)
    return dcp.movie

def get_authorisations_filter(movieid, exhibitorid):
    authorisations = Distribution.query.join(Dcp,Movie).\
        filter(Movie.movieid == int(movieid),
            Distribution.exhibitorid == int(exhibitorid),
            Dcp.valid == True).\
        all()
    return authorisations

def is_movie_authorized_filter(movieid, exhibitorid):
    movie_isauthorised = False
    movie = Movie.query.get(movieid)
    if movie.dcps:
        movie_isauthorised = True
        for _d in movies.dcps:
            permissions = dcps.exhibitors.filter_by(exhibitorid = exhibitorid).first()
            movie_isauthorised = movie_isauthorised & permissions.isauthorised
    return movie_isauthorised

def iptinc_to_web_filter(iptinc):
    try:
        web = iptinc.split('.')[3]
    except IndexError:
        return "Invalid"
    else:
        web = web + '.cinema.indecp.org'
        return web

def filesizeformat_gb_filter(raw_value):
    if raw_value is not None:
        return u'{:8.2f} {}'.format((raw_value*1.0)/10**9, 'Go')
    return None

def deny_cause_fr_filter(movie_request):
    DENY_CAUSE_FR =\
    [
        u'Non défini',
        u'Livraison du DCP par voie postale',
        u'Téléchargement du DCP via un autre prestataire',
        u'Nouvelle version du DCP à venir',
        u'Film non programmé dans votre salle',
        u'Autre',
    ]
    return DENY_CAUSE_FR[movie_request.deny_causeId]
