# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import os
from flask import Flask, current_app
from flask_mail import Mail
from config import config
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_babel import Babel
from flask_wtf.csrf import CSRFProtect
from flask_httpauth import HTTPBasicAuth


db = SQLAlchemy()
ma = Marshmallow()
bootstrap = Bootstrap()
mail = Mail()
login_manager = LoginManager()

babel = Babel()
login_manager.login_view = 'auth.login'
login_manager.login_message = u"Connectez vous pour accéder à cette page"
csrf = CSRFProtect()
apiauth = HTTPBasicAuth()

import filters


def register_blueprints(app):
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)


    # Admin blueprints
    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint)


    from .admin import admin_movies as admin_movies_blueprint
    app.register_blueprint(admin_movies_blueprint, url_prefix = "/admin/movies")

    from .admin import admin_distributors as admin_distributors_blueprint
    app.register_blueprint(admin_distributors_blueprint, url_prefix = "/admin/distributors")


    from .admin import admin_exhibitors as admin_exhibitors_blueprint
    app.register_blueprint(admin_exhibitors_blueprint, url_prefix = "/admin/exhibitors")


    from .admin import admin_dcps as admin_dcps_blueprint
    app.register_blueprint(admin_dcps_blueprint, url_prefix = "/admin/dcps")

    from .admin import admin_catalogues as admin_catalogues_blueprint
    app.register_blueprint(admin_catalogues_blueprint, url_prefix = "/admin/catalogues")

    from .admin import admin_shorts as admin_shorts_blueprint
    app.register_blueprint(admin_shorts_blueprint, url_prefix = "/admin/shorts")

    from .admin import admin_tags as admin_tags_blueprint
    app.register_blueprint(admin_tags_blueprint, url_prefix = "/admin/tags")

    from .admin import admin_movie_requests as admin_movie_requests_blueprint
    app.register_blueprint(admin_movie_requests_blueprint, url_prefix = "/admin/movie_requests")

    from .admin import admin_authorisations as admin_authorisations_blueprint
    app.register_blueprint(admin_authorisations_blueprint, url_prefix = "/admin/authorisations")





    # torrent blueprints
    from .torrent import torrent as torrent_blueprint
    app.register_blueprint(torrent_blueprint)

    # API blueprints
    from .pelle import pelle as pelle_blueprint
    app.register_blueprint(pelle_blueprint)

    # exhibitors blueprints
    from .exhibitor import exhibitor as exhibitor_blueprint
    app.register_blueprint(exhibitor_blueprint)

    from .exhibitor import exhibitor_movies as exhibitor_movies_blueprint
    app.register_blueprint(exhibitor_movies_blueprint, url_prefix = "/exhibitor/movies" )

    from .exhibitor import exhibitor_movie_requests as exhibitor_movie_requests_blueprint
    app.register_blueprint(exhibitor_movie_requests_blueprint, url_prefix = "/exhibitor/movie_requests" )


    from .exhibitor import exhibitor_distributors as exhibitor_distributors_blueprint
    app.register_blueprint(exhibitor_distributors_blueprint, url_prefix = "/exhibitor/distributors" )

    from .exhibitor import exhibitor_tags as exhibitor_tags_blueprint
    app.register_blueprint(exhibitor_tags_blueprint, url_prefix = "/exhibitor/selections" )

    from .exhibitor import exhibitor_shorts as exhibitor_shorts_blueprint
    app.register_blueprint(exhibitor_shorts_blueprint,
            url_prefix = "/exhibitor/shorts" )


    # distributor blueprints
    from .distributor import distributor as distributor_blueprint
    app.register_blueprint(distributor_blueprint)

    from .distributor import distributor_movies as distributor_movies_blueprint
    app.register_blueprint(distributor_movies_blueprint, url_prefix = "/distributor/movies" )

    from .distributor import distributor_movie_requests as distributor_movie_requests_blueprint
    app.register_blueprint(distributor_movie_requests_blueprint, url_prefix = "/distributor/movie_requests" )




def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    db.init_app(app)
    ma.init_app(app)
    bootstrap.init_app(app)
    mail.init_app(app)
    login_manager.init_app(app)
    babel.init_app(app)
    csrf.init_app(app)

    register_blueprints(app)

    app.jinja_env.filters['mydatetime'] = filters.mydatetime_filter
    app.jinja_env.filters['datefr'] = filters.datefr_filter
    app.jinja_env.filters['siso8601todate'] = filters.siso8601todate_filter
    app.jinja_env.filters['siso8601todatetime'] = filters.siso8601todatetime_filter
    app.jinja_env.filters['radi_type'] = filters.radi_type_filter
    app.jinja_env.filters['get_distributor'] = filters.get_distributor_filter
    app.jinja_env.filters['get_movie'] = filters.get_movie_filter
    app.jinja_env.filters['is_movie_authorized'] = filters.get_distributor_filter
    app.jinja_env.filters['get_authorisations'] = filters.get_authorisations_filter
    app.jinja_env.filters['iptinc_to_web'] = filters.iptinc_to_web_filter
    app.jinja_env.filters['filesizeformat_gb'] = filters.filesizeformat_gb_filter
    app.jinja_env.filters['deny_cause_fr'] = filters.deny_cause_fr_filter

    app.jinja_env.add_extension('jinja2.ext.loopcontrols')
    return app


