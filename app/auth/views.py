# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4


from flask import render_template, session, redirect, url_for, flash
from flask import current_app, request, jsonify

from . import auth
from .. import db
from ..models import User, Exhibitor, Distributor, Distribution

from flask_login import login_user, logout_user, login_required, current_user
from .forms import LoginForm, RegistrationForm, AdminRegistrationForm, ApiUserAddForm
from .forms import NewExhibitorForm, RegeneratePasswordForm
from ..email import send_email
from ..decorators import admin_required

from app.TorrentsModel import Client

@auth.before_app_request
def before_request():
    if current_user.is_authenticated():
        current_user.ping()
        if not current_user.confirmed \
                and request.endpoint[:5] != 'auth.' \
                and request.endpoint[:6] != 'pelle.' \
                and request.endpoint != 'static':
            return redirect(url_for('auth.unconfirmed'))

@auth.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous() or current_user.confirmed:
        return redirect(url_for('main.index'))
    return render_template('auth/unconfirmed.html')

def do_login(form):
    # login and validate the user...
    user = form.user
    login_user(user)
    #flash("Logged in successfully. as %s"%user.email)
    _msg = u'user {} connected'.format(user.login)
    current_app.logger.info(_msg)

    if current_user.is_distributor:
        return redirect(request.args.get("next") or url_for("distributor.home"))
    elif current_user.is_exhibitor:
        return redirect(request.args.get("next") or url_for("exhibitor.home"))
    elif current_user.is_admin:
        return redirect(request.args.get("next") or url_for("admin.home"))
    else:
        return redirect(request.args.get("next") or url_for("main.index"))



@auth.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        return do_login(form)
    return render_template("login.html", form=form)


@auth.route("/logout")
@login_required
def logout():
    _msg = u'user {} discconnected'.format(current_user.login)
    logout_user()
    current_app.logger.info(_msg)
    return redirect(url_for("main.index"))

@auth.route('/admin_register', methods=['GET', 'POST'])
@login_required
@admin_required
def admin_register():
    form = AdminRegistrationForm()
    if form.validate_on_submit():
        new_user = User( login = form.login.data,
                     email = form.email.data,
                     user_role = form.role.data)
        if new_user.user_role == User.DISTRIBUTOR:
            _d = Distributor.query.get(form.distributor.data)
            if _d is not None:
                new_user.distributor = _d
            else :
                flash("Unknow distributor")
        elif new_user.user_role == User.EXHIBITOR:
            _e = Exhibitor.query.get(form.exhibitor.data)
            if _e is not None:
                new_user.exhibitor = _e
            else:
                flash("Unknow exhibitor")
        elif new_user.user_role == User.ADMINISTRATOR:
            _msg = "Adding new admin: {}".format(new_user.login)
            flash (_msg)
            current_app.logger.warning(_msg)
        else:
            _msg =  'Something goes wrong in role distribution'
            flash (_msg)
            current_app.logger.warning(_msg)

        db.session.add(new_user)
        db.session.commit()
        token = new_user.generate_confirmation_token()
        send_email(new_user.email, u'Confirmation de création de compte',
                   'auth/email/confirm', user=new_user,
                    token=token)
        flash(u'A confirmation email {}  has been sent to user {} by email.'.format(
            user.email, user.login))
        # TODO : redirection a réfléchir pour admin retourner vers liste des utilisateurs
        return redirect(url_for('auth.admin_register'))
    form.exhibitor.data = 0
    form.distributor.data = 0
    return render_template('auth/admin_register.html', form=form)

@auth.route('/regeneratepassword', methods=['GET', 'POST'])
@login_required
@admin_required
def regenerate_password():
    form = RegeneratePasswordForm()
    if form.validate_on_submit():
        user = User.query.filter_by(userid = form.login.data).first()

        print user
        if user.user_role == User.DISTRIBUTOR:
            pass
        else:
            _msg =  U'Only Distributors login can be regenerated'
            flash (_msg)
            current_app.logger.warning(_msg)

        token = user.generate_confirmation_token()
        send_email(user.email, u'Confirmation de création de compte',
                   'auth/email/confirm', user=user,
                    token=token)
        # TODO : redirection a réfléchir pour admin retourner vers liste des utilisateurs
        flash(u'A confirmation email {}  has been sent to user {} by email.'.format(
            user.email, user.login))
        return redirect(url_for('auth.regenerate_password'))
    return render_template('auth/regenerate_password.html', form=form)



@auth.route('/register', methods=['GET', 'POST'])
@auth.route('/register/<token>/<login>', methods=['GET', 'POST'])
@auth.route('/register/<token>/<login>/<email>', methods=['GET', 'POST'])
def register(token=None, login = None, email = None):
    if token is not None:
        new_user = User.query.filter_by(login = login).first()
        if new_user.confirm(token):
            flash(u'confirmation de création de compte: créer un mot de passe ci dessous', )
        else:
            flash( u'La confirmation de création de compte est invalide ou a expirée')
            return redirect(url_for('main.index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by( email   = form.email.data, login = form.login.data).first()
        if user is None:
            flash(u'Utilsateur non touvé identifiant ou email invalide')
            return redirect(url_for('auth.login'))
        user.set_password(form.password.data)
        user.confirmed = True
        db.session.add(user)
        db.session.commit()
        flash(u'Enregistrement terminé! Vous pouvez vous connecter')
        return redirect(url_for('main.index'))
        #return redirect(url_for('admin.list_users'))
    form.login.data = login
    form.email.data = email

    return render_template('auth/register.html', form=form)

@auth.route('/addapiuser', methods=['GET', 'POST'])
@login_required
@admin_required
def add_api_user():
    form = ApiUserAddForm()
    if form.validate_on_submit():
        user = User.query.filter_by(login = form.login.data).first()
        if user:
            flash(u'user {} already in DB'.format(new_user))
            return redirect(url_for('admin.list_users'))
        new_user = User( login = form.login.data,
                     email = form.email.data,
                     user_role = "Apiuser")
        new_user.set_password(form.password.data)
        new_user.confirmed = True
        db.session.add(new_user)
        db.session.commit()
        flash('API user {} added'.format(new_user))
        # TODO : redirection a réfléchir pour admin retourner vers liste des utilisateurs
        return redirect(url_for('admin.list_users'))
    return render_template('auth/add_api_user.html', form=form)

@auth.route('/newexhibitor', methods=['GET', 'POST'])
@login_required
@admin_required
def newexhibitor():
    form = NewExhibitorForm()
    if form.validate_on_submit():
        login = form.login.data
        password = form.password.data
        iptinc = form.iptinc.data

        _u = User(login = login,
              email = form.email.data,
              user_role = "Exhibitor")
        _u.set_password(password)
        _u.confirmed = True
        db.session.add(_u)
        _e = Exhibitor(name = form.cinema.data,
                   iptinc = iptinc,
                   user = _u,
                   tc_login = login,
                   tc_password = password)
        _e.nbscreens = 0
        db.session.add(_e)

        _tc = Client(
            ipt = iptinc,
            login = login,
            password = password,
            client_type = u'cinema')
        db.session.add(_tc)
        db.session.commit()

        Distribution.create_new_user_authorisations(_e)
        _msg = u'exhibitor {} ({}) added in tucodb'.format(_e, _u)
        current_app.logger.info(_msg)
        flash(_msg)

        return redirect(url_for('admin.list_users'))
    return render_template('auth/newexhibitor.html', form=form)


