# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

from flask_wtf import Form
#from flask import flash
from wtforms import StringField, PasswordField, RadioField, SubmitField, validators
from wtforms import ValidationError, SelectField
from app.models import User, Distributor, Exhibitor


# Login form will provide a Password field (WTForm form field)
class LoginForm(Form):
    login = StringField('Identifiant',validators=[validators.Required()])
    password = PasswordField('Mot de passe',validators=[validators.Required()])
    submit = SubmitField('Connexion')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        user = User.query.filter_by(
            login=self.login.data).first()
        if user is None:
            self.login.errors.append('Unknown username')
            return False

        if not user.check_password(self.password.data):
            self.password.errors.append('Invalid password')
            return False

        self.user = user
        return True

class RegistrationForm(Form):
    login = StringField('login',validators=[validators.Required()], default='utilisateur')
    email = StringField('Email Address', [validators.Required()])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.Length(min=6),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    submit = SubmitField('Envoyer')

    def validate_login(self, field):
        if not User.query.filter_by(login=field.data).first():
            raise ValidationError(u'Identifiant non connu')

    def validate_email(self, field):
        if not User.query.filter_by(email=field.data).first():
            raise ValidationError(u'email non connu')

class AdminRegistrationForm(Form):
    login = StringField('login',validators=[validators.Required()])
    email = StringField('Email Address', [validators.Required()])
    role = RadioField(
        'Role ?',[validators.Required()],
        choices= [(User.DISTRIBUTOR,   'Distributeur'),
                  (User.EXHIBITOR ,    'Exploitant'),
                  (User.ADMINISTRATOR, 'Administrateur')],
        default=User.EXHIBITOR)
    distributor =  SelectField('Distributeur', coerce=int)
    exhibitor   =  SelectField('Exploitant', coerce=int )
    submit = SubmitField('Envoyer')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        _dummy = (0,u'Aucun')
        self.distributor.choices = \
            [ (_d.distributorid, _d.name) for _d in Distributor.query.order_by('name') ]
        self.distributor.choices.append(_dummy)

        self.exhibitor.choices = \
            [ (_d.exhibitorid, _d.name) for _d in Exhibitor.query.order_by('name') ]
        self.exhibitor.choices.append(_dummy)

    def validate_login(self, field):
        if User.query.filter_by(login=field.data).first():
            raise ValidationError(u'Utilisateur déjà enregistré dans la base')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError(u'email déjà enregistré dans la base')

class RegeneratePasswordForm(Form):
    login = SelectField('login',validators=[validators.Required()], coerce=int)
    submit = SubmitField('Envoyer')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        _dummy = (0,u'Aucun')
        self.login.choices = \
            [ (_u.userid, u"{}  / {}  ".format( _u.login, _u.email) ) for _u in  User.query.filter_by(role=User.roles[User.DISTRIBUTOR]).order_by('login') ]
        self.login.choices.append(_dummy)




class ApiUserAddForm(Form):
    login = StringField('login',validators=[validators.Required()])
    password = StringField('API user password', [validators.Required()])
    email = StringField('Email Address', [validators.Required()])
    submit = SubmitField('Envoyer')

    def validate_login(self, field):
        if User.query.filter_by(login=field.data).first():
            raise ValidationError(u'Utilisateur déjà enregistré dans la base')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError(u'email déjà enregistré dans la base')


class DistributorRegistrationForm(Form):
    name = StringField('Nom du distributeur', [validators.Length(min=1)])

class NewExhibitorForm(Form):
    login = StringField(u'login -- Transmission login',validators=[validators.Required()])
    password = StringField(u'password -- Transmission password', [validators.Required()])
    email = StringField(u'Exhibitor Email Address', [validators.Required()])
    cinema = StringField(u'Nom du cinema', [validators.Required()])
    iptinc = StringField(u'Adresse IP tinc du cinema', [validators.Required()])
    submit = SubmitField(u'Envoyer')

    def validate_login(self, field):
        if User.query.filter_by(login=field.data).first():
            raise ValidationError(u'Utilisateur déjà enregistré dans la base')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError(u'email déjà enregistré dans la base')


