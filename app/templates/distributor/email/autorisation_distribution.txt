{% from "email/_footnote.txt" import email_legal_footnote %}
Bonjour Cinéma {{ exhibitor.name }},


{% for dcp in selected_dcps %}
{% if loop.index == 1 %}
{% if loop.length == 1 %}
Vous pouvez télécharger le DCP suivant pour {{ movie.title }}, distribué par
{{ movie.distributor.name }} :
{% else %}
Vous pouvez télécharger les DCP suivants pour   {{ movie.title }}, distribué par
{{ movie.distributor.name }} :
{% endif %}
{% endif %}
* {{dcp.name}}
{% endfor %}

Pour accéder à la fiche film et au(x) téléchargement(s) des DCP, cliquez sur le lien suivant :
{{url_for('exhibitor_movies.details', id=movie.movieid, _external=True) }}


{% for dcp in movie.get_trailers() %}
{% if loop.index == 1 %}
{% if loop.length == 1 %}
Le film annonce suivant pour  {{ movie.title }} est aussi disponible au téléchargement:
{% else %}
Les films annonces suivants pour  {{ movie.title }}  sont aussi disponibles au téléchargement:
{% endif %}
{% endif %}
* {{dcp.name}}
{% endfor %}

indé-CP se tient à votre disposition pour toute question relative au téléchargement des DCP.


Si vous avez des questions sur la distribution, contactez {{ movie.distributor.name }}:
{% if movie.distributor.contact %}
{{ movie.distributor.contact }} /
{% endif %}
{% if movie.distributor.user %}
{{ movie.distributor.user.email }} /
{% endif %}
{% if movie.distributor.phone %}
Tel: {{ movie.distributor.phone }}
{% endif %}


{{ email_legal_footnote() }}

