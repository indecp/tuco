{% macro email_legal_footnote() %}
Bien cordialement,

L’équipe indé-CP
01 86 95 02 25
contacter@indecp.org
www.indecp.org

Note: Ne pas répondre ceci est un mail automatique

Ce message et toutes les pièces jointes (ci-après le 'message') sont
confidentiels et établis à l'intention exclusive de ses destinataires
Si vous n'êtes pas destinataire de ce message, merci de le détruire et d'en
avertir immédiatement l'expéditeur. Toute utilisation ou diffusion non
autorisée est interdite. Tout message électronique est susceptible
d'altération, aussi Cinémascop / indé-CP décline toute responsabilité au
titre de ce message dans l'hypothèse où il aurait été modifié.

{% endmacro %}


