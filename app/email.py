import os.path
import sys
from threading import Thread

from flask import current_app, render_template
from flask_mail import Message
from . import mail
from jinja2.exceptions import TemplateNotFound


def send_async_email(app, msg):
    with app.app_context():
        try:
            mail.send(msg)
        except :
            _err = u'Unexpected error: {}'.format(sys.exc_info())
            app.logger.error(_err)
            _err = u'Mail not sent. To {}, Subject {} '.format(
                msg.recipients, msg.subject)
            app.logger.error(_err)
        else:
          _msg = u'Mail sent. To {}, Subject {} '.format(
                msg.recipients, msg.subject)
          app.logger.info(_msg)


def format_to(p_to):
    if isinstance(p_to, list):
        return p_to
    return [p_to]

def build_to(user):

    from app.models import User, Distributor, Exhibitor
    if not user:
        return []
    if isinstance(user, Distributor):
        return user.get_emails()
    elif isinstance(user, Exhibitor):
        return user.user.emails
    elif isinstance(user, User):
        if user.distributor :
            return user.distributor.get_emails()
        return user.emails
    else:
        return []

def build_cc(user):

    app = current_app._get_current_object()

    from app.models import User, Distributor, Exhibitor
    if not user:
        return [app.config['TUCO_ADMIN']]
    if isinstance(user, Distributor):
        return user.get_emails()
    elif isinstance(user, Exhibitor):
        emails = [app.config['TUCO_ADMIN']]
        emails = emails + user.user.emails
        return emails
    elif isinstance(user, User):
        if user.distributor :
            return user.distributor.get_emails()
        emails = [app.config['TUCO_ADMIN']]
        emails = emails + user.emails
        return emails
    else:
        return [app.config['TUCO_ADMIN']]






def send_email(to, subject, template, **kwargs):
    app = current_app._get_current_object()
    msg = Message(app.config['TUCO_MAIL_SUBJECT_PREFIX'] + ' ' + subject,
                  sender=app.config['TUCO_MAIL_SENDER'], recipients=format_to(to))
    msg.body = render_template(template + '.txt', **kwargs)
    try:
        msg.html = render_template(template + '.html', **kwargs)
    except TemplateNotFound:
        pass

    if 'cc' in kwargs:
        msg.cc = kwargs['cc']
    if 'bcc' in kwargs:
        msg.bcc = kwargs['bcc']

    if 'attached_files' in kwargs:
        for _file in kwargs['attached_files']:
            with app.open_resource(_file) as fp:
                msg.attach(os.path.basename(_file),
                          "application/x-bittorrent",
                          fp.read())
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_email_wo_template(sender, to, subject, body, **kwargs):
    app = current_app._get_current_object()
    msg = Message(app.config['TUCO_MAIL_SUBJECT_PREFIX'] + ' ' + subject,
                  sender=sender, recipients=format_to(to))

    msg.body = body
    # msg.html = render_template(template + '.html', **kwargs)
    if 'cc' in kwargs:
        msg.cc = kwargs['cc']
    if 'bcc' in kwargs:
        msg.bcc = kwargs['bcc']
    if 'attached_files' in kwargs:
        for _file in kwargs['attached_files']:
            with app.open_resource(_file) as fp:
                msg.attach(os.path.basename(_file),
                          "application/x-bittorrent",
                          fp.read())
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr
