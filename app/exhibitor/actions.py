# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import os.path

from flask import safe_join
from app import current_app, db
from app.exceptions import TucoException
from app.models import  MovieRequest, ModelException, Dcp, Message, Movie

from app.tuco_message import send_tuco_message
from app.email import send_email_wo_template
from app.TorrentsModel import  TorrentRequest

ASKFORMOVIE_TEMPLATE_DICT = {
    Movie.UNOFFICIAL : 'exhibitor/email/askformovie_unofficialV2',
    Movie.OFFICIAL_WITHOUT_FTR :
    'exhibitor/email/askformovie_official_wo_movieV2',
    Movie.OFFICIAL_WITH_FTR:  'exhibitor/email/askformovie_official_wi_movieV2',
}


def do_askformovie(movie, exhibitor, body, programed_date = None):
    app = current_app._get_current_object()
    movie_request = MovieRequest.new_request(exhibitor,movie, programed_date)
    subject = u"Demande de téléchargement de {} du cinéma {}".\
            format(movie.title, exhibitor)


    _offi = movie.officiality
    template = ASKFORMOVIE_TEMPLATE_DICT[_offi]

    try:
        Message.prepare_sending(
                msg_from = movie_request.exhibitor,
                msg_to = movie_request.distributor,
                subject = subject,
                template = template,
                payload_msg = body,
                date_progammation = programed_date,
                movie_request = movie_request,
                )
    except TucoException as err:
        app.logger.error(err)
        raise

    _msg = u"REQUEST: Movie {} ({}) requested by {}".\
            format(movie.title, movie.distributor.name, exhibitor.name)
    current_app.logger.info(_msg)


def do_start_torrent_downloads(p_dcpids, exhibitor):
    _dcps = []
    app = current_app._get_current_object()
    for dcp_id in p_dcpids:
        dcp = Dcp.query.get(dcp_id)
        if dcp is None:
            msg = "No DCP found for dcpid {} (exhibitor is {})".format(dcp_id, exhibitor.name)
            app.logger.error(msg)
            raise TucoException(msg)
        _torrent_filename =  safe_join(app.config['TORRENT_FOLDER'],
                dcp.name+".torrent")
        if not os.path.exists(_torrent_filename) :
            msg = u"No torrent file found for {} (torrent : {} ) ".format(dcp.name,_torrent_filename)
            app.logger.error(msg)
            raise TucoException(msg)
        TorrentRequest.add_torrent_by_name(exhibitor.iptinc, dcp.name)
        msg = u"start download request for {} to {} ({})".format(dcp.name, exhibitor.name, exhibitor.iptinc)
        app.logger.info(msg)
        _dcps.append(dcp)
    return _dcps
