# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request, jsonify
from flask import safe_join
from flask_login import login_required, current_user
from flask_restful import fields, marshal

from . import exhibitor_distributors
from app import db, current_app
from app.exceptions import TucoException
from app.torrent.transmission import TorrentClient
from app.decorators import exhibitor_required
from app.common import path2url, redirect_url
from app.models import Movie, Distribution, Dcp, Exhibitor, Distributor
from sqlalchemy.orm import aliased

from .forms import AskforMovieForm
from .actions import do_askformovie
from app.common import redirect_url

blueprint = exhibitor_distributors

TEMPLATES={
    'home': 'all.html',
    'details': 'details.html',
    }


def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )

@blueprint.route('/home', methods=['GET','POST'])
@blueprint.route('/')
@login_required
@exhibitor_required
def home():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor

    return render ('home',
            exhibitor = exhibitor,
            distributors = Distributor.all_valid())


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@exhibitor_required
def details(id):
    exhibitor = current_user.exhibitor
    distributor = Distributor.query.get_or_404(id) 
    return render ('details',
            exhibitor = exhibitor,
            distributor = distributor)

