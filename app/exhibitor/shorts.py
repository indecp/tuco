# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request, jsonify
from flask import send_from_directory
from flask_login import login_required, current_user

from . import exhibitor_shorts
from app import db, current_app, csrf
from app.decorators import exhibitor_required
from app.models import Short, Distributor, Dcp, Catalogue, Tag
from app.exceptions import TucoException
from app.common  import redirect_url

from .actions import do_start_torrent_downloads




TEMPLATES={
    'home': 'all.html',
    'details': 'details.html',
    }

blueprint = exhibitor_shorts

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )



@blueprint.route('/home', methods=('GET', 'POST'))
@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@exhibitor_required
def home():
    _shorts = Short.query.filter_by(valid=True).\
            order_by(Short.title.asc()).all()
    return render ('home',
            shorts = _shorts)


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@exhibitor_required
def details(id ):
    short = Short.query.get_or_404(id)
    app = current_app._get_current_object()
    return render ('details',
            short = short,
            dcps = short.dcps.all())

@blueprint.route('/start_dl/<int:id>')
@login_required
@exhibitor_required
def start_dl(id):
    exhibitor = current_user.exhibitor
    dcp = Dcp.query.get_or_404(id)
    app = current_app._get_current_object()
    try :
        do_start_torrent_downloads([id], exhibitor)
    except TucoException as err:
        flash(u'Imposssible de démarrer le transfert: {}'.format(err))
    else:
        flash(u'Téléchargement de {} demarré'.format(dcp.name))
    return redirect_url('.details', id = dcp.short.shortid )

@blueprint.route('/download/<int:id>')
@login_required
@exhibitor_required
def download(id):
    dcp = Dcp.query.get_or_404(id)
    app = current_app._get_current_object()
    return send_from_directory(app.config['TORRENT_FOLDER'],
                               dcp.name+".torrent",
                               as_attachment = True)


