# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from datetime import date, timedelta

from flask import render_template, redirect, url_for, flash, request, jsonify
from flask import safe_join
from flask_login import login_required, current_user
from flask_restful import fields, marshal

from . import exhibitor_movies
from app import db, current_app
from app.exceptions import TucoException
from app.torrent.transmission import TorrentClient
from app.decorators import exhibitor_required
from app.common import path2url, redirect_url
from app.models import Movie, Distribution, Dcp, Exhibitor, Distributor
from sqlalchemy.orm import aliased

from .forms import AskforMovieForm
from .actions import do_askformovie, do_start_torrent_downloads
from app.common import redirect_url

blueprint = exhibitor_movies

TEMPLATES={
    'home': 'all.html',
    'details': 'details.html',
    'start': 'start.html',
    'newmovies': 'newmovies.html',
    'lineup': 'lineup.html',
    'full': 'full.html',
    }

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )

distribution_fields = {
    'distributionid': fields.Integer,
    'isauthorised': fields.Boolean,
    'dcpid': fields.Integer,
    'dcp': fields.String,
    'exhibitor_cncid': fields.Integer,
    'exhibitorid': fields.Integer,
    'exhibitor': fields.String,
    'autorisation_date': fields.DateTime(dt_format='iso8601'),
    'started_transfer_date': fields.DateTime(dt_format='iso8601'),
    'finished_transfer_date': fields.DateTime(dt_format='iso8601'),
    'status': fields.String,
    'statusId': fields.Integer,
    'eta': fields.String(default='N/A'),
    'progress': fields.String(default='N/A')
}
dcps_fields = {
    'dcpid': fields.Integer,
    'name': fields.String,
    'size': fields.String,
    'torrent_creation': fields.DateTime(dt_format='iso8601'),
    'contentkind': fields.String,
    'valid': fields.Boolean,
#    'exhibitors': fields.List(fields.Nested(distribution_fields)),
    }

tags_fields = {
    'name': fields.String,
    'longname': fields.String,
    'id': fields.Integer,
        }

movies_fields = {
    'movieid': fields.Integer,
    'title': fields.String,
    'original_title': fields.String,
    'releasedate': fields.DateTime(dt_format='iso8601'),
    'distributor':fields.String(attribute=u'distributor.name'),
    'distributorid':fields.Integer(attribute=u'distributor.distributorid'),
    'nb_dcps': fields.Integer,
    'nb_valid_tlrs': fields.Integer,
    'nb_valid_ftrs': fields.Integer,
    'dcps': fields.List(fields.Nested(dcps_fields)),
    'tags':fields.List(fields.Nested(tags_fields)),
    'website_url':fields.String(attribute=u'extra.website_url'),
}

@blueprint.route('/start_dl_json', methods=['POST'])
@login_required
@exhibitor_required
def start_dl_json():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    if request.method == 'POST' :
        try :
            dcps = do_start_torrent_downloads(request.json['dcps'], exhibitor)
        except TucoException as err:
            flash(u'Imposssible de démarrer le transfert: {}'.format(err))
        else:
            if len(dcps) > 1:
                msg = u'Téléchargements de {} demarrés'.format(' ,'.join([ d.name for d in dcps]))
            else:
                msg = u'Téléchargement de {} demarré'.format(' '.join([ d.name for d in dcps]))

            flash(msg)
        return jsonify({'movie': 'loaded'})

@blueprint.route('/start_dl/<int:id>')
@login_required
@exhibitor_required
def start_dl(id):
    exhibitor = current_user.exhibitor
    dcp = Dcp.query.get_or_404(id)
    app = current_app._get_current_object()
    try :
        do_start_torrent_downloads([id], exhibitor)
    except TucoException as err:
        flash(u'Imposssible de démarrer le transfert: {}'.format(err))
    else:
        flash(u'Téléchargement de {} demarré'.format(dcp.name))
    return redirect_url('.details', id = dcp.movie.movieid )

@blueprint.route('/home', methods=['GET','POST'])
@blueprint.route('/')
@login_required
@exhibitor_required
def home():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render ('home',
            exhibitor = exhibitor)

def find_autorisation(autorisations, dcpid):
    for _auth in autorisations:
        if _auth.dcpid == dcpid: return marshal(_auth, distribution_fields)
    return None


@blueprint.route('/moviedata')
@login_required
@exhibitor_required
def moviedata():
    #NBD! performance instrumentation
    #import time
    #start = time.time()
    #print "duration: {}".format((time.time() - start))

    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    movies = Movie.query.filter(Movie.valid == True)
    _delta = date.today() - timedelta(weeks=25)
    movies = Movie.query.\
        filter(Movie.valid == True).\
        filter(Movie.releasedate > _delta).\
        all()

    data_movies = {'data': [marshal(_d, movies_fields) for _d in movies]}
    _autorisations = Distribution.query.join(Dcp).filter(Distribution.exhibitor == exhibitor).all()
    for _m in data_movies['data']:
        for _d in _m['dcps']:
            _d['autorisation'] = find_autorisation(_autorisations, _d['dcpid'])


    return jsonify(data_movies)



@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@exhibitor_required
def details(id, page = 1):
    exhibitor = current_user.exhibitor
    movie = Movie.query.filter_by(movieid = id).first()
    data_movies = {'data': [marshal(movie, movies_fields)]}
    _autorisations = Distribution.query.filter(Distribution.exhibitorid == exhibitor.exhibitorid).all()
    for _m in data_movies['data']:
        for _d in _m['dcps']:
            _d['autorisation'] = find_autorisation(_autorisations, _d['dcpid'])

    return render ('details',
            exhibitor = exhibitor,
            data = data_movies)

@blueprint.route('/newmovies', methods=['GET','POST'])
@login_required
@exhibitor_required
def newmovies():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render ('newmovies',
            exhibitor = exhibitor)

@blueprint.route('/lineup', methods=['GET','POST'])
@login_required
@exhibitor_required
def lineup():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render ('lineup',
            exhibitor = exhibitor)

@blueprint.route('/askformovie/<int:id>', methods=['GET','POST'])
@login_required
@exhibitor_required
def askformovie(id):
    movie = Movie.query.get_or_404(id)
    exhibitor = current_user.exhibitor

    form = AskforMovieForm()
    if form.validate_on_submit():
        try:
            if form.programed_date.data:
                print form.programed_date.data
            do_askformovie(movie, exhibitor, form.message.data,
               form.programed_date.data)
        except TucoException as err:
            msg = u" Imposible d'envoyer un message à {}: {}. Vous pouvez contacter Indé-CP pour résoudre ce problème".format(movie.distributor.name, err)
            flash(msg)
        else:
            _msg = u"La demande  de téléchargement du film {} a été envoyée à {}"\
            .format(movie.title, movie.distributor.name)
            flash(_msg)
        return redirect_url('.home')

    response = {
        'movie': movie.title,
        'exhibitor': exhibitor.name,
        'distributor': movie.distributor.name,
        };
    return jsonify(response)

@blueprint.route('/full', methods=['GET'])
@login_required
@exhibitor_required
def full():
    movies = Movie.all_valid()
    exhibitor = current_user.exhibitor
    return render ('full',
            exhibitor = exhibitor,
            movies = movies)
