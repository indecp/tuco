# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import os.path
import sys
from datetime import date, datetime, timedelta

from flask import render_template, session, redirect, url_for, request, current_app, flash
from flask import send_from_directory, safe_join, jsonify
from flask_login import login_required, current_user
from flask_restful import fields, marshal
from sqlalchemy import or_, and_, func
from . import exhibitor
from .forms import UserPropertiesForm
from .. import db
from ..models import User, Movie, Exhibitor, DistributionStatus, Dcp
from ..models import Distribution, MovieRequest, Short, Distributor
from ..TorrentsModel import Torrent, Client, TorrentRequest, MonitoringStatus
from ..email import send_email_wo_template, build_to
from ..decorators import exhibitor_required
from ..torrent.transmission import TorrentClient
from ..torrent.BitTorrentClient import TransmissionClient
from ..exceptions import TucoException
from ..common import path2url, redirect_url
from ..filters import mydatetime_filter
from app.torrent.tools import format_torrent_data
from app.email import send_email
import json


from app.datatables import ColumnDT, DataTables

@exhibitor.route('/exhibitor/')
@exhibitor.route('/exhibitor/home')
@login_required
@exhibitor_required
def home(page=1):
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render_template('exhibitor/exhibitor_view.html',
                           exhibitor = exhibitor
                           )

def cinema_status(exhibitor):
    res = MonitoringStatus.query.filter_by(ipt=exhibitor.iptinc).one()
    data = {
            u'exhibitor': exhibitor.name,
            u'iptinc'   : res.ipt,
            u'status'   : res.status,
            }
    return data

@exhibitor.route('/exhibitor/getcinemastatus')
@login_required
@exhibitor_required
def getcinemastatus():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    data = cinema_status(exhibitor)
    return jsonify(exhibitor=data)


@exhibitor.route('/exhibitor/download/<dcp_id>')
@login_required
@exhibitor_required
def download(dcp_id):
    dcp = Dcp.query.get_or_404(dcp_id)
    app = current_app._get_current_object()
    return send_from_directory(app.config['TORRENT_FOLDER'],
                               dcp.name+".torrent",
                               as_attachment = True)

@exhibitor.route('/exhibitor/trailers')
@login_required
@exhibitor_required
def trailers():
    _exhibitor = current_user.exhibitor
    _trailers = Dcp.query.join(Movie).\
                         filter(Dcp.contentkind == Dcp.TRAILER,
                                 Dcp.valid == True).\
                         order_by(Movie.releasedate.desc()).all()
    return render_template('exhibitor/trailers.html',
                           exhibitor = _exhibitor,
                           trailers = _trailers,
                           DistributionStatus = DistributionStatus)

#@exhibitor.route('/exhibitor/movies')
#@login_required
#@exhibitor_required
def movies():
    _exhibitor = current_user.exhibitor
    _dcp_with_autorisation = Dcp.query.join(Movie).join(Distribution).\
                         filter(Dcp.contentkind == Dcp.FEATURE,
                                Distribution.exhibitor == _exhibitor).\
                         order_by(Movie.releasedate.desc()).all()
    _dcp_without_autorisation = Dcp.query.join(Movie).join(Distribution).\
                         filter(Dcp.contentkind == Dcp.FEATURE,
                                Distribution.exhibitor != _exhibitor).\
                         order_by(Movie.releasedate.desc()).all()

    items = _exhibitor.get_movies_list_with_statusv2()

    return render_template('exhibitor/list_movies.html',
                           exhibitor = _exhibitor,
                           items = items,
                           DistributionStatus = DistributionStatus)



@exhibitor.route('/exhibitor/shorts')
@login_required
@exhibitor_required
def shorts():
    _exhibitor = current_user.exhibitor
    _shorts = Short.query.all()
    return render_template('exhibitor/shorts.html',
                           exhibitor = _exhibitor,
                           shorts = _shorts)

def torrents_example():
    client = {}
    _torrents = []
    _torrent = {
        u'name'    : u'Charlie',
        u'hash'    : u'adlsjdlkzajdlkza',
        u'progress': 85.2,
        u'status'  : u'downloading'}
    _torrents.append(_torrent)
    _torrent = {
        u'name'    : u'iFati',
        u'hash'    : u'idekjazhdkj',
        u'progress': 100.0,
        u'status'  : u'seeding'}
    _torrents.append(_torrent)
    client['name']= 'localhost'
    client['torrents']= _torrents
    return client


@exhibitor.route('/exhibitor/downloads')
@login_required
@exhibitor_required
def downloads():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    tclient = Client.query.filter_by(ipt = exhibitor.iptinc).first()
    if tclient is None:
        msg= u'Imposible de trouver un client torrent pour {}'.format(exhibitor)
        app.logger.error(msg)
        flash(msg)
        return redirect(url_for('.home'))
    return render_template('exhibitor/downloads.html',
            exhibitor = exhibitor,
            tclient = tclient)


distribution_fields = {
    'dcpid': fields.Integer,
    'dcp': fields.String,
    'exhibitor_cncid': fields.Integer,
    'exhibitorid': fields.Integer,
    'exhibitor': fields.String,
    'autorisation_date': fields.DateTime(dt_format='iso8601'),
    'started_transfer_date': fields.DateTime(dt_format='iso8601'),
    'finished_transfer_date': fields.DateTime(dt_format='iso8601'),
    'status': fields.String,
    'statusId': fields.Integer,
    'eta': fields.String(default='N/A'),
    'progress': fields.String(default='N/A')
}
dcps_fields = {
    'dcpid': fields.Integer,
    'name': fields.String,
    'size': fields.String,
    'contentkind': fields.String,
#    'exhibitors': fields.List(fields.Nested(distribution_fields)),
    }

movies_fields = {
    'movieid': fields.Integer,
    'title': fields.String,
    'original_title': fields.String,
    'releasedate': fields.DateTime(dt_format='iso8601'),
    'distributor':fields.String(attribute=u'distributor.name'),
    'dcps': fields.List(fields.Nested(dcps_fields))
}

@exhibitor.route('/exhibitor/moviedata')
def moviedata():
    app = current_app._get_current_object()
    exhibitor = Exhibitor.query.filter_by(name=u'TEST EXPLOITANT').one()
    movies = Movie.query.all()
    data_movies = {'data': [marshal(_d, movies_fields) for _d in movies]}

    _autorisations = Distribution.query.join(Dcp).filter(Distribution.exhibitor == exhibitor).all()
    data = {'autorisations': [marshal(_d, distribution_fields) for _d in _autorisations]}

    for _m in data_movies['data']:
        for _d in _m['dcps']:
            _d['autorisation'] = None
            for (idx,k) in enumerate(data['autorisations']):
                if k['dcpid'] == _d['dcpid']:
                    _d['autorisation'] = data['autorisations'][idx]
                break

    return jsonify(data_movies)


def datatable_example():
    client={}
    client['torrents'] = [
            {
                u'name' : u'Charlie',
                u'dcpname': u'DCP_CHALRIE',
                u'hash' : u'adlsjdlkzajdlkza',
                u'progress' :  u'85.2',
                u'status' : u'downloading'
                },
            {
                u'name' : u'Fati',
                u'dcpname': u'DCP_FATIMA_DASZEF',
                u'hash' : u'zoombala',
                u'progress' :  u'100',
                u'status' : u'seeding'
                }
            ]
    return client


def get_active_torrents(ipt):
    tclient = Client.query.filter_by(ipt = ipt).first()
    torrents = Torrent.query.filter(Torrent.client == tclient,
                                   Torrent.state != u'deleted').\
                            all()
    data = format_torrent_data(torrents)
    for (idx,_d) in enumerate(data):
        _d[u'DT_RowId'] = idx+1
    return data



@exhibitor.route('/exhibitor/gettorrents')
@login_required
@exhibitor_required
def gettorrents():
    exhibitor = current_user.exhibitor
    data = get_active_torrents(exhibitor.iptinc)
    return jsonify(data=data)


@exhibitor.route('/exhibitor/delete_dcp/<int:dcp_id>')
@login_required
@exhibitor_required
def delete_dcp(dcp_id):
    exhibitor = current_user.exhibitor
    dcp = Dcp.query.get_or_404(dcp_id)
    app = current_app._get_current_object()
    _msg=u"Suppression de {} en cours".format(dcp.name)
    flash(_msg)

    TorrentRequest.delete_torrent_by_hash(exhibitor.iptinc, dcp.torrent_hash)
    return  redirect(request.args.get("next") or url_for("exhibitor.downloads"))

@exhibitor.route('/exhibitor/verify_dcp/<int:dcp_id>')
@login_required
@exhibitor_required
def verify_dcp(dcp_id):
    exhibitor = current_user.exhibitor
    dcp = Dcp.query.get_or_404(dcp_id)
    app = current_app._get_current_object()
    _msg=u"Vérification de {} en cours".format(dcp.name)
    flash(_msg)

    TorrentRequest.verify_torrent_by_hash(exhibitor.iptinc, dcp.torrent_hash)
    return  redirect(request.args.get("next") or url_for("exhibitor.downloads"))


@exhibitor.route('/exhibitor/demands', methods=('GET', 'POST'))
@login_required
@exhibitor_required
def demands():
    exhibitor = current_user.exhibitor
    demands = MovieRequest.query.filter_by(exhibitor = exhibitor).all()
    return render_template('exhibitor/demands.html',
                            demands = demands)


@exhibitor.route('/exhibitor/distributiondata')
@login_required
@exhibitor_required
def distributiondata():
    """Return server side data."""
    print "exhibitor distribution data"
    # defining columns
    columns = []
    columns.append(ColumnDT('dcp.movie.title', 'movie'))
    columns.append(ColumnDT('dcp.name', 'dcp'))
    columns.append(ColumnDT('dcp.movie.distributor.name','distributor'))
    columns.append(ColumnDT('exhibitor.name', 'exhibitor' ))
    columns.append(ColumnDT('autorisation_date', 'autorisation_date',search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('started_transfer_date', 'started_transfer_date',search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('finished_transfer_date', 'finished_transfer_date',search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('status', 'statusid' ,searchable  =False))
    columns.append(ColumnDT('dcp.contentkind', 'contentkind',  searchable  =False))
    columns.append(ColumnDT('dcp.movie.movieid', 'movieid', searchable  =False))
    columns.append(ColumnDT('dcp.movie.distributor.distributorid', 'distributorid', searchable  =False))
    columns.append(ColumnDT('exhibitor.exhibitorid', 'exhibitorid' ,searchable  =False))
    columns.append(ColumnDT('dcp.dcpid', 'dcpid',  searchable  =False))

    try:
        _e = int( request.values['exhibitor'])
    except ValueError:
        _e = None

    try:
        _d = int( request.values['distributor'])
    except ValueError:
        _d = None

    try:
        _m = int( request.values['movie'])
    except ValueError:
        _m = None
    try:
        dl_status_request = request.values['dl_status_request']
    except ValueError:
        dl_status_request = None

    print dl_status_request
    query = db.session.query(Distribution).join(Dcp).join(Exhibitor).join(Movie).join(Distributor)
    if _e is not None:
        query = query.filter(Exhibitor.exhibitorid == _e)
    if _d is not None:
        query = query.filter(Distributor.distributorid == _d)
    if _m is not None:
        query = query.filter(Movie.movieid == _m)


    fromNow = datetime.now() - timedelta(weeks=8)
    if dl_status_request == 'DL_AUTHORIZED':

        query = query.filter(
                Distribution.status == DistributionStatus.DL_AUTHORIZED,
            ).\
        filter(Distribution.autorisation_date > fromNow )
    elif dl_status_request == 'DL_STARTED':
        query = query.filter(
                Distribution.status == DistributionStatus.DL_STARTED,
            ).\
        filter(Distribution.started_transfer_date > fromNow)
    elif dl_status_request == 'DL_FINISHED':
        query = query.filter(
                Distribution.status == DistributionStatus.DL_FINISHED,
            ).\
        filter(Distribution.finished_transfer_date > fromNow )
    else:
        query = query.filter(or_(
            and_(Dcp.contentkind == Dcp.FEATURE, Distribution.isauthorised ==True),
            and_( Dcp.contentkind == Dcp.TRAILER,
                or_( Distribution.autorisation_date != None,  Distribution.started_transfer_date !=None)
                )
            ))

    # instantiating a DataTable for the query and table needed
    rowTable = DataTables(request.args, Distribution, query, columns)
    # returns what is needed by DataTable
    return jsonify(rowTable.output_result())

@exhibitor.route('/exhibitor/preferences')
@login_required
@exhibitor_required
def preferences():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render_template('exhibitor/preferences.html',
                            exhibitor = exhibitor)

@exhibitor.route('/exhibitor/edit_preferences', methods=('GET', 'POST'))
@login_required
@exhibitor_required
def edit_preferences():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    form = UserPropertiesForm()
    if form.cancel.data:
        flash(u'Mise à jour annulé')
        return redirect(url_for('.preferences'))
    if request.method == 'POST' and form.validate():
        exhibitor.name           = form.name.data
        exhibitor.contact        = form.contact.data
        new_emails = []
        if exhibitor.user.email != form.email1.data:
            new_emails.append(form.email1.data)
        if exhibitor.user.email2 != form.email2.data:
            new_emails.append(form.email2.data)
        if exhibitor.user.email3 != form.email3.data:
            new_emails.append(form.email3.data)
        exhibitor.user.email     = form.email1.data
        exhibitor.user.email2     = form.email2.data
        exhibitor.user.email3     = form.email3.data
        exhibitor.phone          = form.phone.data
        exhibitor.address        = form.address.data
        exhibitor.zipcode        = form.zipcode.data
        exhibitor.dept           = form.dept.data
        exhibitor.city           = form.city.data
        exhibitor.cncid          = form.cncid.data
        exhibitor.nbscreens      = form.nbscreens.data
        exhibitor.startauto = \
        (lambda x: True if x == 'yes' else False)(form.startauto.data)
        db.session.commit()
        if new_emails != []:
            send_email(app.config['TUCO_ADMIN'],
                       u"Modification d'email pour l'exploitant {}".format(exhibitor.name),
                       u'exhibitor/email/email_modification',
                       exhibitor = exhibitor,
                       new_emails = ','.join(new_emails))

        return redirect(url_for('.preferences'))
    form.name.data = exhibitor.name
    form.contact.data = exhibitor.contact
    form.email1.data = exhibitor.user.email
    form.email2.data = exhibitor.user.email2
    form.email3.data = exhibitor.user.email3
    form.phone.data = exhibitor.phone
    form.address.data = exhibitor.address
    form.zipcode.data = exhibitor.zipcode
    form.dept.data = exhibitor.dept
    form.city.data = exhibitor.city
    form.cncid.data = exhibitor.cncid
    form.nbscreens.data = exhibitor.nbscreens
    if exhibitor.startauto:
        form.startauto.data = 'yes'
    else:
        form.startauto.data = 'no'
    return render_template('exhibitor/edit_preferences.html',
                            exhibitor = exhibitor,
                            form = form)
