# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request, abort
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError

from . import exhibitor_tags
from app import db, current_app
from app.decorators import exhibitor_required
from app.models import Tag, User

TEMPLATES={
    'home': 'all.html',
    'details': 'details.html',
    }

blueprint = exhibitor_tags

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )

@blueprint.route('/home', methods=('GET', 'POST'))
@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@exhibitor_required
def home():
    _tags = Tag.query\
            .filter_by(valid=True)\
            .all()
    return render ('home',
            tags = _tags)

@blueprint.route('/details/<int:id>', methods=['GET'])
@blueprint.route('/details/<string:id>', methods=['GET'])
@login_required
@exhibitor_required
def details(id):
    app = current_app._get_current_object()
    if isinstance(id, int):
        tag = Tag.query.get_or_404(id)
    elif isinstance(id, unicode):
        tag = Tag.query.filter_by(name=id).first()
        if tag is None:
            _msg = u"No tag found wig id {}".format(id)
            current_app.logger.warning(_msg)
            abort(404)
    else:
        abort(404)
    return render ('details',
            tag = tag)
