# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask_wtf import FlaskForm
from wtforms import widgets
from wtforms import TextField, TextAreaField, SubmitField
from wtforms import RadioField
from wtforms import SelectField
from wtforms import StringField
from wtforms import DateField
from wtforms.fields.html5 import EmailField
from wtforms import validators, ValidationError
class ContactForm(FlaskForm):
    name = TextField(u"Name",  [validators.Required()])
    email = TextField(u"Email",  [validators.Required()])
    subject = TextField(u"Sujet",  [validators.Required()])
    message = TextAreaField(u"Message",  [validators.Required()])
    submit = SubmitField(u"Send")

class AskforMovieForm(FlaskForm):
    name = TextField(u"Exploitant",  [validators.Required()])
    to = TextField(u"Envoyer à",  [validators.Required()])
    movie = TextField(u"Film",  [validators.Required()])
    programed_date = DateField(u"Date programmation", format=u"%d/%m/%Y", validators=(validators.Optional(),))
    message = TextAreaField(u"Message")
    submit = SubmitField(u"Envoyer le message")

class ReadonlyTextField(TextField):
      def __call__(self, *args, **kwargs):
          kwargs.setdefault('readonly', True)
          return super(ReadonlyTextField, self).__call__(*args, **kwargs)

class UserPropertiesForm(FlaskForm):
    name =      ReadonlyTextField(u"Cinéma")
    cncid =     TextField(u"Numéro d'autorisation CNC",  [validators.DataRequired()])
    contact =   TextField(u"Contact",  [validators.DataRequired()])
    email1 =     EmailField(u"Email principal",  [validators.DataRequired(),
                    validators.Email()])
    email2 =     EmailField(u"Email 2" )
    email3 =     EmailField(u"Email 3" )
    phone = StringField(u'Téléphones(s) -- séparé par des virgules')
    address =   TextField(u"Adresse",  [validators.DataRequired()])
    zipcode   = StringField(u'Code postal')
    dept =      TextField(u"Département",  [validators.DataRequired()])
    city =      TextField(u"Ville",  [validators.DataRequired()])
    nbscreens = TextField(u"Nombre d'écrans",  [validators.DataRequired()])

    startauto = SelectField(u'Démarrage automatique des transferts ?',
            validators = [ validators.Required() ],
            choices = [('yes',
                u"Oui -- Démarrer automatiquement le tansfert de DCP \
                        dés qu'il est autorisé par le distributeur"),
                ('no',
                    u'Non -- Pas de  démarrage automatique des transferts')],
                default = 'no')
    submit =    SubmitField(u"Envoyer")
    cancel =    SubmitField(u'Annuler les modifications')

