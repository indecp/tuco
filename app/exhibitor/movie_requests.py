# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from datetime import date, datetime, timedelta

from flask import render_template, redirect, url_for, flash, request, jsonify, abort
from flask import safe_join
from flask_login import login_required, current_user
from flask_restful import fields, marshal
from sqlalchemy import or_, and_

from . import exhibitor_movie_requests
from app import db, current_app
from app.exceptions import TucoException
from app.torrent.transmission import TorrentClient
from app.decorators import exhibitor_required
from app.common import path2url, redirect_url, post_json_data_to_dict
from app.models import MovieRequest, MovieRequestStatus, Movie, Message

from .forms import AskforMovieForm
from app.common import redirect_url

blueprint = exhibitor_movie_requests

TEMPLATES={
    'home': 'home.html',
    'all': 'all.html',
    'new': 'new.html',
    'details': 'details.html',
    }

message_fields = {
    'id'                : fields.Integer,
    'from_id'                : fields.Integer,
    'msg_from'                : fields.String,
    'to_id'                : fields.Integer,
    'msg_to'                : fields.String,
    'timestamp'       : fields.DateTime(dt_format='iso8601'),
    'body'            : fields.String,
    'subject'            : fields.String
    }

movie_request_fields = {
    'id'                : fields.Integer,
    'statusId'          : fields.Integer,
    'status'            : fields.String,
    'first_demand_date' : fields.DateTime(dt_format='iso8601'),
    'demand_date'       : fields.DateTime(dt_format='iso8601'),
    'allow_date'        : fields.DateTime(dt_format='iso8601'),
    'cancel_date'       : fields.DateTime(dt_format='iso8601'),
    'deny_date'         : fields.DateTime(dt_format='iso8601'),
    'deny_causeId'      : fields.Integer,
    'deny_other_cause'  : fields.String,
    'programed_date'    : fields.DateTime(dt_format='iso8601'),
    'is_autorised'      : fields.Boolean,
    'exhibitorid'       : fields.Integer,
    'exhibitor'         : fields.String(attribute="exhibitor.standard_name"),
    'movieid'           : fields.Integer,
    'movie'             : fields.String(attribute='movie.title'),
    'distributorid'     : fields.Integer,
    'distributor'       : fields.String(attribute='distributor.name'),
    'isdcpbay'          : fields.Boolean(attribute='distributor.isdcpbay'),
    'messages'          : fields.List(fields.Nested(message_fields)),
}



from app.exceptions import InvalidUsage
@blueprint.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )


@blueprint.route('/home', methods=['GET','POST'])
@blueprint.route('/pending', methods=['GET','POST'])
@blueprint.route('/')
@login_required
@exhibitor_required
def home():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render ('home',
            exhibitor = exhibitor)

@blueprint.route('/all')
@login_required
@exhibitor_required
def all():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render ('all',
            exhibitor = exhibitor)

@blueprint.route('/new')
@login_required
@exhibitor_required
def new():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    return render ('new',
            exhibitor = exhibitor)



@blueprint.route('/movie_request_data/<string:query_type>')
@login_required
@exhibitor_required
def movie_request_data(query_type):
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor

    fromNow = datetime.now() - timedelta( weeks=5)
    if query_type == 'new':
        movie_requests = MovieRequest.query.\
        filter_by(exhibitor = exhibitor).\
        filter ( or_(
           (MovieRequest.statusId == MovieRequestStatus.PENDING.value),
           (MovieRequest.statusId == MovieRequestStatus.CANCELED.value),
           and_(MovieRequest.statusId == MovieRequestStatus.ALLOWED.value,
               MovieRequest.allow_date > fromNow),
           and_(MovieRequest.statusId == MovieRequestStatus.DENIED.value,
               MovieRequest.deny_date > fromNow)
           ))\
        .all()
    elif query_type == 'pending':
        movie_requests = MovieRequest.query.\
        filter_by(exhibitor = exhibitor).\
        filter (
           (MovieRequest.statusId == MovieRequestStatus.PENDING.value),
           )\
        .all()
    else:
        movie_requests = MovieRequest.query.filter_by(exhibitor = exhibitor).all()

    marshaled = [marshal(_d, movie_request_fields) for _d in movie_requests]
    return jsonify(data=marshaled)



@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@exhibitor_required
def details(id):
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    movie_request = MovieRequest.query.get_or_404(id)
    marshaled = marshal(movie_request, movie_request_fields)
    return render ('details',
            exhibitor = exhibitor,
            data = marshaled,
            requestid = id
            )

@blueprint.route('/detailsdata/<int:id>', methods=['GET'])
@login_required
@exhibitor_required
def detailsdata(id):
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    movie_request = MovieRequest.query.get_or_404(id)
    marshaled = marshal(movie_request, movie_request_fields)
    return jsonify(data=marshaled)

@blueprint.route('/do_relance', methods=['POST'])
@login_required
@exhibitor_required
def do_relance():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    content = request.json
    movie_request = MovieRequest.query.get_or_404(content['id'])
    movie_request.update()
    try:
        Message.prepare_sending(
                msg_from = exhibitor,
                msg_to = movie_request.distributor,
                subject = u"Relance de {}  pour le film {} ".format(exhibitor, movie_request.movie.title),
                template = 'exhibitor/email/relance',
                payload_msg = u"Envoi d'une relance à {} pour le film {}".\
                        format(movie_request.movie.distributor, movie_request.movie.title),
                movie_request = movie_request
                )
    except TucoException as err:
        message = u"Imposible d'envoyer un message à {}.\
Vous pouvez contacter Indé-CP pour résoudre ce problème".\
            format(movie_request.movie.distributor.name, err)
        app.logger.warning(message)
        raise InvalidUsage(message, status_code=410)
    return jsonify( marshal(movie_request,movie_request_fields))


@blueprint.route('/do_cancel', methods=['POST'])
@login_required
@exhibitor_required
def do_cancel():
    app = current_app._get_current_object()
    exhibitor = current_user.exhibitor
    content = post_json_data_to_dict(request.json)
    movie_request = MovieRequest.query.get_or_404(content['movierequestid'])
    movie_request.cancel()
    try:
        Message.prepare_sending(
                msg_from = exhibitor,
                msg_to = movie_request.distributor,
                subject = u"Annulation de demande par {} ".format(exhibitor),
                template = 'exhibitor/email/cancel',
                payload_msg = content[u'message'],
                movie_request = movie_request,
                )
    except TucoException as err:
        message = u"Imposible d'envoyer un message à {}.\
Vous pouvez contacter Indé-CP pour résoudre ce problème".\
            format(movie_request.movie.distributor.name, err)
        app.logger.warning(message)
        raise InvalidUsage(message, status_code=410)
    return jsonify( marshal(movie_request,movie_request_fields))




@blueprint.route('/do_contact', methods=['POST'])
@login_required
@exhibitor_required
def do_contact():
    exhibitor = current_user.exhibitor
    app = current_app._get_current_object()
    content = post_json_data_to_dict(request.json)

    movie= Movie.query.get_or_404(content['movieid'])
    movie_request = MovieRequest.query.get_or_404(content['movierequestid'])
    try:
        Message.prepare_sending(
                msg_from = exhibitor,
                msg_to = movie_request.distributor,
                subject = u"Message de {} ".format(exhibitor),
                template = 'exhibitor/email/contact',
                payload_msg = content[u'message'],
                movie_request = movie_request,
                )
    except TucoException as err:
        message = u"Imposible d'envoyer un message à {}.\
Vous pouvez contacter Indé-CP pour résoudre ce problème".\
            format(movie_request.movie.distributor.name, err)
        app.logger.warning(message)
        raise InvalidUsage(message, status_code=410)
    return jsonify({'status': 'ok'})

