from flask import Blueprint

exhibitor = Blueprint('exhibitor', __name__)
exhibitor_movies = Blueprint('exhibitor_movies', __name__, template_folder="exhibitor/movies/")
exhibitor_movie_requests = Blueprint('exhibitor_movie_requests', __name__, template_folder="exhibitor/movie_requests/")
exhibitor_distributors = Blueprint('exhibitor_distributors', __name__, template_folder="exhibitor/distributors/")
exhibitor_tags = Blueprint('exhibitor_tags', __name__, template_folder="exhibitor/tags/")
exhibitor_shorts = Blueprint('exhibitor_shorts', __name__, template_folder="exhibitor/shorts/")


from . import views
from . import movies
from . import movie_requests
from . import distributors
from . import forms
from . import actions
from . import tags
from . import shorts
