#!/usr/bin/python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4
#
#
# Copyright Nicolas Bertrand (nico@isf.cc), 2014
#
# This file is part of DcpIngest.
#
#    DcpIngest is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DcpIngest is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DcpIngest.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Usage:
# Copy and Verrify a DCP

import argparse
import time
import os
import logging
import sys

import MySQLdb
import binascii
import ipaddr
import time
from datetime import date, datetime

from flask import current_app

class XbtDB(object):
    STARTED = 2
    COMPLETED = 1

    def __init__(self):
        try:
            app = current_app._get_current_object()
            print "XBT_HOST:", app.config['XBT_HOST']
            self.db = MySQLdb.connect(app.config['XBT_HOST'],
                                      app.config['XBT_USER'],
                                      app.config['XBT_PASS'],
                                      app.config['XBT_DB_NAME'])
            self.cursor = self.db.cursor(MySQLdb.cursors.DictCursor)
        except MySQLdb.Error, e:
            print e.args
            logging.error(' %d: %s' % (e.args[0], e.args[1]))
            sys.exit(1)
        else:
            logging.debug('Connected to DB {}'.format(app.config['XBT_HOST']))

    def _execute_select(self, p_query, p_query_params = None):
        if p_query_params is None : 
            _lines = self.cursor.execute(p_query)
        else :
            _lines = self.cursor.execute(p_query, p_query_params)
        _xbt_result =[]
        while True:
            row = self.cursor.fetchone()
            if row == None:
                break
            _xbt = {}
            _xbt['id']         = int(row['id'])
            _xbt['hash']       = binascii.hexlify(row['info_hash'])
            _xbt['event']      = int(row['event'])
            _xbt['downloaded'] = int(row['downloaded'])
            _xbt['left0'] = int(row['left0'])
            _xbt['ts']      = datetime.fromtimestamp(int(row['mtime']))
            _xbt['ipa']        = ipaddr.IPv4Address(row['ipa'])
            _xbt_result.append(_xbt)
        return _xbt_result

    def get_finished_downloads(self):
        """ get all event = 1 in xbt_announce_log table
            cf http://visigod.com/xbt-tracker/table-documentation?xbt-table-documentation=&catid=8#xbt_announce_log
         """
        _query = """SELECT * FROM xbt_announce_log WHERE event = 1 """
        return self._execute_select(_query)

    def get_finished_downloads_by_ip(self, p_ipa):
        """ get all event = 1 in xbt_announce_log table
            cf http://visigod.com/xbt-tracker/table-documentation?xbt-table-documentation=&catid=8#xbt_announce_log
         """
        _query = """SELECT * FROM xbt_announce_log WHERE event = 1 AND ipa=%s """
        _ip_num = p_ipa._ip
        p_query_params = ( _ip_num,)
        return self._execute_select(_query, p_query_params)


    def get_started_downloads(self):
        """ get all event = 2 in xbt_announce_log table
            cf http://visigod.com/xbt-tracker/table-documentation?xbt-table-documentation=&catid=8#xbt_announce_log
         """
        _query = """SELECT * FROM xbt_announce_log WHERE event = 2 """
        return self._execute_select(_query)

    def get_started_and_complete_downloads(self, p_since = None):
        """ get all event = 2 in xbt_announce_log table
            cf http://visigod.com/xbt-tracker/table-documentation?xbt-table-documentation=&catid=9#xbt_announce_log
         """
        p_query_params = None
        if p_since is None:
            _query = """SELECT * FROM xbt_announce_log WHERE event = 2 or event = 1"""
        else :
            _query = """SELECT * FROM xbt_announce_log WHERE  mtime > %s AND (event = 2 or event =1) """
            p_query_params = ( int(time.mktime(p_since.timetuple())), )
        return self._execute_select(_query, p_query_params)

    def get_started_annnounce(self, p_ipa, p_hash, p_end_date = None, p_size = 0 ):
        _query = """SELECT * FROM xbt_announce_log WHERE event = 2 AND ipa = %s AND info_hash =%s AND mtime < %s AND left0 = %s ORDER by mtime DESC"""
        _ip_num = p_ipa._ip
        _ts = int(time.mktime(p_end_date.timetuple()))
        _hash = binascii.unhexlify(p_hash)
        lines = self.cursor.execute(_query, (_ip_num, _hash, _ts, p_size))
        xbt_log=[]
        while True:
            row = self.cursor.fetchone()
            if row == None:
                break
            _xbt={}
            _xbt['hash']       = binascii.hexlify(row['info_hash'])
            _xbt['event']      = int(row['event'])
            _xbt['downloaded'] = int(row['downloaded'])
            _xbt['left0'] = int(row['left0'])
            _xbt['ts']      = datetime.fromtimestamp(int(row['mtime']))
            _xbt['ipa']        = ipaddr.IPv4Address(row['ipa'])
            xbt_log.append(_xbt)
        if xbt_log :
            return xbt_log[0]
        else:
            return None

    def __del__(self):
        # Close cursor
        self.cursor.close ()
        # Close DB
        self.db.close()
        logging.debug('Database connection closed')

def xbt_annouce_logs(p_ipaddr):
    db = MySQLdb.connect(HOST, USER, PASS, DB_NAME)
    with db:
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        _ip_num = ipaddr.IPv4Address(p_ipaddr)._ip
        query = """SELECT * FROM xbt_announce_log WHERE ipa = %s """
        lines = cursor.execute(query, (_ip_num))
        xbt_log=[]
        while True:
            row = cursor.fetchone()
            if row == None:
                break
            _xbt={}
            _xbt['hash'] = binascii.hexlify(row['info_hash'])
            _xbt['event'] = row['event']
            _xbt['downloaded'] = row['downloaded']
            _xbt['mtime'] = row['mtime']
            xbt_log.append(_xbt)
        return xbt_log


def main(argv):
    logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(message)s - %(filename)s %(funcName)s line %(lineno)d thread %(thread)d/%(threadName)s',
                         level= logging.DEBUG)
    #for _log in xbt_annouce_logs('10.10.10.34'):
    #    print _log

    #print "started downloads"
    #RESULTS = DB.get_started_downloads()
    #for _res in RESULTS:
    #    print _res
    return 0
    

if __name__ == "__main__":
   sys.exit(main(sys.argv))


