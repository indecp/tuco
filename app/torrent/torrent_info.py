import os.path
import bencode
import hashlib
from datetime import datetime
class TorrentInfo(object):

    def __init__(self, p_torrent_path):
        if not os.path.exists(p_torrent_path):
            print "ERROR: torrent pah not exist"

    @classmethod
    def info(cls, p_torrent_path):
        if not os.path.exists(p_torrent_path):
            print "ERROR: torrent pah not exist"
            return None
        _torrent_info = {}
        (_info, _metainfo) = cls._get_torrent_meta(p_torrent_path)
        _torrent_info['name'] = _info['name']
        _torrent_info['hash'] = cls._calc_hash(_info)
        _torrent_info['creation_date'] = datetime.fromtimestamp(_metainfo['creation date'])
        _size = 0
        if 'files' in _info:
            for _f in _info['files']:
                _size = _size + int(_f['length'])
            _torrent_info['size'] = _size
        elif 'length' in _info:
            _torrent_info['size'] = _info['length']
        else:
            _torrent_info['size'] = -1
        return _torrent_info

    @staticmethod
    def _get_torrent_meta(p_torrent_path):
        _torrent_file = open(p_torrent_path, "rb")
        _metainfo = bencode.bdecode(_torrent_file.read())
        _info = _metainfo['info']
        _torrent_file.close()
        return (_info, _metainfo)

    @staticmethod
    def _calc_hash (p_info):
        return  hashlib.sha1(bencode.bencode(p_info)).hexdigest()

