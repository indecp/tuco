#!/usr/bin/python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import os
import logging
import sys


from flask import current_app

from transmissionrpc import Client, HTTPHandlerError, TransmissionError
from ..exceptions import TucoException

class TorrentClient(Client):

    def __init__(self, address, port=9091,user=None, password=None):
        try :
            Client.__init__(self, address, port, user, password)
        except TransmissionError as err:
            raise TucoException("TransmissionError {}".format(err))

    def add_torrent(self , p_torrent_path):
        try:
            Client.add_torrent(self, p_torrent_path)
        except TransmissionError as err:
            if u"duplicate torrent" in err.message:
                raise TucoException(u"Transfert déjà démarré")
            raise TucoException("TransmissionError {}".format(err))





def main(argv):
    logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(message)s - %(filename)s %(funcName)s line %(lineno)d thread %(thread)d/%(threadName)s',
                         level= logging.ERROR)
    logging.getLogger('transmissionrpc').setLevel(logging.ERROR)
    tc = TorrentClient('localhost', port=9091, user=None , password=None)
    data = tc.get_torrents()
    return 0

if __name__ == "__main__":
   sys.exit(main(sys.argv))


