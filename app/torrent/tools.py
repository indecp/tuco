#!/usr/bin/python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4


from datetime import datetime, timedelta
from app.models import Exhibitor,Dcp
from app.TorrentsModel import Client
from app.filters import mydatetime_filter

def format_torrent_data(torrents):
    data = []
    for _t in torrents:
        if _t.client is None:
            continue
        ddict =  {
                u'name'         : u'unknown',
                u'dcpname'      : _t.name,
                u'hash'         : _t.hash,
                u'progress'     : u'{}'.format(_t.percent_done),
                u'status'       : _t.state,
                u'update'       : _t.update.isoformat(),
                u'date_added'   : _t.date_added.isoformat(),
                u'date_done'    : _t.date_done.isoformat(),
                u'eta'          : _t.eta,
                u'error'        : _t.error,
                u'errorString'  : _t.errorString,
                u'ip'           : _t.client.ipt,
                u'contentkind'  : u'unknown',
                u'movieid'      : u'unknown',
                }

        if u'error' in ddict[u'status']:
            ddict[u'status'] = u'01.Erreur'

        if ddict[u'status'] == u'stopped':
            ddict[u'status'] = u'02.En pause'

        if ddict[u'status'] == u'downloading':
            ddict[u'status'] = u'03.En téléchargement'

        if ddict[u'status'] == u'checking':
            ddict[u'status'] = u'03.En vérification'

        if ddict[u'status'] == u'seeding':
            ddict[u'status'] = u'05.Terminé'
        if _t.eta is not None:
            if (_t.eta != -1) and ( _t.state == u'downloading'):
                ddict[u'eta'] = datetime.now() + timedelta(seconds = _t.eta)
                ddict[u'eta'] = mydatetime_filter(ddict[u'eta'])
            else:
                ddict[u'eta'] = "N/A"
        else:
            ddict[u'eta'] = "N/A"
        dcp_res = Dcp.query.filter_by(torrent_hash = _t.hash ).first()
        if dcp_res is not None:
            ddict[u'dcpid'] = dcp_res.dcpid
            if dcp_res.movie :
                ddict[u'name'] = dcp_res.movie.title
                ddict[u'movieid'] = dcp_res.movie.movieid
            elif dcp_res.short :
                ddict[u'name'] = dcp_res.short.title
            else:
                ddict[u'name'] = u'Unknwon'
            ddict[u'contentkind'] = dcp_res.contentkind
        else:
            ddict[u'name'] = ddict['dcpname']

        exhibitor = Exhibitor.query.filter_by(iptinc =  _t.client.ipt).first()
        if exhibitor is not None:
            ddict[u'exhibitor'] =  exhibitor.standard_name
            ddict[u'exhibitorid'] =  exhibitor.exhibitorid
        else:
            ddict[u'exhibitor'] =  ddict[u'ip']
            ddict[u'exhibitorid'] = None
        data.append(ddict)

    return data
