#!/usr/bin/python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import os
import logging
import sys
import json

from flask import Flask, current_app

from transmissionrpc import Client, HTTPHandlerError, TransmissionError
from app.exceptions import TucoException
from app import create_app
TRANSMISSION_CLIENT = "transmission"
DELUGE_CLIENT       = "deluge"

CLIENT_TYPE = [
        TRANSMISSION_CLIENT,
        DELUGE_CLIENT]

class BitTorrentClient(object):
    def __init__(self,client_type = TRANSMISSION_CLIENT):
        self.client_type= client_type
    def connect(self, address, port, user, password):
        pass
    def get_torrents(self):
        pass
    def add_torrent(self, torrent_path):
        pass
    def del_torrent(self, torrent_name):
        pass

class TransmissionClient(BitTorrentClient):
    def __init__(self):
        BitTorrentClient.__init__(self, TRANSMISSION_CLIENT)
        self.dict_client={}

    def connect(self, address, port, user, password):
        try :
           self.client = Client( address, port, user, password)
        except TransmissionError as err:
            raise TucoException("TransmissionError {}".format(err))

        self.dict_client['name']= address

    def get_torrents(self):
        tr_torrents = self.client.get_torrents()
        self.dict_client['torrents']= []
        for _torrent in tr_torrents:
            _torrent = {
            u'dcpname'    : _torrent.name,
            u'hash'    : _torrent.hashString,
            u'progress': _torrent.progress,
            u'status'  : _torrent.status
            }
            self.dict_client['torrents'].append(_torrent)
        return self.dict_client
    def add_torrent(self, torrent_path):
        pass
    def del_torrent(self, torrent_name):
        pass




def main(argv):
    if os.path.exists('.env'):
        print('Importing environment from .env...')
        for line in open('.env'):
            var = line.strip().split('=')
            if len(var) == 2:
                os.environ[var[0]] = var[1]


    #app = Flask(__name__)
    app = create_app(os.getenv('FLASK_CONFIG') or 'default')
    with app.app_context():
        logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(message)s - %(filename)s %(funcName)s line %(lineno)d thread %(thread)d/%(threadName)s',
                         level= logging.ERROR)


        return 0

if __name__ == "__main__":
   sys.exit(main(sys.argv))


