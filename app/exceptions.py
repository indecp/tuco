# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import current_app

class TucoException(Exception ):
    def __init__(self, message, status_code=None):
        if status_code is not None:
            self.status_code = status_code
        self.message = message

    def to_dict(self):
        rv = dict()
        rv[u'message'] = self.message
        return rv

    def __repr__(self):
        return u'<TucoException> {}'.format(self.message.encode('utf-8'))

    def __str__(self):
        return u'<TucoException> {}'.format(self.message)

class InvalidUsage(Exception):

    def __init__(self, message, status_code=None, payload=None):
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict( self.payload or ())
        rv[u'message'] = self.message
        return rv


