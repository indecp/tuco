# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

from datetime import datetime
from app import csrf, apiauth
from app.models import User
from flask import Blueprint, current_app, jsonify, make_response
from flask_restful import Api, Resource
from flask_login import login_user, current_user
from resources.foo import Foo
from resources.short import ShortResource
from resources.exhibitors import ExhibitorsResource, ExhibitorResource, ExhibitorDownloadsResource
from resources.exhibitors import ExhibitorCurrentDownloadsResource

from resources.distributors import DistributorsResource, DistributorResource, DistributorMoviesResource
from resources.distributors import DistributionResource, DistributionResourceDl

pelle = Blueprint('pelle', __name__)
api = Api(pelle, catch_all_404s=False, decorators=[csrf.exempt])

@apiauth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(login=username_or_token).first()
        if not user or not user.check_password(password):
            return False
    login_user(user)
    return True

@apiauth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access to pelle API'}), 401)

@pelle.route('/api/token')
@apiauth.login_required
def get_auth_token():
    token = current_user.generate_auth_token()
    token['token'] =  token['token'].decode('ascii')
    token['expiration'] = datetime.fromtimestamp( token['expiration']).isoformat()
    return jsonify(token)

API_VERSION=u'v1.0'
API_NAME=u'pelle'
RESOURCE_PREFIX=u"/{}/{}/".format(API_NAME,API_VERSION)

api.add_resource(Foo,RESOURCE_PREFIX  + u'Foo' , RESOURCE_PREFIX + u'Foo/<int:id>')

api.add_resource(ShortResource, RESOURCE_PREFIX + u'Short'  )
api.add_resource(ExhibitorsResource, RESOURCE_PREFIX + u'exhibitors')
api.add_resource(ExhibitorResource, RESOURCE_PREFIX + u'exhibitors/<int:cncid>')
api.add_resource(ExhibitorCurrentDownloadsResource, RESOURCE_PREFIX + u'exhibitors/<int:cncid>/current_downloads')

api.add_resource(DistributorsResource, RESOURCE_PREFIX + u'distributors')
api.add_resource(DistributorResource, RESOURCE_PREFIX + u'distributors/<int:cncid>')
api.add_resource(DistributorMoviesResource, RESOURCE_PREFIX + u'distributors/<int:cncid>/movies')

api.add_resource(DistributionResource, RESOURCE_PREFIX +u'distributions', RESOURCE_PREFIX +
u'distributions/<int:cncid>/<int:dcpid>')
#api.add_resource(DistributionResourceDl, RESOURCE_PREFIX + u'distributions/<int:cncid>/<int:dcpid>/downloads')
