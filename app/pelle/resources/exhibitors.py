# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import os.path
import json

from flask_restful import  Resource, reqparse, fields, marshal, abort, marshal_with

from flask_login import login_required, current_user

from app.models import Exhibitor
from app.TorrentsModel import Torrent, Client

from app.decorators import admin_required, apiuser_required
from app.torrent.tools import format_torrent_data

from sqlalchemy.orm.exc import NoResultFound

from app import apiauth as auth


# method with auth with token:
# cf . http://blog.miguelgrinberg.com/post/restful-authentication-with-flask

exhibitors_fields = {
    'exhibitorid': fields.Integer,
    'cncid': fields.Integer,
    'name': fields.String,
    'city': fields.String,
}

exhibitor_fields = {
    'exhibitorid': fields.Integer,
    'cncid': fields.Integer,
    'name': fields.String,
    'city': fields.String,
}

download_fields = {
    'dcpname': fields.String,
    'status': fields.String,
    'eta': fields.String,
    'progress': fields.Fixed(decimals=2)
}


class ExhibitorsResource(Resource):
    decorators=[apiuser_required, auth.login_required]
    def __init__(self):
        super(ExhibitorsResource, self).__init__()

    @marshal_with(exhibitors_fields, envelope=u'exhibitors')

    def get(self):
        resq = Exhibitor.all_valid_exhibitors()
        return resq



class ExhibitorResource(Resource):
    decorators=[apiuser_required, auth.login_required]
    def __init__(self):
        super(ExhibitorResource, self).__init__()

    @marshal_with(exhibitors_fields, envelope=u'exhibitor')

    def get(self, cncid):
        try:
            resq = Exhibitor.query.filter_by(cncid=cncid).one()
        except NoResultFound:
            abort(404, message="No Exhibitor found with CNC ID {}".format(cncid))
        return resq


class ExhibitorCurrentDownloadsResource(Resource):
    decorators=[apiuser_required, auth.login_required]
    @marshal_with(download_fields, envelope=u'current_downoads')

    def get(self, cncid):
        resq = Exhibitor.query.filter_by(cncid=cncid).first()
        tclient = Client.query.filter_by(ipt =resq.iptinc).first()
        torrents = Torrent.query.filter(Torrent.client == tclient,
                                   Torrent.state != u'deleted').\
                            all()
        if torrents :
            data = format_torrent_data(torrents)
            return data
        else :
            abort(404, message=u"No current downalods found for {} (CNC ID {})".format(resq.name, cncid))


class ExhibitorDownloadsResource(Resource):
    decorators = [login_required, admin_required]

    def get(self, cncid):
        pass

