# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import os.path
import json

from flask import current_app, g
from flask_restful import  Resource, reqparse, fields, marshal, abort
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError

from app.models import Short, Dcp, User, Distributor, Catalogue, Extra
from app import db
from app.torrent.torrent_info import TorrentInfo
from app.decorators import admin_required

shorts = [
    {
        'id': 1,
        'title': u'La blatte',
        'director': u'Agathe',
        'torrents': [u'RADI_LABLATTE_SHR'],
    },
    {
        'id': 2,
        'title': u'Le Cafard',
        'director': u'Gaspard',
        'torrents':[ u'RADI_LECAFARD_SHR'],
    },

]

dcps_fields = {
    'dcpid': fields.Integer,
    'name': fields.String,
    'size': fields.String,
    'contentkind': fields.String,
    }


short_fields = {
    'id': fields.Integer,
    'valid': fields.Boolean,
    'title': fields.String,
    'director': fields.String,
    'tags': fields.List(fields.String),
    'catalogues': fields.List(fields.String),
    'dcps': fields.List(fields.Nested(dcps_fields))
}



# method with auth with token:
# cf . http://blog.miguelgrinberg.com/post/restful-authentication-with-flask
# Not implemented
#from flask_httpauth import HTTPBasicAuth
#auth = HTTPBasicAuth()
#@auth.verify_password
#def verify_password(username_or_token, password):
#    # first try to authenticate by token
#    user = User.verify_auth_token(username_or_token)
#    if not user:
#        # try to authenticate with username/password
#        user = User.query.filter_by(login=username_or_token).first()
#        if not user or not user.check_password(password):
#            return False
#    g.user = user
#    return True


class ShortResource(Resource):
    decorators = [login_required, admin_required]
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('title', type=unicode, location='json')
        self.reqparse.add_argument('director', type=unicode, location='json')
        self.reqparse.add_argument('url', type=unicode, location='json')
        self.reqparse.add_argument('torrents', type=list, location='json')
        super(ShortResource, self).__init__()

    def get(self):
        resq = Short.query.all()

        return {'shorts': [marshal(short, short_fields) for short in resq]}

    def post(self):
        args = self.reqparse.parse_args()
        app = current_app._get_current_object()
        new_short = {
            'id': shorts[-1]['id'] + 1,
            'title': args['title'],
            'director': args['director'],
            'url': args['url'],
            'torrents': args['torrents'],
        }
        #resq = Short.query.filter_by(title = new_short['title']).first()
        resq = None
        _distributor = Distributor.query.filter_by(name = u'AGENCE DU COURT MÉTRAGE').first()
        if _distributor is None:
            app.logger.error(u'Distributor not found')
            return

        if resq is None :
            new_row = Short(
                    title = new_short['title'],
                    director = new_short['director'],
                    distributor = _distributor,
                    )
            db.session.add(new_row)
            db.session.commit()
            catalogue = Catalogue.query.\
                filter_by(name=u"L'EXTRA COURT").first()
            if catalogue:
                new_row.catalogues.append(catalogue)
                db.session.commit()
            if new_short['url']:
                new_row.extra = Extra(website_url = new_short['url'])
                db.session.commit()

            for _t in new_short['torrents'] :
                _path = os.path.join(app.config['TORRENT_FOLDER'], _t+'.torrent')
                _torrent = TorrentInfo.info(_path)
                if _torrent is not None:
                    _dcp = Dcp(
                        name = _t ,
                        contentkind = Dcp.SHORT,
                        size = _torrent['size'],
                        torrent_hash = _torrent['hash'],
                        torrent_creation = _torrent['creation_date'],
                        short = new_row)
                    db.session.add(_dcp)
                    db.session.commit()
                else:
                   app.logger.error(u"Torrent not found {}".format(os.path.basename(_path)))
            try:
                db.session.commit()
            except IntegrityError, exc:
                app.logger.info("{}".format(exc.message))
                abort(409, message = u"Duplicate entry",
                    short=new_short['title'])

            app.logger.info(u"Adding short {}".format(new_short[u'title']).encode('utf-8'))
            return {'short': marshal(new_short, short_fields)}, 201
        else:
            app.logger.error(u"Short already in DB:  {}".format(new_short['title']).encode('utf-8'))
            abort(409, message="Short already exists",
                    short=new_short['title'],
                    shortid = resq.shortid)




