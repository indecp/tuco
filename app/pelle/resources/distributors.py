# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import os.path
import json
from datetime import datetime, timedelta

from flask import current_app
from flask_restful import  Resource, reqparse, fields, marshal, abort
from flask_restful import marshal_with
from flask_login import login_required, current_user
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from app.models import Distributor, Exhibitor, Dcp, Distribution, DistributionStatus
from app.TorrentsModel import Torrent, Client

from app.decorators import apiuser_required
from app.torrent.tools import format_torrent_data
from app import db

from exhibitors import  download_fields
from app.distributor.distribution import do_distribution
from app.exceptions import TucoException

from app import apiauth as auth
from app.email import send_email_wo_template

distributors_fields = {
    'distributorid': fields.Integer,
    'cncid': fields.Integer(attribute=u'cnc_code') ,
    'name': fields.String,
}

dcps_fields = {
    'dcpid': fields.Integer,
    'name': fields.String,
    'size': fields.String,
    'contentkind': fields.String,
    }

movies_fields = {
    'movieid': fields.Integer,
    'title': fields.String,
    'original_title': fields.String,
    'releasedate': fields.String,
    'dcps': fields.List(fields.Nested(dcps_fields))
}

distribution_fields = {
    'distributionid':fields.Integer,
    'dcpid': fields.Integer,
    'dcp': fields.String,
    'exhibitor_cncid': fields.Integer,
    'exhibitor': fields.String,
    'isauthorised':fields.Boolean,
    'autorisation_date': fields.DateTime(dt_format='iso8601'),
    'started_transfer_date': fields.DateTime(dt_format='iso8601'),
    'finished_transfer_date': fields.DateTime(dt_format='iso8601'),
    'status': fields.String,
    'statusId': fields.Integer,
    'eta': fields.DateTime(dt_format='iso8601' ),
    'progress': fields.String
}

class DistributorsResource(Resource):
    decorators=[apiuser_required, auth.login_required]
    def __init__(self):
        super(DistributorsResource, self).__init__()


    def get(self):
        resq = Distributor.query.all()
        return {'distributors': [marshal(_e, distributors_fields) for _e in resq]}



class DistributorResource(Resource):
    decorators=[apiuser_required, auth.login_required]
    def __init__(self):
        super(DistributorResource, self).__init__()


    def get(self, cncid):
        app = current_app._get_current_object()
        try :
            resq = Distributor.query.filter_by(cnc_code=cncid).one()
        except NoResultFound:
            message = u"No Distributor found with ID {}".format(cncid)
            app.logger.error(u'PELLE: {}'.format(message))
            abort(404, message=message)

        return {'distributor': marshal(resq, distributors_fields) }



class DistributorMoviesResource(Resource):
    decorators=[apiuser_required, auth.login_required]

    def get(self, cncid):
        app = current_app._get_current_object()
        try :
            resq = Distributor.query.filter_by(cnc_code=cncid).one()
        except NoResultFound:
            message = u"No Distributor found with ID {}".format(cncid)
            app.logger.error(u'PELLE: {}'.format(message))
            abort(404, message=message)

        movies = resq.movies
        return {'movies': [marshal(_d, movies_fields) for _d in movies]}


class DistributionResource(Resource):
    decorators=[apiuser_required, auth.login_required]

    DISTRIBUTION_STATUS = {
            DistributionStatus.DL_NOT_AUTHORIZED : u'Distribution not allowed',
            DistributionStatus.DL_AUTHORIZED : u'Distribution allowed',
            DistributionStatus.DL_STARTED : u'Distribution started',
            DistributionStatus.DL_FINISHED : u'Distribution finished',
            DistributionStatus.DL_ERROR : u'Distribution error',
            }


    def __init__(self):
        super(DistributionResource, self).__init__()
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('cncid', type=unicode, location='json')
        self.reqparse.add_argument('dcpid', type=unicode, location='json')

    def format_ouptut(self, p_distribution_item):
        _data ={
            'distributionid': p_distribution_item.distributionid,
            'dcpid': p_distribution_item.dcpid,
            'dcp': p_distribution_item.dcp.name,
            'exhibitor_cncid': p_distribution_item.exhibitor.cncid,
            'exhibitor': p_distribution_item.exhibitor.name,
            'isauthorised': p_distribution_item.isauthorised,
            'autorisation_date': p_distribution_item.autorisation_date,
            'started_transfer_date':  p_distribution_item.started_transfer_date,
            'finished_transfer_date': p_distribution_item.finished_transfer_date,
            'status': self.DISTRIBUTION_STATUS[p_distribution_item.status],
            'statusId': p_distribution_item.status,
            }
        if p_distribution_item.status == DistributionStatus.DL_STARTED :
            tclient = Client.query.filter_by(ipt = p_distribution_item.exhibitor.iptinc).one()
            try :
                torrent = Torrent.query.filter(Torrent.client == tclient,
                                   Torrent.hash == p_distribution_item.dcp.torrent_hash).one()
            except NoResultFound:
                return _data
            if torrent.eta != -1 :
                _data['eta'] = datetime.now() + timedelta(seconds = torrent.eta)
            _data['progress'] = u'{}'.format(torrent.percent_done)

        return _data

    @marshal_with(distribution_fields, envelope=u'distributions')
    def get(self,cncid = None , dcpid = None):
        app = current_app._get_current_object()
        app.logger.info(u'PELLE DistributionResource GET -- cncid = {} dcpid = {} '.format(cncid, dcpid))

        if cncid is None:
            message = u"No exhibitor ID given by request".format(cncid)
            app.logger.error(u'PELLE: {}'.format(message))
            abort(404, message=message)

        if dcpid is None:
            message = u"No DCP ID given by request".format(cncid)
            app.logger.error(u'PELLE: {}'.format(message))
            abort(404, message=message)


        try:
            _e = Exhibitor.query.filter_by(cncid = cncid).one()
        except NoResultFound:
            message = u"No exhibitor found with CNC ID {}".format(cncid)
            app.logger.error(u'PELLE: {}'.format(message))
            abort(404, message=message)

        try:
            _d = Distribution.query.filter_by(exhibitor = _e, dcpid = dcpid).one()
        except NoResultFound:
            message = "No distribution item found for exhibitor id {} and dcp id {}".format(cncid ,dcpid)
            app.logger.error(u'PELLE: {}'.format(message))
            abort(404, message=message)

        app.logger.info(u'PELLE DistributionResource GET -- {} / {} ({})'.\
                format(_d,
                    _d.dcp.movie.title,
                    _d.dcp.movie.distributor.name))
        return self.format_ouptut(_d)

    @marshal_with(distribution_fields, envelope=u'distributions')
    def post(self):
        app = current_app._get_current_object()
        args = self.reqparse.parse_args()

        app.logger.info(u'PELLE  DistributionResource POST {}'.format(args))

        try:
            _e = Exhibitor.query.filter_by(cncid = args[u'cncid']).one()
        except NoResultFound:
            message=u"No exhibitor found with CNC ID {}".format(args['cncid']).encode('utf-8')
            app.logger.error(u'PELLE: {}'.format(message))
            abort(404, message = message)

        try:
            _dcp = Dcp.query.filter_by(dcpid = args[u'dcpid']).one()
        except NoResultFound:
            message = u"No  DCP for dcpid {}".format(args['dcpid']).encode('utf-8')
            app.logger.error(u'PELLE: {}'.format(message))
            abort(409, message = message)

        _d = Distribution.query.filter_by(exhibitor = _e, dcp = _dcp).one()
        if _d.isauthorised and _dcp.contentkind == Dcp.FEATURE:
            message=u"DCP {}  already in distribution to {}".format(_dcp.name, _e.name)
            app.logger.warning(u'PELLE: {}'.format(message))
            abort(409, message= message)

        try:
            added_dcps = do_distribution(_dcp.movie, [_dcp], _e)
        except TucoException:
            message = u'Something goes wrong in distribution'
            app.logger.error(_msg.encode('utf-8'))
            abort(409, message= message)

        app.logger.info(u'PELLE  DistributionResource POST {}  to {}'.format(_dcp.name, _e.name ))
        if added_dcps :
            _output_data = [ self.format_ouptut(_d) for _d in added_dcps]
            send_email_wo_template(sender = app.config['TUCO_MAIL_SENDER'],
                        to = app.config['TUCO_ADMIN'],
                        subject = u'[TUCO/PELLE] Distribution via cinego',
                        body = u' Autorisation de distrinbution de {} vers {}'.format(_dcp.name, _e.name)
                        )

            return _output_data,201
        else:
            message = u'PELLE No Dcp to ditribute'
            app.logger.info(message.encode('utf-8'))
            return message , 201



    def delete(self, cncid, dcpid):
        app = current_app._get_current_object()

        app.logger.info(u'PELLE  DistributionResource DELETE -- cncid={} dcpid={} '.format(cncid, dcpid))
        try:
            _e = Exhibitor.query.filter_by(cncid = cncid).one()
        except NoResultFound:
            abort(404, message="No exhibitor found with CNC ID {}".format(cncid))

        try:
            _d = Distribution.query.filter_by(exhibitorid = _e.exhibitorid, dcpid =dcpid).one()
        except NoResultFound:
            abort(404, message="No distrubtion item found")

        if _d.dcp.contentkind == Dcp.FEATURE:
            _d.isauthorised = False
            db.session.commit()
        elif _d.dcp.contentkind == Dcp.FEATURE:
            _d.autorisation_date = None
            db.session.commit()
        else:
            abort(409, message="Only DCP of type FEATURE can be unauthorized")

        _msg = u'Distribution of {} ({}) to {} ({}) not authorized'.format(
                _d.dcp.name, dcpid,
                _e.name, cncid)
        current_app.logger.info(_msg)
        return 'deleted' ,204

class DistributionResourceDl(Resource):
    decorators=[apiuser_required, auth.login_required]

    marshal_with(download_fields, 'current_download')
    def get(self,cncid, dcpid):

        try:
            _e = Exhibitor.query.filter_by(cncid = cncid).one()
        except NoResultFound:
            abort(404, message="No exhibitor found with CNC ID {}".format(cncid))

        try:
            _d = Distribution.query.filter_by(exhibitor = _e, dcpid =dcpid).one()

        except NoResultFound:
            abort(404, message="No distrubtion item found")

        tclient = Client.query.filter_by(ipt = _d.exhibitor.iptinc).first()
        torrents = Torrent.query.filter(Torrent.client == tclient,
                                   Torrent.state != u'deleted').\
                            all()
        data = format_torrent_data(torrents)
        #import json
        #print json.dumps(data,indent=4, separators=(',', ': ')) 
        for _t in data:
            if _t['hash'] == _d.dcp.torrent_hash:
                return _t
        abort(404, message="No current download found")


