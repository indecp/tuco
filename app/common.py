# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import urlparse, urllib
from flask import request, url_for, redirect

def path2url(path):
    return urlparse.urljoin(
      'file:', urllib.pathname2url(path))

def redirect_url(default='index', **kwargs):
    return redirect(request.args.get('next') or \
           request.referrer or \
           url_for(default, **kwargs))

def post_json_data_to_dict(json):
    _dict = {}
    print json
    for i in json:
        _dict[i['name']] = i['value']
    return _dict


