# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, current_app

from app import db
from app.models import Message as TucoMessage
from app.email import build_to, send_email, send_email_wo_template


def send_tuco_message(msg_from, msg_to, subject, body=None, template=None,
        **kwargs):
    _body = None
    app = current_app._get_current_object()

    if template:
        _body = render_template(template + '.txt', **kwargs)

    if body:
        _body = body

    if not msg_from.user:
        from_id = None
    else:
        from_id = msg_from.user.userid

    if not msg_to.user:
        to_id = None
    else:
        to_id = msg_to.user.userid,



    message = TucoMessage(
            from_id = from_id,
            to_id = to_id,
            subject = subject,
            body =  _body)
    db.session.add(message)
    db.session.commit()

    if template:
        send_email(build_to(msg_to),
            subject,
            template,
            **kwargs
            )
    else:
        send_email_wo_template(
            sender = build_to(msg_from)[0],
            to = build_to(msg_to),
            subject = subject,
            body = _body,
            cc = [app.config['TUCO_ADMIN']]
            )
    return message


