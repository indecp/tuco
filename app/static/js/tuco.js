$(function(){
	t = $('#table_movies')
    	if (t) t.dataTable();

	t = $('.table-summary')
    	if (t) t.dataTable( {
        	"bPaginate": false,
	} );

});

// Disable search and ordering by default
$.extend( $.fn.dataTable.defaults, {
	searching: true,
	ordering:  true,
		language: {
			"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/French.json"
		}

} );

