// Initialisation:
// For Datatables
// Disable search and ordering by default
$.extend( $.fn.dataTable.defaults, {
    searching: true,
    ordering:  true,
    "pageLength": 50,
    language: {
        url: "/static/lang/French.json"
    }

} );

// Useful fonctions
function update_locales() {
    moment.locale('fr');

    moment.updateLocale(moment.locale(), { invalidDate: "" })
    if (typeof $.fn.dataTable.moment != 'undefined') {
        $.fn.dataTable.moment( moment.ISO_8601 );
    }
}
function render_contentkind_label(contentkind, muted) {

    var $content=$('<span>', {
        'class':'label label-default label-as-badge',
    });
    $content.text(contentkind);

    if (muted == false) {
        if (contentkind == 'FTR'){
            $content.attr('class','label label-primary label-as-badge');
        }
        else if (contentkind == 'TLR'){
            $content.attr('class','label label-info label-as-badge');
        }
        else if (contentkind == 'SHR'){
            $content.attr('class','label label-success label-as-badge');
        };
    };

    var $div=$('<div>');
    $div.append($content);
    return $content
}

function render_downlolad_button(status, dcpid) {
    var todisplay = '<div class="btn-group btn-group-sm" role="group" aria-label="...">'+
            '<a href="movies/start_dl/'+ dcpid + '"'+
                'class="btn btn-primary" role="button" title="Lancer le transfert du DCP" >'+
                '<span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>'+
            '</a>'+
            '<a href="download/'+ dcpid + '"'+
                'class="btn" role="button" title="Télécharger le fichier torrent" >'+
                '<span class="glyphicon glyphicon-save" aria-hidden="true"></span>'+
            '</a>'+
        '</div>';
     return todisplay;
}


function render_datetime_old(data, type) {
    var retdata = moment(0)
    switch (type) {
        case "sort":
            if ( data != "None" ) {
                retdata = moment(data);
                }
            break;
        default:
            if ( data != "None" ) {
                retdata = (moment(data).format("lll"));
            } else {
                retdata = "";
            }
        }
        return retdata;
}

function render_datetime(data, type) {
    if (type == "display") {
        if ( moment(data).format('lll') != moment.unix(0).format('lll') ) {
           return (moment(data).format("lll"));
        }
        else {
            return "";
        }
    } else {
        if (data) {
            //return (moment(data).format("YYYY-MM-DD, h:mm:ss"));
            return (moment(data).format());
        }
        else {
            return "";
        }
    }
}
function render_date(data, type) {
    if (type == "display") {
        if ( moment(data).format('lll') != moment.unix(0).format('lll') ) {
           return (moment(data).format("ll"));
            //return (moment(data).format("YYYY-MM-DD"));
        }
        else {
            return "";
        }
    } else {
        if (data) {
            return (moment(data).format("YYYY-MM-DD"));
        }
        else {
            return "";
        }
    }
}


function dateTimePickerSearch(p_begin, p_end, p_col, p_datatable) {
    var begin = p_begin.data("DateTimePicker").date();
    var end = p_end.data("DateTimePicker").date();
    var col = p_datatable.column(p_col);

    var tosearch =[begin.unix(),end.unix()];
    col.search(tosearch).draw()

}
function dateTimePickerClear(p_begin, p_end, p_col, p_datatable) {
    var begin = p_begin.data("DateTimePicker").date();
    var end = p_end.data("DateTimePicker").date();
    var col = p_datatable.column(p_col);

    col.search('').draw()

}

function render_filezize(data, type) {
    var res= 0 ;
    if (data == "Inconnu") {
        res = Number.MAX_SAFE_INTEGER
    } else if (data == "-1") {
        res = Number.MAX_SAFE_INTEGER
    } else {
        res = Number(data);
    }
    if (type == "display") {
        if (res ==  Number.MAX_SAFE_INTEGER) {
            return "Inconnu"
        }
        res = filesize(res)
    }
    return res
}


// For status managemement
//
var statusStr= [
{ str: 'Non défini', class: 'status-not_set'  },
{ str: 'En attente', class: 'status-pending'  },
{ str: 'Autorisé',   class: 'status-allowed'  },
{ str: 'Refusé',     class: 'status-denied'   },
{ str: 'Annulé',     class: 'status-canceled' },
];

var DenyCauseStr= [
{ str: 'Non défini',  },
{ str: 'Livraison du DCP par voie postale',  },
{ str: 'Téléchargement du DCP via un autre prestataire',    },
{ str: 'Nouvelle version du DCP à venir',      },
{ str: 'Film non programmé dans votre salle',      },
{ str: 'Autre',      },
];


var ButtonsToDisplay = {
    'exhibitor':
        {
            'NOT_SET': [ 'consult'],
            'PENDING': ['redo', 'cancel', 'contact', 'consult' ],
            'ALLOWED': ['contact' , 'consult'],
            'DENIED': ['contact', 'consult'],
            'CANCELED': ['redo', 'contact', 'consult']  ,
        },
    'distributor':
        {
            'NOT_SET': ['consult'],
            'PENDING': ['allow', 'deny', 'contact','consult'],
            'ALLOWED': ['contact', 'deny','consult'],
            'DENIED': ['allow', 'contact','consult'],
            'CANCELED': [ 'contact','consult'],
        },
    'admin':
        {
            'NOT_SET': [],
            'PENDING': ['redo', 'cancel',  'contact'],
            'ALLOWED': ['contact',  'cancel'],
            'DENIED': [ 'contact', 'cancel'],
            'CANCELED': [ 'contact', 'redo'],
        },



};


var buttontext = {
  'redo'   : 'Refaire',
  'cancel' : 'Annuler',
  'contact': 'Contacter',
  'allow'  : 'Autoriser',
  'deny'   : 'Refuser',
  'consult'   : 'Consulter',
}

function ButtonTag(button_action, text, aria_label) {
    return $('<button>', {
        'type': 'button',
        'class':'btn btn-primary action-'+button_action ,
        'text': text,
        'aria_label': aria_label
    })
}

function DisplayButtongroup(tag, status, role) {
    var buttonGroup = $('<div>', {
        'class':'btn-group',
        'role':'toolbar',
        'aria_label':'Action demandes',
    });

    ButtonsToDisplay[role][status].forEach(function(element) {
        ButtonTag(element, buttontext[element], "A button").
            appendTo(buttonGroup);
    });

    buttonGroup.appendTo(tag);
    return tag
}


