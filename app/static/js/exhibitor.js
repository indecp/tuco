function table_distribution(tableid, json_url, params ) {

    var myTable = $(tableid).DataTable({
        "processing": true,
        "serverSide": true,
        "pageLength": 50,
        "lengthMenu": [ [ 25, 50, 100, -1], [ 25, 50, 100, "Tous"] ],
        "ajax": {
            "url": json_url,
            "data": function ( d) {
                d.exhibitor = params.exhibitor
                d.distributor = params.distributor
                d.movie = params.movie
                d.dl_status_request = params.dl_status_request
            }
        },
        dom: 'Blfrtip',
        buttons: [
            'csv', 'pdf', 'print'
        ],
        "columns": [
            {
                'data':'movie',
                'name':'movie',
                "render": function ( data, type, row ) {

                    var $todisplay = $("<div>");
                    $("<a>", {
                        'href': 'movies/details/'+row.movieid,
                        'text': data
                    }).appendTo($todisplay);
                    return $todisplay.html();
                }
            },
            {
                'data': 'dcp',
                'name':'dcp',
                "render": function ( data, type, row )
                {
                    var contentkind = render_contentkind_label(row.contentkind);
                    var $todisplay = $("<div>");
                    $todisplay.append(contentkind);
                    $('<small>', {
                        'text': data}).appendTo($todisplay);
                        return $todisplay.html();
                },
            },
            {
                'data': 'distributor',
                'name': 'distributor',
                "render": function ( data, type, row )
                {
                    var $todisplay = $("<div>");
                    $("<a>", {
                        'href': 'distributors/details/'+row.distributorid,
                        'text': data
                    }).appendTo($todisplay);
                    return $todisplay.html();
                }
            },
            {
                'data': 'exhibitor',
                'name': 'exhibitor',
                "render": function ( data, type, row ) {
                    var todisplay = data;
                    return todisplay;
                }
            },
            {
                'data': 'autorisation_date',
                "render": function ( data, type, row ) {
                    return render_datetime(data,type);
                }
            },
            {
                'data': 'started_transfer_date',
                "render": function ( data, type, row ) {
                    return render_datetime(data,type);
                }
            },
            {
                'data': 'finished_transfer_date',
                "render": function ( data, type, row ) {
                    return render_datetime(data,type);
                }
            },
            {

                'data': 'statusid',
                orderable: false,
                "render": function ( data, type, row , meta ) {
                    return render_downlolad_button(row.statusid,row.dcpid);
                },
            },

        ],
        "order"  :[[4, 'desc'] ],

    });
    if (params.dl_status_request == 'DL_STARTED' ) {
        myTable.order([5, 'desc']);
    }
    // hide movie column when a movie is selected
    if (params.movie) {
        myTable.colum('movie:name').visible(false);
    }
    // hide distributor column when a distributor is selected
    if (params.distributor) {
        myTable.column('distributor:name').visible(false);
    }
    // hide exhibitor column when a exhibitor is selected
    if (params.exhibitor) {
        myTable.column('exhibitor:name').visible(false);
    }
}
