function render_authorisations( $element, data) {
    var template = $('#list-authorisation-template').html();
    $element.append(template);

    var $span = $element.find('span.authorisation-status');

    switch(data.statusId) {
        case 0:
          return;
        case 1:
            if ((data.contentkind == 'TLR') &&
                (data.autorisation_date == null))
              return;
            $span.removeClass('label-default');
            $span.addClass('label-info');
            $span.text('Distribution autorisée');
            break;
        case 2:
            $span.removeClass('label-default');
            $span.addClass('label-warning');
            $span.text('Distribution démarrée');
            break;
        case 4:
            $span.removeClass('label-default');
            $span.addClass('label-success');
            $span.text('Distribution terminée');
            break;
        default:
            return;
    }
    //$span.text(data.status);

    var $span = $element.find('span.authorisation-contentkind');
    if (data.contentkind == 'TLR') {
            $span.removeClass('label-primary');
            $span.addClass('label-default');
    }

    $span.text(data.contentkind);

    var $span = $element.find('span.authorisation-dcpname');
    $span.text(data.dcp);


    return $element
}



$(document).ready(function() {
    var dcptable =$('#ExhibitorTable').DataTable({
        "ajax": {
            "url" : "../exhibitorsdata",
            "data": function (d) {
                d.movieid = movie
            },
        },
        "order" : [[1, 'asc']],
        "searching": true,
        "paging": false,
        "info": true,
        select: {
            style:    'os',
            selector: 'td:first-child'
        },

        "columns" : [
            {
                'data':'city',
            },
            {
                'data':'name',
            },
           {
                'data':'cncid',
            },
            {
                'data':null,
                "defaultContent": "<i>Not set</i>",
                orderable: false,
                searchable: false,
                render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        var div = $("<div>");
                        var input=$('<input>', {
                            'type':'checkbox',
                            'class':'exhibitor-chkbox',
                            'name':'exhibitors',
                            'value':data['exhibitorid'],
                        }).appendTo(div);
                        return div.html();
                    }
                },
            },
            {
                'data':'authorisations',
                'render': function ( data, type, row ) {
                    var $div = $("<div>");
                    var todisplay ='';
                    data.forEach(function(element) {
                        var $res = $("<div>");
                        $div.append(render_authorisations($res,element)) ;
                    });

                    return $div.html();
                },
            },
        ],
    } );


//    $('#ExhibitorTable tbody').on('click', 'tr', function () {
//        var data = dcptable.row( this ).data();
//        alert( 'You clicked on '+data[0]+'\'s row' );
//    } );

//    $('#ExhibitorTable tbody').on('click', 'input.exhibitor-chkbox', function () {
//        var data = dcptable.row( this ).data();
//        alert( 'You clicked on '+data[0]+'\'s row' );
//    } );

//    $('#ExhibitorTable tbody').on('change', 'tr :checkbox', function () {
//        var data = dcptable.row( this.closest('tr') ).data();
//        alert( 'You clicked on '+data['name']+'\'s row' );
//    } );
/*
    $('#buttonsubmit').click(function() {
        console.log("click on button submit")
        var arr = {'exhibitors': []};
        $.each($("#ExhibitorTable").find('input:checkbox'), function () {
            if (this.checked) {
                var data = dcptable.row( this.closest('tr') ).data();
                arr['exhibitors'].push(data.exhibitorid)
            }
        });
        console.log(arr);
        var csrftoken = $('meta[name=csrf-token]').attr('content');
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken)
                }
            }
        });
        arr['movieid']='670';
        var posting = $.ajax({
            url: '../distributedcps',
            type: 'POST',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true,
            });
    });
*/

    $( "#target" ).submit(function( event ) {
        console.log('gotcha');
        var arr = {'exhibitors': []};
        $.each($("#ExhibitorTable").find('input:checkbox'), function () {
            if (this.checked) {
                var data = dcptable.row( this.closest('tr') ).data();
                arr['exhibitors'].push(data.exhibitorid)
            }
        });
        console.log(arr);
        var csrftoken = $('meta[name=csrf-token]').attr('content');
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken)
                }
            }
        });
        arr['movieid']='670';
        var posting = $.ajax({
            url: '../distributedcps',
            type: 'POST',
            data: JSON.stringify(arr),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true,
            success: function(msg) {
                window.location.href = data.redirect;
            }
        });
        event.preventDefault();
    });
    $('#distributeFormnull').on('submit', function(e) {
        //e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
        var $this = $(this); // L'objet jQuery du formulaire
        var arr = {'exhibitors': []};
        $.each($("#ExhibitorTable").find('input:checkbox'), function () {
            if (this.checked) {
                var data = dcptable.row( this.closest('tr') ).data();
                arr['exhibitors'].push(data.exhibitorid)
            }
        });
        console.log(arr);
    } );
} );

