# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request, jsonify
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError

from app.datatables import ColumnDT, DataTables

from . import admin_dcps
from app import db, current_app
from app.decorators import admin_required
from app.models import Dcp, Movie,Distribution, Distributor

from .forms import EditDcpForm
from .actions import do_deletedcp
from .marshal import authorisations_fields, dcp_fields
from flask_restful import marshal
from app.filters import filesizeformat_gb_filter



TEMPLATES={
    'home': 'all.html',
    'delete': 'delete.html',
    'details': 'details.html',
    'edit': 'edit.html',
    'new': 'new.html',
    }

blueprint = admin_dcps

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )



@blueprint.route('/home', methods=('GET', 'POST'))
@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@admin_required
def home():
    _dcps = Dcp.query\
            .limit(100)
    return render ('home',
            data = _dcps)

@blueprint.route('/delete/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def delete(id):
    _d = Dcp.query.get_or_404(id)
    if _d.valid :
        do_deletedcp(_d)
        msg = u'DCP {} supprimée(non valid)'.format(_d.name)
    else:
        do_deletedcp(_d, force = True)
        msg = u'DCP {} supprimée( définitivement)'.format(_d.name)
    flash(msg)
    msg = msg + u' ID {} par {}.'.format(_d.dcpid, current_user)
    current_app.logger.warning(msg)
    db.session.commit()
    return redirect(url_for(".home"))


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id):
    _d = Dcp.query.get_or_404(id)
    return render ('details',
           data = _d)

@blueprint.route('/edit/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def edit(id):
    dcp = Dcp.query.get_or_404(id)
    form = EditDcpForm()
    if form.validate_on_submit():
        if form.cancel.data:
            flash(u'mise à jour annulé')
            return redirect(url_for('.details', id = dcp.dcpid))
        if form.contentkind.data != dcp.contentkind:
            dcp.contentkind= form.contentkind.data
            flash(u'Type de DCP mis à jour')
        if dcp.contentkind in [Dcp.FEATURE, Dcp.TRAILER]:
            movieid = form.movie.data.split(":::")[0]
            movieid = int(movieid.strip())
            movie = Movie.query.get_or_404(movieid)
            if movie != dcp.movie:
                dcp.movie = movie
        if form.valid.data != dcp.valid:
            dcp.valid = form.valid.data
        db.session.commit()

        return redirect(url_for('.details', id = dcp.dcpid))
    form.name.data        = dcp.name
    form.valid.data       = dcp.valid
    form.contentkind.data = dcp.contentkind
    try :
        form.movie.data       = u'{}::: {}({})'.\
            format(dcp.movie.movieid,
            dcp.movie.title,
            dcp.movie.distributor.name)
    except AttributeError :
        form.movie.data = u''

    return render ('edit',
            dcp = dcp,
            form = form,
            )

@blueprint.route('getmovies', methods=['GET'])
@login_required
@admin_required
def getmovies():
    movies = Movie.query.filter_by(valid=True).all()
    data = []
    for movie in movies:
        data.append({
            'title':   u'{}'.format(movie.title),
            'movieid': u'{}'.format(movie.movieid),
            'distributor': u'{}'.format(movie.distributor.name),
            })
    return jsonify(data)


@blueprint.route('get_authorisations/<int:id>', methods=['GET'])
@login_required
@admin_required
def get_authorisations(id):
    app = current_app._get_current_object()
    dcp = Dcp.query.get_or_404(id)
    authorisations = Distribution.query.\
            filter_by(dcp=dcp).all()
    marshaled = [marshal(_d, authorisations_fields) for _d in authorisations]
    return jsonify(data=marshaled)


@blueprint.route('/dcpsdata', methods=['GET'])
@login_required
@admin_required
def dcpsdata():
    app = current_app._get_current_object()
    dcps = Dcp.query\
            .limit(100)
    marshaled = [marshal(_d, dcp_fields) for _d in dcps]
    return jsonify(data=marshaled)

@blueprint.route('/dcpsdata_serverside', methods=['GET'])
@login_required
@admin_required
def dcpsdata_serverside():
    app = current_app._get_current_object()
    columns = []
    columns.append(ColumnDT('dcpid', 'dcpid'))
    columns.append(ColumnDT('name', 'name'))
    columns.append(ColumnDT('valid', 'valid' ))
    columns.append(ColumnDT('size', 'size', filter=filesizeformat_gb_filter ))
    columns.append(ColumnDT('contentkind', 'contentkind' ))
    columns.append(ColumnDT('torrent_creation', 'torrent_creation' , search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('movie.title', 'title' ))
    columns.append(ColumnDT('movie.releasedate', 'releasedate' , search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('movie.distributor.name', 'distributor' ))
    columns.append(ColumnDT('movie.movieid', 'movieid', searchable=False ))
    columns.append(ColumnDT('movie.distributor.distributorid', 'distributorid', searchable=False ))

    query = db.session.query(Dcp).join(Movie).join(Distributor)
    # instantiating a DataTable for the query and table needed
    rowTable = DataTables(request.args, Dcp, query, columns)
    # returns what is needed by DataTable
    return jsonify(rowTable.output_result())


