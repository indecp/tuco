# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request, jsonify
from flask import send_from_directory
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
import wtforms
from wtforms import validators

from . import admin_shorts
from app import db, current_app, csrf
from app.decorators import admin_required
from app.models import Short, Distributor, Dcp, Catalogue, Tag
from app.exceptions import TucoException

from app.common import redirect_url

from .forms import NotifyDistributorForm, ShortTorrentForm
from .actions import do_deletedcp, do_notifydistributor, do_newdcp


TEMPLATES={
    'home': 'all.html',
    'delete': 'delete.html',
    'details': 'details.html',
    'edit': 'edit.html',
    'new': 'new.html',
    'upload_torrent': 'upload_torrent.html',
    }

blueprint = admin_shorts

class ShortForm(FlaskForm):
    title   =  wtforms.StringField('Title',validators=[validators.Required()])
    valid = wtforms.BooleanField('Short valid')
    director   =  wtforms.StringField('Director',)
    distributor =  wtforms.SelectField('Distributor', coerce=int)
    website_url   =  wtforms. StringField('Site web')
    catalogue =  wtforms.SelectField(' Appartient au(x) catalogue(s):', coerce = int, validators=[validators.Optional()] )
    tag =  wtforms.SelectField(' tags:', coerce = int, validators=[validators.Optional()] )
    submit = wtforms.SubmitField('Submit')

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.distributor.choices = \
            [ (_d.distributorid, _d.name) for _d in Distributor.query.order_by('name') ]

        self.catalogue.choices = \
            [ (_d.id, _d.name) for _d in Catalogue.query.order_by('name') ]

        self.tag.choices = \
            [ (_d.id, _d.name) for _d in Tag.query.order_by('name') ]

class EditShortForm(ShortForm):
    cancel = wtforms.SubmitField('Cancel')



def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )

@blueprint.route('/home', methods=('GET', 'POST'))
@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@admin_required
def home():
    _shorts = Short.query.\
            order_by(Short.title.asc()).all()
    return render ('home',
            shorts = _shorts)

@blueprint.route('/new', methods=('GET', 'POST'))
@login_required
@admin_required
def new():
    form = ShortForm()
    if form.validate_on_submit():
        short = Short( title          = form.title.data,
                    director       = form.director.data,
                   )

        _distributor = Distributor.query.get(form.distributor.data)
        if _distributor :
            short.distributor = _distributor
        short.catalogues = [Catalogue.query.get(int(cid)) for cid in form.catalogue.raw_data]

        short.tags = [Tag.query.get(int(cid)) for cid in form.tag.raw_data]
        db.session.add(short)
        db.session.commit()
        if form.website_url.data :
            if short.extra:
                extra = short.extra
                extra.website_url = form.website_url.data
            else:
                extra = Extra(website_url = form.website_url.data)
                short.extra = extra
        db.session.commit()
        return redirect(url_for(".home"))
    form.distributor.data = Distributor.query.filter_by(name='AGENCE DU COURT MÉTRAGE').first().distributorid
    return render ('new',
                    form = form)

@blueprint.route('delete/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def delete(id):
    _user = current_user
    _short = Short.query.get_or_404(id)
    if _short.valid:
        _short.valid = False
    else:
        db.session.delete(_short)
    db.session.commit()
    msg = u'fiche film pour {} supprimée'.format(_short.title)
    flash(msg)
    msg = msg + u' ID {} par {}.'.format(_short.shortid, current_user)
    current_app.logger.warning(msg)
    return redirect(url_for(".home"))


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id ):
    short = Short.query.get_or_404(id)
    app = current_app._get_current_object()
    form1 = ShortTorrentForm(prefix='form1')
    if form1.validate_on_submit():
        _filename = secure_filename(form1.torrent.data.filename)
        _path = os.path.join(app.config['TORRENT_FOLDER'], _filename)
        form1.torrent.data.save(_path)
        try:
            do_newdcp(short, _path, Dcp.SHORT)
        except TucoException as err:
            app.logger.exception(err)
            flash(u'something goes wrong : {}'.format(err))
        return redirect(url_for('.details', id = short.shortid))

    form2 =FlaskForm(prefix='form2')
    if form2.validate_on_submit():
        for _id in request.form.getlist('form2-dcps'):
            _dcp = Dcp.query.get_or_404(_id)
            _dcp = do_deletedcp(_dcp)
            flash ("Dcp {} deleted".format(_dcp.name))
        return redirect(url_for('.details', id = id))
    return render ('details',
            short = short,
            dcps = short.dcps.all(),
            form1 = form1,
            form2 = form2)

@blueprint.route('/edit/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def edit(id):
    short = Short.query.get_or_404(id)
    form = EditShortForm()
    if form.validate_on_submit():
        if form.cancel.data:
            flash(u'Mise à jour annulé')
            return redirect(url_for('.details', id = short.shortid))

        if form.title.data != short.title:
            short.title = form.title.data
            flash(u'Titre mis à jour')
        if form.valid.data != short.valid:
            short.valid = form.valid.data
        if form.director.data != short.director:
            short.director = form.director.data
            flash(u'Titre original mis à jour')
        _distributor = Distributor.query.get(form.distributor.data)
        if _distributor != short.distributor:
            flash(u'Distributeur mis à jour')
            short.distributor = _distributor
        short.catalogues = [Catalogue.query.get(int(cid)) for cid in form.catalogue.raw_data]
        short.tags = [Tag.query.get(int(cid)) for cid in form.tag.raw_data]
        if form.website_url.data :
            if short.extra:
                short.extra.website_url = form.website_url.data
            else:
                extra = Extra(website_url = form.website_url.data)
                short.extra = extra
        db.session.commit()
        return redirect(url_for('.details', id = short.shortid))

    form.title.data = short.title
    form.valid.data       = short.valid
    form.director.data = short.director
    if short.distributor :
        form.distributor.data = short.distributor.distributorid
    form.catalogue.data = [ (el.id, el.name) for el in short.catalogues ]
    form.tag.data = [ (el.id, el.name) for el in short.tags ]

    if short.extra:
        form.website_url.data = short.extra.website_url
    return render ('edit',
            short = short,
            form = form)

@blueprint.route('/download/<int:id>')
@login_required
@admin_required
def download(id):
    dcp = Dcp.query.get_or_404(id)
    app = current_app._get_current_object()
    return send_from_directory(app.config['TORRENT_FOLDER'],
                               dcp.name+".torrent",
                               as_attachment = True)

@blueprint.route('/notifydistributorDcp/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def notifydistributorDcp(id):
    dcp=Dcp.query.get_or_404(id)
    short= dcp.short
    distributor = short.distributor
    form = NotifyDistributorForm()
    if form.validate_on_submit():
        try:
            to = form.to.data.split(',')
            to = [ x.strip() for x in to ]
            do_notifydistributor(form.subject.data, to, form.message.data)
        except TucoException as err:
            flash(err.msg)
        else:
            _msg = u"La Notification pour le DCP {} à été envoyée au distributeur {}"\
            .format(dcp.name,distributor.name)
            flash(_msg)
        return redirect(url_for('.details', id = short.shortid))

    body=render_template('admin/email/notify_new_dcp.txt',
            dcp=dcp)

    to = distributor.get_emails()
    subject = u"Mise à disposition du DCP {} ".format(dcp.name)
    response = {
            'subject'  : subject,
            'to'       : ','.join(to),
            'body'     : body,
            }
    return jsonify(response)

@blueprint.route('/notifydistributorShort/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def notifydistributoShort(id):
    short= Short.query.get_or_404(id)
    distributor = short.distributor
    form = NotifyDistributorForm()
    if form.validate_on_submit():
        try:
            to = form.to.data.split(',')
            to = [ x.strip() for x in to ]
            do_notifydistributor(form.subject.data, to, form.message.data)
        except TucoException as err:
            flash(err.msg)
        else:
            _msg = u"La Notification pour le film {} à été envoyée au distributeur {}"\
            .format(short.title,distributor.name)
            flash(_msg)
        return redirect(url_for('.details', id = short.shortid))

    body=render_template('admin/email/notify_short.txt',
            short=short)

    to = distributor.get_emails()
    subject = u"Mise à disposition de DCP pour le film {} ".format(short.title)
    response = {
            'subject'  : subject,
            'to'       : ','.join(to),
            'body'     : body,
            }
    return jsonify(response)

from werkzeug import secure_filename
import os.path
@blueprint.route('/upload_torrent/<int:short>', methods=['GET','POST'])
@login_required
@admin_required
def upload_torrent(short = None):
    short = Short.query.get(short)
    app = current_app._get_current_object()
    form = ShortTorrentForm()
    if form.validate_on_submit():
        _filename = secure_filename(form.torrent.data.filename)
        _path = os.path.join(app.config['TORRENT_FOLDER'], _filename)
        form.torrent.data.save(_path)
        try:
            do_newdcp(short, _path, Dcp.SHORT)
        except TucoException as err:
            app.logger.exception(err)
            flash(u'something goes wrong : {}'.format(err))
        return redirect(url_for('.details', id = short.shortid))
    return render ('upload_torrent',
            short = short,
            form = form)

from werkzeug import secure_filename
import os.path
@blueprint.route('/uploader/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def uploader(id = None):
    app = current_app._get_current_object()
    short = Short.query.get_or_404(id)
    if request.method == 'POST':
        form = ShortTorrentForm(prefix='form1')
        if form.validate_on_submit():
            _filename = secure_filename(form.torrent.data.filename)
            _path = os.path.join(app.config['TORRENT_FOLDER'], _filename)
            form.torrent.data.save(_path)
            try:
                do_newdcp(short, _path, 'TLR')
            except TucoException as err:
                app.logger.exception(err)
                flash(u'something goes wrong : {}'.format(err))
            flash(u'Dcp uploaded');
        return redirect_url('.details', id = short.shortid )


