# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError

from flask_wtf import FlaskForm
import wtforms
from wtforms import validators

from . import admin_exhibitors
from app import db, current_app
from app.decorators import admin_required
from app.models import Exhibitor, User, Permission
from app.TorrentsModel import Client





TEMPLATES={
    'home': 'all.html',
    'delete': 'delete.html',
    'details': 'details.html',
    'edit': 'edit.html',
    'new': 'new.html',
    'startauto' : 'preferences_startauto.html',
    }

blueprint = admin_exhibitors

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )

class ExhibitorForm(FlaskForm):
    name      = wtforms.StringField(u'Exhibitor name',
                                    validators = [validators.Required()])
    login     = wtforms.StringField(u'Tuco login',
                                    validators = [validators.Required()])
    password  = wtforms.StringField(u'Tuco password',
                                    validators = [validators.Required()])
    email     = wtforms.StringField(u'email principal',
                                    validators = [validators.Required()])
    email2    = wtforms.StringField(u'email 2')
    email3    = wtforms.StringField(u'email 3')
    cncid     = wtforms.IntegerField(u'Autorisation CNC',
                                    validators = [validators.Required()])
    contact   = wtforms.StringField(u'Contact')
    phone     = wtforms.StringField(u'Téléphones(s) -- séparé par des virgules')
    address   = wtforms.StringField(u'Adresse')
    zipcode   = wtforms.StringField(u'Code postal')
    city      = wtforms.StringField(u'Ville')
    dept      = wtforms.IntegerField(u'Departement')
    nbscreens = wtforms.IntegerField(u'Nombre d ecrans', default=0)
    iptinc    = wtforms.StringField(u'IP tinc',
                                    validators = [validators.Required()])
    submit    = wtforms.SubmitField('Send')


class EditExhibitorForm(ExhibitorForm):


    user    =  wtforms.SelectField('Utilisateur', coerce=int)
    cancel    = wtforms.SubmitField('Cancel')

    def __init__(self, *args, **kwargs):
        ExhibitorForm.__init__(self, *args, **kwargs)
        del self.login
        del self.password
        self.user.choices = \
            [ (_u.userid, _u.login) for _u in User.query.filter_by(role = Permission.EXHIBIT).order_by('login') ]
        _dummy = (0,u'Aucun')
        self.user.choices.append(_dummy)

    def validate_email(self, field):
        # TODO: ADD email check only on change
        pass

@blueprint.route('/home', methods=('GET', 'POST'))

@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@admin_required
def home():
    _exhibitors = Exhibitor.query\
            .filter_by(valid=True)\
            .all()
    return render ('home',
            Exhibitor = Exhibitor,
            Client = Client,
            data = _exhibitors)

@blueprint.route('/new', methods=('GET', 'POST'))
@login_required
@admin_required
def new():
    form = ExhibitorForm()
    if form.validate_on_submit():
        _u = User(login = form.login.data.strip(),
              email = form.email.data.strip() ,
              user_role = "Exhibitor")
        _u.set_password(form.password.data.strip())
        _u.confirmed = True
        try:
            db.session.add(_u)
        except IntegrityError, exc:
            _msg = "{}".format(exc.message)
            flash(_msg)
            current_app.logger.error(_msg)
            db.session.rollback()
            return redirect(url_for(".home"))

        _tc = Client(
            ipt = form.iptinc.data,
            login = form.login.data.strip(),
            password = form.password.data.strip(),
            client_type = u'cinema')
        try:
            db.session.add(_tc)
        except IntegrityError, exc:
            _msg = "{}".format(exc.message)
            flash(_msg)
            current_app.logger.error(_msg)
            db.session.rollback()
            return redirect(url_for(".home"))


        _e = Exhibitor(
                name      = form.name.data.strip(),
                contact   = form.contact.data,
                address   = form.address.data,
                city      = form.city.data,
                dept      = form.dept.data,
                nbscreens = form.nbscreens.data,
                cncid     = form.cncid.data,
                zipcode     = form.zipcode.data,
                phone     = form.phone.data,
                iptinc    = form.iptinc.data,
                tc_login  = form.login.data.strip(),
                tc_password = form.password.data.strip(),
                user = _u
                )
        try:
            db.session.add(_e)
            db.session.commit()
        except IntegrityError, exc:
            _msg = "{}".format(exc.message)
            flash(_msg)
            current_app.logger.error(_msg)
            db.session.rollback()
        else :
            _msg = u'Exhibitor {} added'.format(_e.name)
            flash(_msg)
            current_app.logger.warning(_msg)
        return redirect(url_for(".home"))

    return render ('new',
            form = form)

@blueprint.route('/delete/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def delete(id):
    _user = current_user
    _d = Exhibitor.query.get_or_404(id)
    _d.valid = False
    msg = u'fiche film pour {} supprimée'.format(_d.name)
    flash(msg)
    msg = msg + u' ID {} par {}.'.format(_d.exhibitorid, current_user)
    current_app.logger.warning(msg)
    db.session.commit()
    return redirect(url_for(".home"))


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id):
    _e = Exhibitor.query.get_or_404(id)
    tclient = Client.query.filter_by(ipt = _e.iptinc).first()
    return render ('details',
           data     = _e,
           tclient  = tclient)

@blueprint.route('/edit/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def edit(id):
    exhibitor = Exhibitor.query.get_or_404(id)
    form = EditExhibitorForm()
    if form.validate_on_submit():
        if form.cancel.data:
            flash(u'Mise à jour annulé')
            return redirect(url_for('.home'))
        if form.name.data != exhibitor.name:
            exhibitor.name = form.name.data
            flash('Exhibitor name updated')
        if form.cncid.data != exhibitor.cncid:
            exhibitor.cncid = form.cncid.data
            flash('Exhibitor CNC code updated')
        _u = User.query.get(form.user.data)
        if _u and _u != exhibitor.user:
            exhibitor.user = _u
            db.session.add(_u)
            flash('user modified')
        if exhibitor.user:
             if exhibitor.user.email != form.email.data:
                exhibitor.user.email = form.email.data
                db.session.add(exhibitor.user)
                flash('user email modified')
        else:
            _msg='No _user for {}. Cannot set email'.format(exhibitor.name)
            flash (_msg)
        if exhibitor.user and exhibitor.user.email2 != form.email2.data:
            exhibitor.user.email2 = form.email2.data
            db.session.add(exhibitor.user)
        if exhibitor.user and exhibitor.user.email3 != form.email3.data:
            exhibitor.user.email3 = form.email3.data
            db.session.add(exhibitor.user)
        if form.contact.data != exhibitor.contact:
            exhibitor.contact = form.contact.data
            flash('Exhibitor contact updated')
        if form.address.data  != exhibitor.address:
            exhibitor.address = form.address.data
        if form.phone.data  != exhibitor.phone:
            exhibitor.phone = form.phone.data
        if form.zipcode.data  != exhibitor.zipcode:
            exhibitor.zipcode = form.zipcode.data
        if form.city.data  != exhibitor.city:
            exhibitor.city = form.city.data
            flash('Exhibitor city updated')
        if form.dept.data != exhibitor.dept:
            exhibitor.dept = form.dept.data
            flash('Exhibitor dept updated')
        if form.nbscreens.data != exhibitor.nbscreens:
            exhibitor.nbscreens = form.nbscreens.data
            flash('Exhibitor nbscreens updated')
        if form.iptinc.data != exhibitor.iptinc:
            exhibitor.iptinc = form.iptinc.data
            _tc = Client.query.filter_by(login = exhibitor.user.login).first()
            if _tc is not None:
                _tc.ipt = exhibitor.iptinc
            flash('Exhibitor iptinc updated')
        db.session.add(exhibitor)
        db.session.commit()
        return redirect(url_for('.home'))


    form.name.data      = exhibitor.name
    form.contact.data   = exhibitor.contact
    form.address.data   = exhibitor.address
    form.city.data      = exhibitor.city
    form.dept.data      = exhibitor.dept
    form.nbscreens.data = exhibitor.nbscreens
    form.iptinc.data    = exhibitor.iptinc
    form.cncid.data     = exhibitor.cncid
    if exhibitor.user:
        form.user.data   = exhibitor.user.userid
        form.email.data  = exhibitor.user.email
        form.email2.data = exhibitor.user.email2
        form.email3.data = exhibitor.user.email3
    else:
        form.user.data = 0

    return render ('edit',
            data  =  exhibitor,
            form = form)

@blueprint.route('/startauto', methods=('GET', 'POST'))
@login_required
@admin_required
def startauto():
    _exhibitors = Exhibitor.query\
            .filter_by(valid=True)\
            .all()
    return render ('startauto',
            data = _exhibitors)
