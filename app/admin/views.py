# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import os.path
from datetime import datetime, timedelta
from string import upper

from sqlalchemy.exc import IntegrityError
from sqlalchemy import or_, and_, func
from flask import render_template, session, redirect, url_for, request, current_app, flash
from flask import jsonify
from flask_login import login_required, current_user
from werkzeug import secure_filename

from . import admin
from .. import db
from ..models import User, Movie, Exhibitor, Distributor, Dcp, Short
from ..models import Distribution, DistributionStatus, TrackerMessage, MovieRequest
from ..TorrentsModel import MonitoringRequest, Torrent, MonitoringStatus
from .forms import TorrentForm, MovieForm,  EditMovieForm
from .forms import EditDistributorForm, EditUserForm
from ..decorators import admin_required

from ..filters import mydatetime_filter

from app.datatables import ColumnDT, DataTables

from app.torrent.torrent_info import TorrentInfo
from app.torrent.tools import format_torrent_data
from app.exceptions import TucoException
from .actions import do_newdcp, send_exhibitor_reminder

@admin.route('/admin/')
@admin.route('/admin/home')
@login_required
@admin_required
def home():
    _administrator = current_user
    return render_template('admin/administrator_view.html',
                           title='Tuco',
                           administrator=_administrator)

@admin.route('/admin/tests')
@login_required
@admin_required
def tests():
    _administrator = current_user
    return render_template('admin/tests.html')

@admin.route('/admin/do_test', methods=['GET','POST'])
@login_required
@admin_required
def do_test( ):
    _administrator = current_user
    from app.common import post_json_data_to_dict
    content = post_json_data_to_dict(request.json)
    print content

    return jsonify(data= {'data':'OK'})



def cinema_status():
    query = MonitoringStatus.query.all()
    data = []
    for _row in query:
        _dict = {
                u'exhibitor': u'UNKNOWN',
                u'iptinc'   : _row.ipt,
                u'status'   : _row.status,
                }
        cinema = Exhibitor.query.filter_by(iptinc = _dict[u'iptinc']).first()
        app = current_app._get_current_object()
        if cinema is not None:
            try:
                _dict[u'exhibitor'] = u"{} - {}".format(cinema.city, cinema.name)
            except UnicodeEncodeError as err:
                app.logger.exception(err)
                _dict[u'exhibitor'] = u'encoding pb'

        data.append(_dict)
    return data

@admin.route('/admin/getcinemastatus')
@login_required
@admin_required
def getcinemastatus():
    data = cinema_status()
    return jsonify(data=data)

@admin.route('/admin/upload_torrent', methods=('GET', 'POST'))
@admin.route('/admin/upload_torrent/<int:movie>', methods=['GET','POST'])
@login_required
@admin_required
def upload_torrent(movie = None):
    app = current_app._get_current_object()
    form = TorrentForm()
    if form.validate_on_submit():
        _filename = secure_filename(form.torrent.data.filename)
        _movie = Movie.query.get(form.movie.data)
        _path = os.path.join(app.config['TORRENT_FOLDER'], _filename)
        form.torrent.data.save(_path)
        try:
            do_newdcp(_movie, _path, form.contentkind.data)
        except TucoException as err:
            app.logger.exception(err)
            flash(u'something goes wrong : {}'.format(err))
        finally:
            pass


        return redirect(url_for('admin_movies.details', id = _movie.movieid))
    if movie is not None:
        form.movie.data = Movie.query.get(movie).movieid
    return render_template('admin/upload_torrent.html', form=form )



class FinishedDownload(object):
    hash = None
    name = None
    ip   = None
    reception_date = None

@admin.route('/admin/tracker_messages', methods=['GET', 'POST'])
@admin.route('/admin/tracker_messages/<int:page>', methods=['GET', 'POST'])
@login_required
@admin_required
def tracker_messages(page = 1):
    messages = TrackerMessage.query.order_by(TrackerMessage.stamp.desc()).paginate(page, 20, False)
    return render_template('admin/tracker_messages.html',
                           title='Tuco',
                           messages = messages )

@admin.route('/admin/distribution_status', methods=['GET', 'POST'])
@login_required
@admin_required
def distribution_status():
    return render_template('admin/distribution_status.html')

@admin.route('/admin/distribution_status_TLR', methods=['GET', 'POST'])
@login_required
@admin_required
def distribution_status_TLR():
    return render_template('admin/distribution_status_TLR.html')


@admin.route('/admin/list_users', methods=('GET', 'POST'))
@login_required
@admin_required
def list_users():
    users = User.query.all()
    if request.method == 'POST':
        for _id in request.form.getlist('delete'):
            _user = User.query.get_or_404(_id)
            db.session.delete(_user)
            db.session.commit()
            _msg = u'User {} deleted'.format(_user.login)
            flash(_msg)
            current_app.logger.warning(_msg)
        return redirect(url_for('admin.list_users'))
    return render_template('admin/list_users.html',
                            users = users)

@admin.route('/admin/user/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def user(id):
    user = User.query.get_or_404(id)
    form = EditUserForm()
    if form.cancel.data:
        flash(u'Mise à jour annulé')
        return redirect(url_for('admin.list_users'))
    if form.validate_on_submit():
        if form.email.data != user.email:
            user.email = form.email.data
            flash('user email modified')
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('admin.list_users'))
    form.email.data = user.email
    return render_template('admin/user.html',
                            user = user,
                            form = form)

@admin.route('/admin/list_demands', methods=('GET', 'POST'))
@login_required
@admin_required
def list_demands():
    requests = MovieRequest.query.all()
    return render_template('admin/list_demands.html',
                            demands = requests)

@admin.route('/admin/downloads')
@login_required
@admin_required
def downloads():
    app = current_app._get_current_object()

    return render_template('admin/downloads.html')

def get_active_torrents():
    torrents = Torrent.query.\
            filter(~Torrent.state.in_([u'deleted'])).\
            all()

    data = format_torrent_data(torrents)
    for (idx,_d) in enumerate(data):
        _d[u'DT_RowId'] = idx+1
    return data



@admin.route('/admin/gettorrents')
@login_required
@admin_required
def gettorrents():
    data = get_active_torrents()
    return jsonify(data=data)

@admin.route('/admin/shorts')
@login_required
@admin_required
def shorts():
    _shorts = Short.query.all()
    return render_template('admin/shorts.html',
                           shorts = _shorts)


@admin.route('/admin/distributiondata')
@login_required
@admin_required
def distributiondata():
    """Return server side data."""
    # defining columns
    columns = []
    columns.append(ColumnDT('distributionid', searchable  =False))
    columns.append(ColumnDT('dcp.movie.title'))
    columns.append(ColumnDT('dcp.name' ))
    columns.append(ColumnDT('dcp.movie.distributor.name'))
    columns.append(ColumnDT('exhibitor.standard_name'))
    columns.append(ColumnDT('dcp.movie.releasedate'))
    columns.append(ColumnDT('autorisation_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('started_transfer_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('finished_transfer_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('status'))
    columns.append(ColumnDT('dcp.contentkind', searchable  =False))
    columns.append(ColumnDT('dcp.movie.movieid',  searchable  =False))
    columns.append(ColumnDT('dcp.movie.distributor.distributorid', searchable  =False))
    columns.append(ColumnDT('exhibitor.exhibitorid', searchable  =False))
    columns.append(ColumnDT('dcp.dcpid', searchable  =False))
    columns.append(ColumnDT('dcp.valid', searchable  =False))

    try:
        _e = int( request.values['exhibitor'])
    except ValueError:
        _e = None

    try:
        _d = int( request.values['distributor'])
    except ValueError:
        _d = None

    try:
        _m = int( request.values['movie'])
    except ValueError:
        _m = None

    try:
        _contentkind =  request.values['contentkind']
    except ValueError:
        _contentkind = None

    query = db.session.query(Distribution).join(Dcp).join(Exhibitor).join(Movie).join(Distributor)
    if _e is not None:
        query = query.filter(Exhibitor.exhibitorid == _e)
    if _d is not None:
        query = query.filter(Distributor.distributorid == _d)
    if _m is not None:
        query = query.filter(Movie.movieid == _m)

    if _contentkind == 'FTR':
        query = query.filter(
            and_(Dcp.contentkind == Dcp.FEATURE, Distribution.isauthorised ==True),
        )
    elif _contentkind == 'TLR':
        query = query.filter(
        Dcp.contentkind == Dcp.TRAILER,
        or_( Distribution.autorisation_date != None,  Distribution.started_transfer_date !=None)
        )
    else:
        query = query.filter(
            or_(
            and_(Dcp.contentkind == Dcp.FEATURE, Distribution.isauthorised ==True),
            and_( Dcp.contentkind == Dcp.TRAILER,
                or_( Distribution.autorisation_date != None,  Distribution.started_transfer_date !=None)
                )
        ))

    # instantiating a DataTable for the query and table needed
    rowTable = DataTables(request.args, Distribution, query, columns)
    # returns what is needed by DataTable
    return jsonify(rowTable.output_result())

@admin.route('/admin/distribution_short_status', methods=['GET', 'POST'])
@login_required
@admin_required
def distribution_short_status():
    return render_template('admin/distribution_short_status.html')


@admin.route('/admin/distributionshort')
@login_required
@admin_required
def distributionshort():
    """Return server side data."""
    # defining columns
    columns = []
    columns.append(ColumnDT('dcp.short.title'))
    columns.append(ColumnDT('dcp.short.catalogues.name'))
    columns.append(ColumnDT('dcp.name', filter=upper))
    columns.append(ColumnDT('exhibitor.standard_name'))
    columns.append(ColumnDT('autorisation_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('started_transfer_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('finished_transfer_date', search_like = False, search_datetime_range = True))
    columns.append(ColumnDT('dcp.contentkind', searchable  =False))
    columns.append(ColumnDT('dcp.short.shortid',  searchable  =False))
    columns.append(ColumnDT('dcp.short.distributor.distributorid', searchable  =False))
    columns.append(ColumnDT('exhibitor.exhibitorid', searchable  =False))
    columns.append(ColumnDT('dcp.dcpid', searchable  =False))
    columns.append(ColumnDT('dcp.valid', searchable  =False))

    try:
        _e = int( request.values['exhibitor'])
    except ValueError:
        _e = None

    try:
        _d = int( request.values['distributor'])
    except ValueError:
        _d = None

    try:
        _m = int( request.values['shr'])
    except ValueError:
        _m = None

    query = db.session.query(Distribution).join(Dcp).join(Exhibitor).join(Short).join(Distributor)
    if _e is not None:
        query = query.filter(Exhibitor.exhibitorid == _e)
    if _d is not None:
        query = query.filter(Distributor.distributorid == _d)
    if _m is not None:
        query = query.filter(Short.shortid == _m)

    # instantiating a DataTable for the query and table needed
    rowTable = DataTables(request.args, Distribution, query, columns)
    # returns what is needed by DataTable

    return jsonify(rowTable.output_result())

@admin.route('/admin/relanceSalles', methods=['GET', 'POST'])
@login_required
@admin_required
def relancesSalles():
    return render_template('admin/relancesalles.html')


from flask_restful import fields, marshal
relance_fields= {
        'distributionid': fields.Integer(),
        'exhibitor': fields.String(attribute='exhibitor.standard_name'),
        'exhibitorid': fields.Integer(),
        'movie': fields.String(attribute='dcp.movie.title'),
        'releasedate': fields.DateTime(attribute='dcp.movie.releasedate', dt_format='iso8601'),
        'autorisation_date': fields.DateTime(dt_format='iso8601'),
        'last_reminder': fields.DateTime(dt_format='iso8601'),
        'dcpid': fields.Integer(),
        'dcp': fields.String(attribute='dcp.name'),
        }

@admin.route('/admin/relanceSallesJson')
@login_required
@admin_required
def relancesSallesJson():
    from datetime import date, timedelta
    today = date.today()
    query = db.session.query(Distribution).join(Dcp).join(Exhibitor).join(Movie).join(Distributor)
    query = query.filter(
    and_(Distribution.autorisation_date != None,
        Distribution.started_transfer_date == None,
        Distribution.isauthorised ==True,
        Dcp.contentkind == Dcp.FEATURE
    )).\
    filter(Distribution.autorisation_date.between( today - timedelta(weeks=4), today))

    marshaled = [marshal(_d, relance_fields) for _d in query.all()]
    return jsonify(data=marshaled)

@admin.route('/admin/do_relance', methods=['POST'])
@login_required
@admin_required
def dorelance():
    app = current_app._get_current_object()
    content = request.json
    authorization = Distribution.query.filter_by(distributionid = request.json['distributionid']).one()
    if authorization:
        send_exhibitor_reminder(authorization)
        flash(u'Salle {} relancée'.format(authorization.exhibitor.name))
        _msg = u"Reminder for {} ({}) sent to {}".format(
                authorization.dcp.name,
                authorization.dcp.movie.title,
                authorization.exhibitor.name)
        app.logger.info(_msg)
    return jsonify({'data': 'dummy'})



@admin.route('/admin/authorization/<int:id>',)
@login_required
@admin_required
def authorization(id):
    auth = Distribution.query.filter_by(distributionid = id).first()
    return render_template('admin/authorization.html',
            data = auth)
