# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError

from . import admin_distributors
from app import db, current_app
from app.decorators import admin_required
from app.models import Distributor, User

from .forms import NewDistributorForm, EditDistributorForm




TEMPLATES={
    'home': 'all.html',
    'delete': 'delete.html',
    'details': 'details.html',
    'edit': 'edit.html',
    'new': 'new.html',
    }

blueprint = admin_distributors

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )



@blueprint.route('/home', methods=('GET', 'POST'))
@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@admin_required
def home():
    _distributors = Distributor.query\
            .filter_by(valid=True)\
            .all()
    return render ('home',
            distributors = _distributors)

@blueprint.route('/new', methods=('GET', 'POST'))
@login_required
@admin_required
def new():
    form = NewDistributorForm()
    if form.validate_on_submit():
        _m = Distributor(
                    name          = form.name.data,
                    isdcpbay      = form.isdcpbay.data,
                    contact       = form.contact.data,
                    email         = form.email.data,
                    phone         = form.phone.data,
                    cnc_code      = form.cnc_code.data
                   )
        try:
            db.session.add(_m)
            db.session.commit()
        except IntegrityError, exc:
            _msg = "{}".format(exc.message)
            flash(_msg)
            current_app.logger.error(_msg)
            db.session.rollback()
        else :
            _msg = u'Distributor {} added'.format(_m.name)
            flash(_msg)
            current_app.logger.warning(_msg)
        return redirect(url_for(".home"))


    return render ('new',
                            form = form)

@blueprint.route('delete/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def delete(id):
    _user = current_user
    _d = Distributor.query.get_or_404(id)
    _d.valid = False
    msg = u'fiche film pour {} supprimée'.format(_d.name)
    flash(msg)
    msg = msg + u' ID {} par {}.'.format(_d.distributorid, current_user)
    current_app.logger.warning(msg)
    db.session.commit()
    return redirect(url_for(".home"))


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id):
    distributor = Distributor.query.get_or_404(id)
    return render ('details',
           distributor = distributor)

@blueprint.route('/edit/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def edit(id):
    distributor = Distributor.query.get_or_404(id)
    form = EditDistributorForm()
    if form.cancel.data:
        flash(u'Mise à jour annulé')
        return redirect(url_for('.details', id = distributor.distributorid))
    if form.validate_on_submit():
        if form.name.data != distributor.name:
            distributor.name = form.name.data
            flash('Distributor name updated')
        _u = User.query.get(form.user.data)

        # Manage user modifications
        if _u is None:
            if distributor.user is not None:
                distributor.user = None
        else :
            if _u != distributor.user:
                distributor.user = _u
                flash('user modified')
            else:
                if distributor.user.email != form.email.data:
                    distributor.user.email = form.email.data
                    flash('user email modified')
                if distributor.user.email2 != form.email2.data:
                    distributor.user.email2 = form.email2.data
                if distributor.user.email3 != form.email3.data:
                    distributor.user.email3 = form.email3.data

        if distributor.email != form.distributor_email.data:
            distributor.email = form.distributor_email.data
            flash('distributor email modified')


        if form.isdcpbay.data != distributor.isdcpbay:
            distributor.isdcpbay = form.isdcpbay.data
            flash('Distributor is oficial updated')
        if form.contact.data  != distributor.contact:
            distributor.contact = form.contact.data
            flash('Distributor contact updated')
        if form.phone.data  != distributor.phone:
            distributor.phone = form.phone.data
            flash('Distributor phone updated')
        if form.cnc_code.data != distributor.cnc_code:
            distributor.cnc_code = form.cnc_code.data
            flash('Distributor CNC code updated')
        db.session.commit()
        return redirect(url_for('.details', id = distributor.distributorid))

    form.name.data     = distributor.name
    form.isdcpbay.data = distributor.isdcpbay
    form.contact.data  = distributor.contact
    form.phone.data    = distributor.phone
    form.distributor_email.data    = distributor.email
    form.cnc_code.data = distributor.cnc_code
    if distributor.user:
        form.user.data = distributor.user.userid
        form.email.data = distributor.user.email
        form.email2.data = distributor.user.email2
        form.email3.data = distributor.user.email3
    else:
        form.user.data = 0

    return render ('edit',
            distributor =  distributor,
            form = form)

