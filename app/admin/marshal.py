# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask_restful import fields

authorisations_fields = {
    'distributionid':fields.Integer,
    'dcpid': fields.Integer,
    'dcp': fields.String(attribute=u'dcp.name'),
    'exhibitor_cncid': fields.Integer(attribute = u'exhibitor.cncid'),
    'exhibitor': fields.String,
    'exhibitorid': fields.Integer,
    'isauthorised':fields.Boolean,
    'autorisation_date': fields.DateTime(dt_format='iso8601'),
    'started_transfer_date': fields.DateTime(dt_format='iso8601'),
    'finished_transfer_date': fields.DateTime(dt_format='iso8601'),
    'status': fields.Integer,
}


dcp_fields = {
    'dcpid': fields.Integer,
    'name': fields.String,
    'valid': fields.Boolean,
    'size': fields.String,
    'contentkind': fields.String,
    }



