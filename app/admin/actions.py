# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
import os
from datetime import datetime

from sqlalchemy.exc import IntegrityError

from app import current_app, db
from app.exceptions import TucoException
from app.torrent.torrent_info import TorrentInfo
from app.models import Dcp, Distribution, Short, Movie, MovieRequest,DistributionStatus
from app.email import send_email, send_email_wo_template, build_to
from app.tuco_message import send_tuco_message
from app.exhibitor.actions import do_start_torrent_downloads

def do_newdcp(p_content, p_path, p_contentkind):
    _dcp = None
    app = current_app._get_current_object()
    _torrent = TorrentInfo.info(p_path)
    _name = os.path.basename(p_path)
    _dcp = Dcp(
        name = os.path.splitext(_name)[0],
        contentkind = p_contentkind,
        size = _torrent['size'],
        torrent_hash = _torrent['hash'],
        torrent_creation = _torrent['creation_date'])
    if isinstance(p_content, Short):
        _dcp.short = p_content
    elif  isinstance(p_content, Movie):
        _dcp.movie = p_content
    else:
        _msg = u"Unkonwn content object: {}".format(p_content)
        current_app.logger.warning(_msg)
        raise TucoException(_msg)
    try:
        db.session.add(_dcp)
        db.session.commit()
    except IntegrityError, exc:
        _msg = exc.message
        if "Duplicate entry" in _msg:
            app.logger.error(_msg)
            _msg = u'{} already in Dcp table'.format(_dcp)
        db.session.rollback()
        raise TucoException(_msg)
    else:
        if isinstance(p_content, Movie):
            p_content.nb_valid_ftr = p_content.nb_ftrs()
            p_content.nb_valid_tlr = p_content.nb_tlrs()
            db.session.commit()
            app.logger.info(u'DCP  {} created for {}  '.format(_dcp.name, _dcp.movie.title ))
        elif  isinstance(p_content, Short):
            app.logger.info(u'DCP {} created for {}  '.format(_dcp.name, _dcp.short.title))


    # create authorisations
    app.logger.info(u'creating authorisations for {}'.format(_dcp))
    Distribution.create_authorisations(_dcp)
    return _dcp

def do_deletedcp(dcp, force = False):
    app = current_app._get_current_object()
    dcp.valid = False
    if dcp.movie:
        dcp.movie.nb_valid_ftr = dcp.movie.nb_ftrs()
        dcp.movie.nb_valid_tlr = dcp.movie.nb_tlrs()
    if force:
        db.session.delete(dcp)
    db.session.commit()

    app.logger.info(u'Dcp {} deleted'.format(dcp))
    return dcp

def do_deletemovie(movie, force = False):
    app = current_app._get_current_object()
    movie.valid = False
    if force:
        db.session.delete(movie)
    db.session.commit()

    app.logger.info(u'Movie {} deleted'.format(movie))
    return movie

def do_deleteshort(short, force = False):
    app = current_app._get_current_object()
    short.valid = False
    if force:
        db.session.delete(short)
    db.session.commit()

    app.logger.info(u'Short {} deleted'.format(short))
    return short



def do_notifydistributor(subject,to,body):
    app = current_app._get_current_object()
    print to
    if to == []:
        _msg = u"Impossible d'envoyer un message: pas de destinataire"
        current_app.logger.warning(_msg)
        raise TucoException(_msg)
    send_email_wo_template(sender = app.config['TUCO_MAIL_SENDER'],
                        to = to,
                        subject = subject,
                        body = body,
                        bcc = [app.config['TUCO_ADMIN']] )

def do_askDcp(subject,to,body):
    app = current_app._get_current_object()
    print to
    if to == []:
        _msg = u"Impossible d'envoyer un message: pas de destinataire"
        current_app.logger.warning(_msg)
        raise TucoException(_msg)
    send_email_wo_template(sender = app.config['TUCO_SUIVI'],
                        to = to,
                        subject = subject,
                        body = body,
                        bcc = [app.config['TUCO_ADMIN']] )


def send_exhibitor_reminder(authorization):
    app = current_app._get_current_object()
    if authorization:
        authorization.last_reminder = datetime.utcnow()
        db.session.commit()
        send_email(
                build_to(authorization.exhibitor.user),
                u'[RAPPEL] DCP pour {} disponible au téléchargement sur Indé-CP'.\
                        format(authorization.dcp.movie.title),
                'distributor/email/reminder_distribution',
                cc = [app.config['TUCO_ADMIN']],
                authorization= authorization)

def deny_request(movie_request, deny_causeId = None, deny_other_cause = None):
    print "deny request : {}".format(deny_causeId)
    movie_request.deny(deny_causeId, deny_other_cause)
    subject = u'Réponse négative - Demande de transfert de {} pour le cinéma {}'.\
            format(movie_request.movie.title, movie_request.exhibitor)

    template = 'distributor/email/deny'
    message = send_tuco_message(
            msg_from = movie_request.movie.distributor,
            msg_to = movie_request.exhibitor,
            subject = subject,
            template =  'distributor/email/deny',
            movie_request = movie_request)


    movie_request.messages.append(message)
    db.session.commit()

def do_sendmsg(movie, exhibitor, body, movie_request = None):
    #TODO: Best thing to do is to use tuco_send_messsage
    app = current_app._get_current_object()
    subject= u"Message de {} pour le film {}".format(movie.distributor.name, movie.title)

    try:
        to = build_to(exhibitor)
    except ModelException as err:
        app.logger.error(u'{}'.format(err))
        raise TucoException(err)
    send_email_wo_template(sender = app.config['TUCO_ADMIN'],
                        to = to,
                        subject = subject,
                        body = body,
                        cc = [ 'contacter@indecp.org'],
                        bcc = [app.config['TUCO_ADMIN']] )
    _msg = u"Msg from {} to {} about  {}".\
        format(movie.distributor, exhibitor, movie)
    message = Message(
            from_id = movie.distributor.user.userid,
            to_id = exhibitor.user.userid,
            subject = subject,
            body = body)

    movie_request.messages.append(message)
    db.session.commit()

    current_app.logger.warning(_msg)

def notify_exhibitor(exhibitor, movie, selected_dcps):
    if exhibitor.startauto:
        dcpids = [ d.dcpid for d in selected_dcps]
        try :
            do_start_torrent_downloads(dcpids, exhibitor)
        except TucoException as err:
            raise TucoException(err)
        else:
            send_email(build_to(exhibitor.user),
            u'Téléchargement - {} de {}'.format(
                movie.title,
                movie.distributor),
            'distributor/email/distribution_started',
            exhibitor = exhibitor, movie = movie,
            selected_dcps = selected_dcps
            )
    else:
        send_email(
            build_to(exhibitor),
            u'Autorisation de téléchargement - {} de {}'.format(
                movie.title,
                movie.distributor),
            'distributor/email/autorisation_distribution',
            exhibitor = exhibitor,
            movie = movie,
            selected_dcps = selected_dcps
            )



def do_distribution(movie, selected_dcps, exhibitor):
    """
    distribute a movie ( 1+ DCP) to an exhibitor
    manage errors and update flash and logging
    returns nothing
    """
    # Is the exhibitor linked to a user? (robustness)
    if not exhibitor.user:
        _msg = u'Exploitant {} non actif. Distribution impossible'.\
            format(exhibitor.name)
        raise TucoException(_msg)
    if selected_dcps == []:
        _msg = u'Pas de DCP selectionné'
        raise TucoException(_msg)
    # Is movie (FTR only) already on distribution (robustness)
    MovieRequest.distribution_is_autorised(exhibitor,movie)
    _added_dcps =  movie.add_dcps_for_distribution(exhibitor, selected_dcps)
    if _added_dcps:
        #QUESTION: if an exception is raised a db.session.rollback is needed ?
        # ie to undo the db.session.add in add_dcps_for_distribution.
        # looks like no need of commit but more test or doc read needed
        current_app.logger.warning(u'Selected DCPS : {}'.format(selected_dcps))
        try:
            notify_exhibitor(exhibitor, movie, selected_dcps)
        except TucoException as err:
            current_app.logger.error(err)
            raise TucoException(err)
        except:
            _msg = u'Something goes wrong in expedition'
            current_app.logger.exception(_msg)
            raise TucoException(_msg)
        else :
            db.session.commit()
            _msg = u'Distribution de {} ajouté pour  {}'.format(movie.title, exhibitor.name)
            current_app.logger.info(_msg)
            return _added_dcps
    else:
        _msg = u'Pas de DCPs a distribué pour {}. Le ciméma {} les a déjà ?'.\
            format(movie.title, exhibitor.name)
        current_app.logger.warning(_msg)
        return None
    return None

def do_remove_authorisation(authorisation):
    if authorisation.dcp.contentkind == Dcp.FEATURE:
        authorisation.isauthorised = False
        authorisation.autorisation_date = None
        authorisation.status = DistributionStatus.DL_NOT_AUTHORIZED
    elif authorisation.dcp.contentkind == Dcp.TRAILER:
        authorisation.autorisation_date = None
        authorisation.status = DistributionStatus.DL_AUTHORIZED
    db.session.commit()
