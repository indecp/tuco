from flask import Blueprint

admin = Blueprint('admin', __name__)
admin_movies = Blueprint('admin_movies', __name__, template_folder="admin/movies/")
admin_distributors = Blueprint('admin_distributors', __name__, template_folder="admin/distributors/")
admin_exhibitors = Blueprint('admin_exhibitors', __name__, template_folder="admin/exhibitors/")
admin_dcps = Blueprint('admin_dcps', __name__, template_folder="admin/dcps/")
admin_catalogues = Blueprint('admin_catalogues', __name__, template_folder="admin/catalogues/")
admin_shorts = Blueprint('admin_shorts', __name__, template_folder="admin/shorts/")
admin_tags = Blueprint('admin_tags', __name__, template_folder="admin/tags/")
admin_movie_requests = Blueprint('admin_movie_requests', __name__, template_folder="admin/movie_requests/")
admin_authorisations = Blueprint('admin_authorisations', __name__, template_folder="admin/authorisations/")

from . import views
from . import actions
from . import movies
from . import distributors
from . import exhibitors
from . import dcps
from . import catalogues
from . import shorts
from . import tags
from . import movie_requests
from . import authorisations


#from . import errors

