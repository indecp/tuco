# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from datetime import date, datetime, timedelta

from flask import render_template, redirect, url_for, flash, request, jsonify
from flask import safe_join
from flask_login import login_required, current_user
from flask_restful import  marshal
from sqlalchemy import or_, and_

from . import admin_authorisations
from app import db, current_app
from app.exceptions import TucoException
from app.torrent.transmission import TorrentClient
from app.decorators import admin_required
from app.common import path2url, redirect_url, post_json_data_to_dict
from app.models import Distribution, DistributionStatus, Movie, Exhibitor
from sqlalchemy.orm import aliased
from .actions import deny_request, do_sendmsg, do_distribution, do_remove_authorisation
from .marshal import authorisations_fields

from app.common import redirect_url

blueprint = admin_authorisations

TEMPLATES={
    'home': 'home.html',
    'all': 'all.html',
    'details': 'details.html',
    }




def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )


@blueprint.route('/home', methods=['GET','POST'])
@blueprint.route('/new')
@blueprint.route('/')
@login_required
@admin_required
def home():
    app = current_app._get_current_object()
    return render ('home',)

@blueprint.route('/all')
@login_required
@admin_required
def all():
    app = current_app._get_current_object()
    return render ('all')


@blueprint.route('/authorisations_data/<string:query_type>')
@login_required
@admin_required
def authorisations_data(query_type):
    app = current_app._get_current_object()
    fromNow = datetime.now() - timedelta( weeks=8)
    if query_type == 'new':
        authorisationss = Distribution.query.\
        filter (
           Distribution.status == DistributionStatus.DL_AUTHORIZED,
           Distribution.autorisation_date > fromNow,
           )\
        .all()
    else:
        authorisationss = Distribution.query.\
                order_by(Distribution.distributionid.desc()).\
                limit(20000)

    marshaled = [marshal(_d, authorisations_fields) for _d in authorisationss]
    return jsonify(data=marshaled)



@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id):
    app = current_app._get_current_object()
    authorisations = Distribution.query.filter_by(distributionid = id).first()
    marshaled = marshal(authorisations, authorisations_fields)
    print marshaled
    return render ('details',
            data = marshaled,
            requestid = id
            )

@blueprint.route('/detailsdata/<int:id>', methods=['GET'])
@login_required
@admin_required
def detailsdata(id):
    app = current_app._get_current_object()
    authorisations = Distribution.query.filter_by(distributionid = id).first()
    marshaled = marshal(authorisations, authorisations_fields)
    return jsonify(data=marshaled)

@blueprint.route('/do_allow', methods=['GET', 'POST'])
@login_required
@admin_required
def do_allow():
    app = current_app._get_current_object()
    content = post_json_data_to_dict(request.json)
    authorisation = Distribution.query.filter_by( distributionid = content['id']).first()

    if not authorisation.isauthorised:
        app.logger.info('Distribute DCP')
        do_distribution(authorisation.dcp.movie,
                        [authorisation.dcp],
                        authorisation.exhibitor)

    return jsonify( marshal(authorisation, authorisations_fields))

@blueprint.route('/do_cancel', methods=['GET', 'POST'])
@login_required
@admin_required
def do_cancel():
    app = current_app._get_current_object()
    print request.json
    content = post_json_data_to_dict(request.json)
    authorisation = Distribution.query.filter_by( distributionid = content['id']).first()

    if authorisation.isauthorised:
        app.logger.info('cancel authorisation')
        do_remove_authorisation(authorisation)
    return jsonify( marshal(authorisation, authorisations_fields))

