# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError

from . import admin_catalogues
from app import db, current_app
from app.decorators import admin_required
from app.models import Catalogue, User

from .forms import NewCatalogueForm, EditCatalogueForm




TEMPLATES={
    'home': 'all.html',
    'delete': 'delete.html',
    'details': 'details.html',
    'edit': 'edit.html',
    'new': 'new.html',
    }

blueprint = admin_catalogues

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )



@blueprint.route('/home', methods=('GET', 'POST'))
@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@admin_required
def home():
    _catalogues = Catalogue.query\
            .all()
    return render ('home',
            catalogues = _catalogues)

@blueprint.route('/new', methods=('GET', 'POST'))
@login_required
@admin_required
def new():
    form = NewCatalogueForm()
    if form.validate_on_submit():
        _m = Catalogue(
                    name          = form.name.data,
                   )
        try:
            db.session.add(_m)
            db.session.commit()
        except IntegrityError, exc:
            _msg = "{}".format(exc.message)
            flash(_msg)
            current_app.logger.error(_msg)
            db.session.rollback()
        else :
            _msg = u'Catalogue {} added'.format(_m.name)
            flash(_msg)
            current_app.logger.warning(_msg)
        return redirect(url_for(".home"))


    return render ('new',
                            form = form)

@blueprint.route('delete/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def delete(id):
    _user = current_user
    _d = Catalogue.query.get_or_404(id)
    if _d.valid :
        _d.valid = False
        msg = u'Catalogue {} desactivée'.format(_d.name)
    else:
        db.session.delete(_d)
        msg = u'Catalogue {} supprimée'.format(_d.name)
    flash(msg)
    msg = msg + u' ID {} par {}.'.format(_d.id, current_user)
    current_app.logger.warning(msg)
    db.session.commit()
    return redirect(url_for(".home"))


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id):
    catalogue = Catalogue.query.get_or_404(id)
    return render ('details',
           catalogue = catalogue)

@blueprint.route('/edit/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def edit(id):
    catalogue = Catalogue.query.get_or_404(id)
    form = EditCatalogueForm()
    if form.cancel.data:
        flash(u'Mise à jour annulé')
        return redirect(url_for('.details', id = catalogue.id))
    if form.validate_on_submit():
        print " FORM submit"
        if form.name.data != catalogue.name:
            catalogue.name = form.name.data
        if form.valid.data != catalogue.valid:
            print "coucou valid: ", form.valid.data
            catalogue.valid = form.valid.data
        flash('Catalogue updated')
        db.session.commit()
        return redirect(url_for('.details', id = catalogue.id))

    form.name.data     = catalogue.name
    form.valid.data     = catalogue.valid
    return render ('edit',
            catalogue =  catalogue,
            form = form)

