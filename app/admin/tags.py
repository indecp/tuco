# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import render_template, redirect, url_for, flash, request
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError

from . import admin_tags
from app import db, current_app
from app.decorators import admin_required
from app.models import Tag, User

from .forms import NewTagForm, EditTagForm




TEMPLATES={
    'home': 'all.html',
    'delete': 'delete.html',
    'details': 'details.html',
    'edit': 'edit.html',
    'new': 'new.html',
    }

blueprint = admin_tags

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )



@blueprint.route('/home', methods=('GET', 'POST'))
@blueprint.route('/', methods=('GET', 'POST'))
@login_required
@admin_required
def home():
    _tags = Tag.query\
            .all()
    return render ('home',
            tags = _tags)

@blueprint.route('/new', methods=('GET', 'POST'))
@login_required
@admin_required
def new():
    form = NewTagForm()
    if form.validate_on_submit():
        _m = Tag(
                    name          = form.name.data,
                   )
        try:
            db.session.add(_m)
            db.session.commit()
        except IntegrityError, exc:
            _msg = "{}".format(exc.message)
            flash(_msg)
            current_app.logger.error(_msg)
            db.session.rollback()
        else :
            _msg = u'Tag {} added'.format(_m.name)
            flash(_msg)
            current_app.logger.warning(_msg)
        return redirect(url_for(".home"))


    return render ('new',
                            form = form)

@blueprint.route('delete/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def delete(id):
    _user = current_user
    _d = Tag.query.get_or_404(id)
    if _d.valid :
        _d.valid = False
        msg = u'Tag {} desactivée'.format(_d.name)
    else:
        db.session.delete(_d)
        msg = u'Tag {} supprimée'.format(_d.name)
    flash(msg)
    msg = msg + u' ID {} par {}.'.format(_d.id, current_user)
    current_app.logger.warning(msg)
    db.session.commit()
    return redirect(url_for(".home"))


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@blueprint.route('/details/<string:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id):
    app = current_app._get_current_object()
    if isinstance(id, int):
        tag = Tag.query.get_or_404(id)
    elif isinstance(id, unicode):
        tag = Tag.query.filter_by(name=id).first()
        if tag is None:
            _msg = u"No tag found wig id {}".format(id)
            current_app.logger.warning(_msg)
            abort(404)
    else:
        abort(404)
    return render ('details',
            tag = tag)

@blueprint.route('/edit/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def edit(id):
    tag = Tag.query.get_or_404(id)
    form = EditTagForm()
    if form.cancel.data:
        flash(u'Mise à jour annulé')
        return redirect(url_for('.details', id = tag.id))
    if form.validate_on_submit():
        print " FORM submit"
        if tag.name != form.name.data :
            tag.name = form.name.data
        if tag.valid != form.valid.data :
            tag.valid = form.valid.data
        if tag.longname != form.longname.data :
            tag.longname = form.longname.data
        if tag.description != form.description.data :
            tag.description = form.description.data
        flash('Tag updated')
        db.session.commit()
        return redirect(url_for('.details', id = tag.id))

    form.valid.data       = tag.valid
    form.name.data        = tag.name
    form.longname.data    = tag.longname
    form.description.data = tag.description
    return render ('edit',
            tag =  tag,
            form = form)
