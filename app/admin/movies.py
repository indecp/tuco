# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import pickle

from flask import render_template, redirect, url_for, flash, request, jsonify
from flask import send_from_directory
from flask_login import login_required, current_user
from flask_restful import fields, marshal
from flask_wtf import FlaskForm

import wtforms
from wtforms import validators

from . import admin_movies
from app import db, current_app, csrf
from app.decorators import admin_required
from app.models import Movie, Distributor, Dcp, Tag, Extra, Catalogue
from app.exceptions import TucoException

from .forms import  NotifyDistributorForm
from .actions import do_deletedcp, do_notifydistributor, do_deletemovie
from .actions import do_askDcp


TEMPLATES={
    'home': 'all.html',
    'delete': 'delete.html',
    'details': 'details.html',
    'edit': 'edit.html',
    'new': 'new.html',
    }

blueprint = admin_movies

def render(template, **kwargs):
    return render_template( blueprint.template_folder + TEMPLATES[template],
                            **kwargs
                            )

movies_fields = {
    'movieid': fields.Integer,
    'valid': fields.Boolean,
    'title': fields.String,
    'original_title': fields.String,
    'releasedate': fields.String,
    'distributor':fields.String(attribute='distributor.name'),
    'distributorid':fields.Integer(attribute='distributor.distributorid'),
    'nb_ftrs':fields.Integer(attribute='nb_valid_ftr'),
    'nb_tlrs':fields.Integer(attribute='nb_valid_tlr'),
}

class MovieForm(FlaskForm):
    title           = wtforms. StringField('Titre',validators=[validators.Required()])
    valid           = wtforms. BooleanField('Movie valid', default =True)
    original_title  =  wtforms. StringField('Titre Original')
    releasedate     = wtforms. DateField ('Date de sortie ( "YYYY-MM-DD") ',validators=[validators.Required()])
    tag            = wtforms.SelectField(' tags:', coerce = int, validators=[validators.Optional()] )
    distributor     =  wtforms. SelectField('Distributeur', coerce=int)
    catalogue =  wtforms.SelectField(' Appartient au(x) catalogue(s):', coerce = int, validators=[validators.Optional()] )
    website_url   =  wtforms. StringField('Site web')
    submit = wtforms. SubmitField('Send')

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.distributor.choices = \
            [ (_d.distributorid, _d.name) for _d in Distributor.query.order_by('name') ]
        self.tag.choices = \
            [ (_d.id, _d.name) for _d in Tag.query.order_by('name') ]
        self.catalogue.choices = \
            [ (_d.id, _d.name) for _d in Catalogue.query.order_by('name') ]

class EditMovieForm(MovieForm):
    cancel = wtforms.SubmitField('Cancel')





@blueprint.route('/home')
@blueprint.route('/')
@login_required
@admin_required
def home():
    return render('home')

@blueprint.route('/getMoviesJson')
@login_required
@admin_required
def getMoviesJson():
    _movies = Movie.query.\
            order_by(Movie.releasedate.desc()).all()

    marshaled = [marshal(_d, movies_fields) for _d in _movies]
    return jsonify(data=marshaled)


@blueprint.route('/new', methods=('GET', 'POST'))
@login_required
@admin_required
def new():
    _movies = Movie.query.order_by(Movie.releasedate.desc()).all()
    form = MovieForm()
    if form.validate_on_submit():
        _distributor = Distributor.query.get(form.distributor.data)
        movie = Movie( title          = form.title.data,
                    original_title = form.original_title.data,
                    releasedate    = form.releasedate.data,
                    valid          = form.valid.data,
                    distributor    = _distributor
                   )
        movie.catalogues = [Catalogue.query.get(int(cid)) for cid in form.catalogue.raw_data]
        movie.tags = [Tag.query.get(int(cid)) for cid in form.tag.raw_data]
        db.session.add(movie)
        db.session.commit()
        if form.website_url.data :
            if movie.extra:
                extra = movie.extra
                extra.website_url = form.website_url.data
            else:
                extra = Extra(website_url = form.website_url.data)
                movie.extra = extra
        db.session.commit()
        return redirect(url_for(".home"))
    return render ('new',
                            movies = _movies,
                            form = form)

@blueprint.route('/delete/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def delete(id):
    _movie = Movie.query.get_or_404(id)
    if _movie.valid :
        do_deletemovie(_movie)
        msg = u'FILM {} supprimée (non valid)'.format(_movie.title)
    else:
        do_deletemovie(_movie, force = True)
        msg = u'FILM {} supprimée (définitivement)'.format(_movie.title)

    current_app.logger.info(msg)
    flash(msg)
    return redirect(url_for(".home"))


@blueprint.route('/details/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def details(id ):
    movie = Movie.query.get_or_404(id)
    from flask_wtf import Form
    form = Form()
    if form.validate_on_submit():
        for _id in request.form.getlist('dcps'):
            _dcp = Dcp.query.get_or_404(_id)
            if _dcp.valid :
                _dcp = do_deletedcp(_dcp)
            else:
                _dcp = do_deletedcp(_dcp, True)
            flash ("Dcp {} deleted".format(_dcp.name))
        return redirect(url_for('.details', id = id))
    return render ('details',
            movie = movie,
            #extra = pickle.loads(movie.extra),
            dcps = movie.dcps.all(),
            form = form)

@blueprint.route('/edit/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def edit(id):
    movie = Movie.query.get_or_404(id)
    form = EditMovieForm()
    if form.validate_on_submit():
        if form.cancel.data:
            flash(u'Mise à jour annulé')
            return redirect(url_for('.details', id = movie.movieid))
        if form.title.data != movie.title:
            movie.title = form.title.data
            flash(u'Titre mis à jour')
        if form.original_title.data != movie.original_title:
            movie.original_title = form.original_title.data
            flash(u'Titre original mis à jour')
        if form.releasedate.data != movie.releasedate:
            movie.releasedate = form.releasedate.data
            flash(u'Date de sortie mis à jour')
        _distributor = Distributor.query.get(form.distributor.data)
        if _distributor != movie.distributor:
            flash(u'Distributeur mis à jour')
            movie.distributor = _distributor
        if  movie.valid != form.valid.data:
            movie.valid = form.valid.data

        movie.catalogues = [Catalogue.query.get(int(cid)) for cid in form.catalogue.raw_data]
        movie.tags = [Tag.query.get(int(cid)) for cid in form.tag.raw_data]
        if form.website_url.data :
            print "update website"
            if movie.extra:
                extra = movie.extra
                extra.website_url = form.website_url.data
            else:
                extra = Extra(website_url = form.website_url.data)
                movie.extra = extra

        db.session.commit()
        movie.tags = [Tag.query.get(int(cid)) for cid in form.tag.raw_data]
        db.session.add(movie)
        db.session.commit()
        return redirect(url_for('.details', id = movie.movieid))

    form.title.data = movie.title
    form.original_title.data = movie.original_title
    form.valid.data       = movie.valid
    form.releasedate.data = movie.releasedate
    form.distributor.data = movie.distributor.distributorid
    form.catalogue.data = [ (el.id, el.name) for el in movie.catalogues ]
    form.tag.data = [ (el.id, el.name) for el in movie.tags ]
    if movie.extra:
        form.website_url.data = movie.extra.website_url

    return render ('edit',
            movie = movie,
            form = form)

@blueprint.route('/download/<int:id>')
@login_required
@admin_required
def download(id):
    dcp = Dcp.query.get_or_404(id)
    app = current_app._get_current_object()
    return send_from_directory(app.config['TORRENT_FOLDER'],
                               dcp.name+".torrent",
                               as_attachment = True)

@blueprint.route('/notifydistributorDcp/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def notifydistributorDcp(id):
    dcp=Dcp.query.get_or_404(id)
    movie= dcp.movie
    distributor = movie.distributor
    form = NotifyDistributorForm()
    if form.validate_on_submit():
        try:
            print form.to.data
            to = form.to.data.split(',')
            to = [ x.strip() for x in to ]
            do_notifydistributor(form.subject.data, to, form.message.data)
        except TucoException as err:
            flash(err.msg)
        else:
            _msg = u"La Notification pour le DCP {} à été envoyée au distributeur {}"\
            .format(dcp.name,distributor.name)
            flash(_msg)
        return redirect(url_for('.details', id = movie.movieid))

    body=render_template('admin/email/notify_new_dcp.txt',
            dcp=dcp)

    to = distributor.get_emails()
    subject = u"Mise à disposition du DCP {} ".format(dcp.name)
    response = {
            'subject'  : subject,
            'to'       : ','.join(to),
            'body'     : body,
            }
    return jsonify(response)

@blueprint.route('/notifydistributorMovie/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def notifydistributoMovie(id):
    movie= Movie.query.get_or_404(id)
    distributor = movie.distributor
    form = NotifyDistributorForm()
    if form.validate_on_submit():
        try:
            print form.to.data
            to = form.to.data.split(',')
            to = [ x.strip() for x in to ]
            do_askDcp(form.subject.data, to, form.message.data)
        except TucoException as err:
            flash(err.msg)
        else:
            _msg = u"La demade de DCP(s) {} à été envoyée au distributeur {}"\
            .format(movie.title,distributor.name)
            flash(_msg)
        return redirect(url_for('.details', id = movie.movieid))

    body=render_template('admin/email/notify_movie.txt',
            movie=movie)

    to = distributor.get_emails()
    subject = u"Mise à disposition de DCP pour le film {} ".format(movie.title)
    response = {
            'subject'  : subject,
            'to'       : ','.join(to),
            'body'     : body,
            }
    return jsonify(response)

@blueprint.route('/askDcp/<int:id>', methods=['GET','POST'])
@login_required
@admin_required
def askDcp(id):
    movie= Movie.query.get_or_404(id)
    distributor = movie.distributor
    form = NotifyDistributorForm()
    if form.validate_on_submit():
        try:
            print form.to.data
            to = form.to.data.split(',')
            to = [ x.strip() for x in to ]
            do_askDcp(form.subject.data, to, form.message.data)
        except TucoException as err:
            flash(err.msg)
        else:
            _msg = u"La demande de DCP pour le film {} à été envoyée au distributeur {}"\
            .format(movie.title,distributor.name)
            flash(_msg)
        return redirect(url_for('.details', id = movie.movieid))

    body=render_template('admin/email/ask_dcp.txt',
            movie=movie)

    to = distributor.get_emails()
    subject = u"Demande de DCP pour le film {} ".format(movie.title)
    response = {
            'subject'  : subject,
            'to'       : ','.join(to),
            'body'     : body,
            }
    return jsonify(response)
