# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

from flask_wtf import Form
from wtforms import FileField, SelectField, DateField, StringField, SubmitField, SelectMultipleField
from wtforms import IntegerField, BooleanField, TextField, TextAreaField, RadioField
from wtforms import validators, ValidationError
from wtforms.fields.html5 import SearchField
from app.models import Movie, Distributor, Dcp , User, Permission, Catalogue, Tag

class TorrentForm(Form):
    movie   =      SelectField('Select Movie', coerce=int)
    contentkind =  SelectField('Type', choices = [(Dcp.FEATURE, Dcp.FEATURE) , (Dcp.TRAILER, Dcp.TRAILER )])
    torrent =      FileField('Torrent file')
    submit =       SubmitField('Submit')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.movie.choices = \
            [ (_m.movieid, _m.title) for _m in Movie.query.order_by('title') ]

class ShortTorrentForm(Form):
    torrent =      FileField('Torrent file')
    submit =       SubmitField('Submit')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)



class MovieForm(Form):
    title   =  StringField('Title',validators=[validators.Required()])
    releasedate = DateField ('Release date ( "YYYY-MM-DD") ',validators=[validators.Required()])
    distributor =  SelectField('Distributor', coerce=int)
    submit = SubmitField('Submit')
    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.distributor.choices = \
            [ (_d.distributorid, _d.name) for _d in Distributor.query.order_by('name') ]

class EditMovieForm(Form):
    title   =  StringField('Titre',validators=[validators.Required()])
    original_title   =  StringField('Titre Original')
    releasedate = DateField ('Date de sortie ( "YYYY-MM-DD") ',validators=[validators.Required()])
    distributor =  SelectField('Distributeur', coerce=int)
    website_url   =  StringField('Site web')
    valid = BooleanField('Movie valid')
    tag =  SelectMultipleField(' tags:', coerce = int, validators=[validators.Optional()] )
    submit = SubmitField('Send')
    cancel = SubmitField('Cancel')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.distributor.choices = \
            [ (_d.distributorid, _d.name) for _d in Distributor.query.order_by('name') ]
        self.tag.choices = \
            [ (_d.id, _d.name) for _d in Tag.query.order_by('name') ]

class NewDistributorForm(Form):
    name   =  StringField('Distributor name',validators=[validators.Required()])
    isdcpbay = BooleanField('Is official?')
    contact  = StringField('Contact')
    phone    = StringField('Phone')
    email    = StringField('Mail de contact')
    #TODO cnc code to validate. try if useful
    cnc_code = IntegerField('CNC code',validators=None)
    submit = SubmitField('submit')


class EditDistributorForm(Form):
    name     = StringField('Distributor name',validators=[validators.Required()])
    isdcpbay = BooleanField('Is official?')
    contact  = StringField('Contact')
    phone    = StringField('Phone')
    distributor_email = StringField(u'Mail Général du distributeur (hors utilisateur)')

    #TODO cnc code to validate. try if useful
    cnc_code = IntegerField('CNC code',validators=None)
    user    =  SelectField('Utilisateur', coerce=int)
    email  = StringField(u'Utilisateur email principal')
    email2  = StringField(u'Utilisateur email 2')
    email3  = StringField(u'Utilisateur email 3')
    submit = SubmitField('Send')
    cancel = SubmitField('Cancel')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user.choices = \
            [ (_u.userid, _u.login) for _u in User.query.filter_by(role = Permission.DISTRIBUTE).order_by('login') ]
        _dummy = (0,u'Aucun')
        self.user.choices.append(_dummy)

    def validate_email(self, field):
        # TODO: ADD email check only on change
        pass

class EditUserForm(Form):
    email = StringField('Email Address', [validators.Required()])
    submit    = SubmitField('Send')
    cancel    = SubmitField('Cancel')

    def validate_email(self, field):
        # TODO: ADD email check only on change
        pass

class EditDcpForm(Form):
    name     = StringField(u'Nom du DCP',)
    contentkind =  SelectField(u'Type de DCP', choices =  [(Dcp.FEATURE, Dcp.FEATURE),
                                                           (Dcp.TRAILER, Dcp.TRAILER),
                                                           (Dcp.SHORT, Dcp.SHORT)])
    valid = BooleanField('Dcp valid')
    movie = TextField()
    submit = SubmitField('Send')
    cancel = SubmitField('Cancel')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

class NotifyDistributorForm(Form):
    subject = TextField(u"Envoyer à",  [validators.Required()])
    to = TextField(u"Envoyer à",  [validators.Required()])
    message = TextAreaField(u"Message",  [validators.Required()])
    submit = SubmitField(u"Envoyer le message")

class NewCatalogueForm(Form):
    name   =  StringField('Catalogue name',validators=[validators.Required()])
    submit = SubmitField('Send')
    cancel = SubmitField('Cancel')


class EditCatalogueForm(Form):
    name     = StringField('Catalogue name',validators=[validators.Required()])
    valid = BooleanField('Is Catalogue valid?')
    submit = SubmitField('Send')
    cancel = SubmitField('Cancel')


class NewTagForm(Form):
    name   =  StringField('Tag name',validators=[validators.Required()])
    submit = SubmitField('Send')
    cancel = SubmitField('Cancel')


class EditTagForm(Form):
    name     = StringField('Tag name',validators=[validators.Required()])
    longname     = TextField('Nom explicite',)
    description     = TextAreaField('Description',)
    valid = BooleanField('Is Tag valid?')
    submit = SubmitField('Send')
    cancel = SubmitField('Cancel')


